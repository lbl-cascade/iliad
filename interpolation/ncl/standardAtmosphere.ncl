;$LastChangedDate: 2013-11-22 08:46:54 -0800 (Fri, 22 Nov 2013) $
;$Rev: 12 $
;$Author: taobrien $
;

load "$CFSNCL/earthPhysics.ncl"

;Calculate and define some additional constants
stdLapseRate = 6.5e-3 ; standard lapse rate [K/m]
stdTemperature = 288.15 ; Kelvin
stdPressure = 101325. ; Pa
stdInvHeight = stdLapseRate/stdTemperature ; 1/m
stdNegExponent = -grav/(rgas*stdLapseRate)
stdPosExponent = grav/(rgas*stdLapseRate)
stdPosExnerExponent = rgas/cpd
stdNegExnerExponent = -rgas/cpd


undef("calculateSLP")
function calculateSLP(surfacePressure,surfaceElevation)
;Returns the surface sea level pressure from the raw surface pressure and
;surface height, assuming a standard atmosphere
begin
  return surfacePressure*(1.0 - stdInvHeight*surfaceElevation)^stdNegExponent
end

undef("calculatePS")
function calculatePS(seaLevelPressure,surfaceElevation)
;Returns the surface pressure from the sea level pressure and
;surface height, assuming a standard atmosphere
begin
  return seaLevelPressure*(1.0 - stdInvHeight*surfaceElevation)^stdPosExponent
end

undef("calculateSLT")
function calculateSLT(temperature,pressure)
;Returns the sea-level temperature from the actual temperature and pressure,
;assuming a standard atmosphere
begin
  return temperature*(pressure/stdPressure)^stdNegExnerExponent
end

undef("calculateT")
function calculateT(seaLevelTemperature,pressure)
;Returns the temperature given the sea level temperature and pressure,
;assuming a standard atmosphere
begin
  return seaLevelTemperature*(pressure/stdPressure)^stdPosExnerExponent
end


undef("testStandardAtmosphere")
procedure testStandardAtmosphere()
;Does unit testing on the standardAtmosphere include file
begin
  ;Do some code validation based on the ATM 120 homework 18 Q3b (from 2010)
  ps = 94030. ;Pa
  zs = 639.8  ;m

  slpAnswer = 101500. 
  slpFunc = calculateSLP(ps,zs)
  psFunc = calculatePS(slpAnswer,zs)

  print("From calculateSLP(), and from HW 18 3b Key: "+slpFunc+" Pa, "+slpAnswer+" Pa")

  print("From calculatePS(), and from HW 18 3b Key: "+psFunc+" Pa, "+ps+" Pa")

  print("")

  ts = 280.   ;K
  p = 90000.  ;Pa
  sltFunc = calculateSLT(ts,p)
  tsFunc = calculateT(sltFunc,p)
  print("From calculateSLT(): " + sltFunc+"K")
  print("From calculateT() and original T: " + tsFunc+" K, "+ts+" K")

  print("")

  print("Running 2D test")

  psFileName = "cami-mam3_0000-01-01_1.9x2.5_L30_c090306.nc"
  phisFileName = "USGS-gtopo30_1.9x2.5_remap_c050602.nc"

  if(.not.isfilepresent(psFileName))then
    print("Warning: "+psFileName+" not found.  Skipping 2D SLP calculation test.")
    return
  end if
  if(.not.isfilepresent(phisFileName))then
    print("Warning: "+phisFileName+" not found.  Skipping 2D SLP calculation test.")
    return
  end if

  fps = addfile(psFileName,"r")
  fphis = addfile(phisFileName,"r")

  ps := fps->PS(0,:,:)
  phis = fphis->PHIS(:,:)

  zs := phis/grav
  slp = ps
  slp = calculateSLP(ps,zs)

  outFileName = "slp2DTest.nc"
  if(isfilepresent(outFileName))then
    system("rm " + outFileName)
  end if
  fout = addfile(outFileName,"c")
  fout->SLP = slp

  print("2D test complete")

end

;Do unit testing
;begin
;  testStandardAtmosphere()
;end
