;$LastChangedDate: 2013-11-27 10:07:53 -0800 (Wed, 27 Nov 2013) $
;$Rev: 25 $
;$Author: taobrien $
;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "$CFSNCL/addVarToFile.ncl"

begin
  

  opt = True
  opt@InterpMethod = "bilinear"
  opt@ForceOverwrite = True
  opt@PrintTimings = True
  opt@DstESMF = True

  srcSCRIPFiles = (/"cfsr-0.5deg-SCRIP.nc", \
                    "cfsr-0.3deg-SCRIP.nc"/)
  destESMFFiles = (/"cam-ne30np4-ESMF.nc", \
                     "cam-ne30np4-ESMF.nc"/)
  wgtFileNames = (/"wgts_CFSR0.5deg-to-ne30np4.nc", \
                   "wgts_CFSR0.3deg-to-ne30np4.nc" /)
                  

  do i = 0,1
    srcSCRIPFile = srcSCRIPFiles(i)
    destESMFFile = destESMFFiles(i)
    wgtFileName = wgtFileNames(i)
    ESMF_regrid_gen_weights(srcSCRIPFile,destESMFFile,wgtFileName,opt)
    fsrc := addfile(srcSCRIPFile,"r")
    fdest := addfile(destESMFFile,"r")
    if(i.lt.1)then
      addVarToFile(wgtFileName,fdest->hyam,"hyam")
      addVarToFile(wgtFileName,fdest->hybm,"hybm")
      addVarToFile(wgtFileName,fdest->P0,"P0")
      addVarToFile(wgtFileName,fsrc->lv_ISBL0,"lv_ISBLO")
    end if
  end do
end
