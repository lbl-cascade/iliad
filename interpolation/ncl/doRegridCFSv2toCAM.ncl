load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
;TODO: Prefix these with an environment variable for the cfsInterpolation installation
load "$CFSNCL/standardAtmosphere.ncl"
load "$CFSNCL/earthPhysics.ncl"
load "$CFSNCL/camClouds.ncl"

;The coefficient to determine the pressure of Hybrid level 1 variables from the
;CFSv2 flx files ( = cfsv2HybSig1 * PS )
cfsv2HybSig1 = 0.99467117

;Define a routine to add a level to a variable
undef("concatenateLevel")
function concatenateLevel(var3D,var2D)
local dsize,nlev,newVar3D
begin
  ;Get the dimensions of the 3D variable
  dsize = dimsizes(var3D)
  ;Save the number of levels (assumed to be the first dim)
  nlev = dsize(0)
  ;Add one level to the dimensions
  dsize(0) = dsize(0) + 1
  ;Declare a new variable with the new dimension sizes
  newVar3D = new(dsize,typeof(var2D))
  ;Set the first part to be the 3D variable
  newVar3D(0:nlev-1,:,:) = var3D
  ;And put the 2D var at the end
  newVar3D(nlev,:,:) = var2D
  ;return the new variable
  return newVar3D
end

begin

  ;**********************************************
  ; The following vars should be provided by
  ; the wrapper script
  ;**********************************************
  ;cfsv2PgbFile = "/projects/data/CFSv2/cdas.20140116/cdas1.t00z.pgrbh00.grib2"
  ;cfsv2FlxFile = "/projects/data/CFSv2/cdas.20140116/cdas1.t00z.sfluxgrbf00.grib2"
  ;wgtFile_h_latxlon = "wgts_CFSv20.5deg-to-fv1.9x2.5-latxlon.nc"
  ;wgtFile_h_slatxlon = "wgts_CFSv20.5deg-to-fv1.9x2.5-slatxlon.nc"
  ;wgtFile_h_latxslon = "wgts_CFSv20.5deg-to-fv1.9x2.5-latxslon.nc"
  ;wgtFile_f_latxlon = "wgts_CFSv20.3deg-to-fv1.9x2.5-latxlon.nc"
  ;wgtFile_f_slatxlon = "wgts_CFSv20.3deg-to-fv1.9x2.5-slatxlon.nc"
  ;wgtFile_f_latxslon = "wgts_CFSv20.3deg-to-fv1.9x2.5-latxslon.nc"
  ;camTopoFile = "USGS-gtopo30_1.9x2.5_remap_c050602.nc"
  ;outFile = "iniTestFile.nc"
  ;camSSTFile = "sst-0000-01-01_fv1.9x2.5.nc"
  ;beVerbose = True

  ;*********************
  ; Read 3D CFSv2 data
  ;*********************
  if(beVerbose)then
    print("Reading CFSv2 data from PGB file")
  end if
  fpgbh = addfile(cfsv2PgbFile,"r")
  ;Read in the CFSv2 dynamical variables from the PGB file
  Ucfsv2 = fpgbh->UGRD_P0_L100_GLL0
  Vcfsv2 = fpgbh->VGRD_P0_L100_GLL0
  Tcfsv2 = fpgbh->TMP_P0_L100_GLL0
  Qcfsv2 = fpgbh->SPFH_P0_L100_GLL0
  QCcfsv2 = fpgbh->CLWMR_P0_L100_GLL0

  ;*********************
  ; Read 2D CFSv2 data
  ;*********************
  if(beVerbose)then
    print("Reading CFSv2 data from FLX file")
  end if
  fflxf = addfile(cfsv2FlxFile,"r")
  ;Read in the surface/near-surface CFSv2 dynamical variables from the FLX file
  PScfsv2_f_h1 = fflxf->PRES_P0_L1_GGA0
  ZScfsv2_f_h1 = fflxf->HGT_P0_L1_GGA0
  Tcfsv2_f_h1 = fflxf->TMP_P0_L105_GGA0
  Qcfsv2_f_h1 = fflxf->SPFH_P0_L105_GGA0
  Ucfsv2_f_h1 = fflxf->UGRD_P0_L105_GGA0
  Vcfsv2_f_h1 = fflxf->VGRD_P0_L105_GGA0
  MASKcfsv2_f = fflxf->LAND_P0_L1_GGA0
  TScfsv2_f = fflxf->TMP_P0_L1_GGA0
  ICEcfsv2_f = fflxf->ICEC_P0_L1_GGA0

  ;*********************
  ; Read CAM Topo data
  ;*********************
  if(beVerbose)then
    print("Reading CAM topo data")
  end if
  ftopo = addfile(camTopoFile,"r")
  ;Read in the CAM topo
  ZShoriz_latxlon = ftopo->PHIS
  ZShoriz_latxlon = ZShoriz_latxlon/grav

  ;****************************************
  ; Prepare variables for interpolation
  ;****************************************
  if(beVerbose)then
    print("Preparing variables for interpolation")
  end if
  ;Convert PS to SLP
  SLPcfsv2 = calculateSLP(PScfsv2_f_h1,ZScfsv2_f_h1)
  SLTcfsv2_f_h1 = calculateSLT(Tcfsv2_f_h1,PScfsv2_f_h1)

  ;Add cloud water to the water vapor mixing ratio to get total water
  ;the QC variable is missing the top 5 model levels, hence the 5: slicing
  ; and QC is a mixing ratio; the q/(1+q) converts it to specific form
  Qcfsv2(5:,:,:) = Qcfsv2(5:,:,:) + QCcfsv2/(1. + QCcfsv2)

  ;Mask the land points in the SST variable and do a poisson fill
  if(beVerbose)then
    print("Doing grid fill on SST")
  end if
  TScfsv2_f@_FillValue = -9999.
  TScfsv2_f = where(MASKcfsv2_f.ne.0.0,TScfsv2_f@_FillValue,TScfsv2_f)
  poisson_grid_fill(TScfsv2_f, True,1,1500,1e-2,0.6,0)
;  TScfsv2_f = where(MASKcfsv2_f.ne.0.0, \
;                 conform_dims(dimsizes(TScfsv2_f),dim_avg(TScfsv2_f),(/0/)), \
;                 TScfsv2_f)
  TScfsv2_f = TScfsv2_f - t0
  ;TScfsv2_f = where(ICEcfsv2_f.ge.1,-1.8,TScfsv2_f)

  opt = True
  if(beVerbose)then
    opt@PrintTimings = True
    print("Regridding to the horizontal CAM grid.")
  end if
  ;******************************************
  ; Regrid horizontally to the CAM grid
  ;******************************************
  UShoriz = ESMF_regrid_with_weights(Ucfsv2,wgtFile_h_slatxlon,opt)
  VShoriz = ESMF_regrid_with_weights(Vcfsv2,wgtFile_h_latxslon,opt)
  Thoriz = ESMF_regrid_with_weights(Tcfsv2,wgtFile_h_latxlon,opt)
  Qhoriz = ESMF_regrid_with_weights(Qcfsv2,wgtFile_h_latxlon,opt)
  SLPhoriz_latxlon = ESMF_regrid_with_weights(SLPcfsv2,wgtFile_f_latxlon,opt)
  SLPhoriz_slatxlon = ESMF_regrid_with_weights(SLPcfsv2,wgtFile_f_slatxlon,opt)
  SLPhoriz_latxslon = ESMF_regrid_with_weights(SLPcfsv2,wgtFile_f_latxslon,opt)
  ZShoriz_slatxlon = ESMF_regrid_with_weights(ZScfsv2_f_h1,wgtFile_f_slatxlon,opt)
  ZShoriz_latxslon = ESMF_regrid_with_weights(ZScfsv2_f_h1,wgtFile_f_latxslon,opt)

  UShoriz_h1 = ESMF_regrid_with_weights(Ucfsv2_f_h1,wgtFile_f_slatxlon,opt)
  VShoriz_h1 = ESMF_regrid_with_weights(Vcfsv2_f_h1,wgtFile_f_latxslon,opt)
  SLThoriz_h1 = ESMF_regrid_with_weights(SLTcfsv2_f_h1,wgtFile_f_latxlon,opt)
  Qhoriz_h1 = ESMF_regrid_with_weights(Qcfsv2_f_h1,wgtFile_f_latxlon,opt)
  TShoriz = ESMF_regrid_with_weights(TScfsv2_f,wgtFile_f_latxlon,opt)
  ICEhoriz = ESMF_regrid_with_weights(ICEcfsv2_f,wgtFile_f_latxlon,opt)

  ;******************************************
  ; Undo horizontal interpolation prep work 
  ;******************************************
  if(beVerbose)then
    print("Undoing interpolation prep")
  end if
  ;Calculate PS from SLP
  PShoriz_latxlon = calculatePS(SLPhoriz_latxlon,ZShoriz_latxlon)
  PShoriz_slatxlon = calculatePS(SLPhoriz_slatxlon,ZShoriz_slatxlon)
  PShoriz_latxslon = calculatePS(SLPhoriz_latxslon,ZShoriz_latxslon)

  Thoriz_h1 = calculateT(SLThoriz_h1,PShoriz_latxlon)

  ;******************************************
  ; Clean memory of unneeded vars
  ;******************************************
  delete(Ucfsv2)
  delete(Vcfsv2)
  delete(Tcfsv2)
  delete(Qcfsv2)
  delete(PScfsv2_f_h1)
  delete(fpgbh)
  delete(fflxf)
  delete(ftopo)
  delete(ZScfsv2_f_h1)
  delete(ZShoriz_latxlon)
  delete(ZShoriz_slatxlon)
  delete(ZShoriz_latxslon)
  delete(SLTcfsv2_f_h1)
  delete(Tcfsv2_f_h1)

  ;******************************************
  ; Regrid vertically to the CAM grid
  ;******************************************
  ;Load fields for calculation of the 3D CAM pressure field
  fwgt = addfile(wgtFile_h_latxlon,"r")
  hyam = fwgt->hyam
  hybm = fwgt->hybm
  p0 = fwgt->P0

  ;Calculate dimension sizes for the CAM grid
  dsizeT = dimsizes(Thoriz)
  dsizeUS = dimsizes(UShoriz)
  dsizeVS = dimsizes(VShoriz)
  dsize_latxlon = (/dimsizes(hyam),dsizeT(1),dsizeT(2)/)
  dsize_slatxlon = (/dimsizes(hyam),dsizeUS(1),dsizeUS(2)/)
  dsize_latxslon = (/dimsizes(hyam),dsizeVS(1),dsizeVS(2)/)

  ;Load the CFSv2 level variable
  Pcfsv2 = fwgt->lv_ISBL0
  ;Create a couple 3D versions of this variable
  Pcfsv2_latxlon = conform_dims(dsizeT,Pcfsv2,(/0/))
  Pcfsv2_slatxlon = conform_dims(dsizeUS,Pcfsv2,(/0/))
  Pcfsv2_latxslon = conform_dims(dsizeVS,Pcfsv2,(/0/))

  ;Filter out levels below the hybrid 1 level, since these had to have been
  ;extrapolated
  H13D_latxlon = conform_dims(dimsizes(Pcfsv2_latxlon),cfsv2HybSig1*PShoriz_latxlon,(/1,2/))
  H13D_slatxlon = conform_dims(dimsizes(Pcfsv2_slatxlon),cfsv2HybSig1*PShoriz_slatxlon,(/1,2/))
  H13D_latxslon = conform_dims(dimsizes(Pcfsv2_latxslon),cfsv2HybSig1*PShoriz_latxslon,(/1,2/))
  Thoriz@_FillValue = -9999.
  Qhoriz@_FillValue = -9999.
  UShoriz@_FillValue = -9999.
  VShoriz@_FillValue = -9999.
  if(all((Pcfsv2_latxlon.ge.H13D_latxlon).eq.False))then
    print("Warning: No pressure values found below H1 level.")
  end if
  Thoriz = where(Pcfsv2_latxlon.ge.H13D_latxlon,Thoriz@_fillValue,Thoriz)
  Qhoriz = where(Pcfsv2_latxlon.ge.H13D_latxlon,Qhoriz@_fillValue,Qhoriz)
  UShoriz = where(Pcfsv2_slatxlon.ge.H13D_slatxlon,UShoriz@_fillValue,UShoriz)
  VShoriz = where(Pcfsv2_latxslon.ge.H13D_latxslon,VShoriz@_fillValue,VShoriz)

  delete(H13D_latxlon)
  delete(H13D_slatxlon)
  delete(H13D_latxslon)

  if(beVerbose)then
    print("Concatenating hybrid level 1 variables onto 3D dynamic vars")
  end if
  ;Concatenate the hybrid level 1 variables onto the regriddided
  ;dynamic variables so that we have data down near to the surface
  Thoriz := concatenateLevel(Thoriz,Thoriz_h1)
  Qhoriz := concatenateLevel(Qhoriz,Qhoriz_h1)
  UShoriz := concatenateLevel(UShoriz,UShoriz_h1)
  VShoriz := concatenateLevel(VShoriz,VShoriz_h1)
  Pcfsv2_latxlon := concatenateLevel(Pcfsv2_latxlon,cfsv2HybSig1*PShoriz_latxlon)
  Pcfsv2_slatxlon := concatenateLevel(Pcfsv2_slatxlon,cfsv2HybSig1*PShoriz_slatxlon)
  Pcfsv2_latxslon := concatenateLevel(Pcfsv2_latxslon,cfsv2HybSig1*PShoriz_latxslon)


  if(beVerbose)then
    print("Calculating the 3D CAM pressure fields for interpolation.")
  end if
  ;Calculate the 3D CAM pressure field on the latxlon grid
  ps3D := conform_dims(dsize_latxlon,PShoriz_latxlon,(/1,2/))
  hyam3D := conform_dims(dsize_latxlon,hyam,(/0/))
  hybm3D := conform_dims(dsize_latxlon,hybm,(/0/))
  P3D_latxlon = hyam3D*p0 + hybm3D*ps3D

  ;Calculate the 3D CAM pressure field on the latxlon grid
  ps3D := conform_dims(dsize_slatxlon,PShoriz_slatxlon,(/1,2/))
  hyam3D := conform_dims(dsize_slatxlon,hyam,(/0/))
  hybm3D := conform_dims(dsize_slatxlon,hybm,(/0/))
  P3D_slatxlon = hyam3D*p0 + hybm3D*ps3D

  ;Calculate the 3D CAM pressure field on the latxslon grid
  ps3D := conform_dims(dsize_latxslon,PShoriz_latxslon,(/1,2/))
  hyam3D := conform_dims(dsize_latxslon,hyam,(/0/))
  hybm3D := conform_dims(dsize_latxslon,hybm,(/0/))
  P3D_latxslon = hyam3D*p0 + hybm3D*ps3D

  if(beVerbose)then
    print("Doing vertical interpolation")
  end if
  ;Do the vertical interpolation
  UScam = int2p_n(Pcfsv2_slatxlon,UShoriz,P3D_slatxlon,2,0)
  VScam = int2p_n(Pcfsv2_latxslon,VShoriz,P3D_latxslon,2,0)
  Qcam = int2p_n(Pcfsv2_latxlon,Qhoriz,P3D_latxlon,2,0)
  Tcam = int2p_n(Pcfsv2_latxlon,Thoriz,P3D_latxlon,1,0)

  if(beVerbose)then
    print("Inferring cloud water and amount")
  end if
  RHcam = rhfromstate(P3D_latxlon,Tcam,Qcam)
  ;Calculate supersaturation
  SScam = where(RHcam.gt.1.and.RHcam.lt.2,RHcam - 1.0,0.0)
  print("Supersaturation maximum: " + max(SScam))
  ;Calculate cloud liquid water
  QCcam = Qcam
  QCcam = doubletofloat(SScam)*Qcam
  ;Remove the liquid water from the total
  Qcam = Qcam - QCcam

  CLDcam = parkCloudFraction(RHcam,P3D_latxlon,Qcam)
  
  if(beVerbose)then
    print("Writing to " + outFile)
  end if
  fout = addfile(outFile,"w")
  ;Create a cloud variable if necessary
  if(.not.isfilevar(fout,"CLOUD"))then
    CLDcam@units = "fraction"
    CLDcam@long_name = "Cloud fraction"
    filevardef(fout,"CLOUD",typeof(CLDcam),(/"time","lev","lat","lon"/))
    filevarattdef(fout,"CLOUD",CLDcam)
  end if
  ;Create a cloud water variable if necessary
  if(.not.isfilevar(fout,"QCWAT"))then
    QCcam@units = "kg/kg"
    QCcam@long_name = "q associated with cloud water"
    filevardef(fout,"QCWAT",typeof(QCcam),(/"time","lev","lat","lon"/))
    filevarattdef(fout,"QCWAT",QCcam)
  end if
  if(getfilevartypes(fout,"US").eq."float")then
    fout->US(0,:,:,:) = (/dble2flt(UScam)/)
  else
    fout->US(0,:,:,:) = (/UScam/)
  end if
  if(getfilevartypes(fout,"VS").eq."float")then
    fout->VS(0,:,:,:) = (/dble2flt(VScam)/)
  else
    fout->VS(0,:,:,:) = (/VScam/)
  end if
  if(getfilevartypes(fout,"T").eq."float")then
    fout->T(0,:,:,:) = (/dble2flt(Tcam)/)
  else
    fout->T(0,:,:,:) = (/Tcam/)
  end if
  if(getfilevartypes(fout,"Q").eq."float")then
    fout->Q(0,:,:,:) = (/dble2flt(Qcam)/)
  else
    fout->Q(0,:,:,:) = (/Qcam/)
  end if
  if(getfilevartypes(fout,"PS").eq."float")then
    fout->PS(0,:,:) = (/dble2flt(PShoriz_latxlon)/)
  else
    fout->PS(0,:,:) = (/PShoriz_latxlon/)
  end if
  if(getfilevartypes(fout,"QCWAT").eq."float")then
    fout->QCWAT(0,:,:,:) = (/dble2flt(QCcam)/)
  else
    fout->QCWAT(0,:,:,:) = (/QCcam/)
  end if
  if(getfilevartypes(fout,"CLOUD").eq."float")then
    fout->CLOUD(0,:,:,:) = (/dble2flt(CLDcam)/)
  else
    fout->CLOUD(0,:,:,:) = (/CLDcam/)
  end if

  if(beVerbose)then
    print("Writing to " + camSSTFile)
  end if
  ;Output regridded SST and ice into the SST file
  fsst = addfile(camSSTFile,"w")
  do i = 0,11
    fsst->SST_cpl(i,:,:) = (/TShoriz/)
    fsst->ice_cov(i,:,:) = (/ICEhoriz/)
  end do

  if(beVerbose)then
    print("Done.")
  end if
  
status_exit(0)
end
status_exit(1)
