load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "$CFSNCL/addVarToFile.ncl"

begin

;  These should be specified by the wrapper script
;  gridFileName = "cami-mam3_0000-01-01_ne30np4_L30_c130424.nc"
;  gridDirectory = "./"
;  camDomainName = "ne30np4"

  ;Open the CAM SE grid file
  fgrid = addfile(gridFileName,"r")
  lat = fgrid->lat
  lon = fgrid->lon

  ;Set some generation options
  opt = True
  opt@ForceOverwrite = True
  opt@PrintTimings = True
  opt@Debug = True

  scripFileName = gridDirectory+"/cam-"+camDomainName+"-ESMF.nc"
  opt@Title = "CAM "+camDomainName+" grid description (ESMF format)"
  ;Generate the ESMF grid description file 
  unstructured_to_ESMF(scripFileName,lat,lon,opt)
  ;Add variables describing the vertical grid to the file
  addVarToFile(scripFileName,fgrid->hyam,"hyam")
  addVarToFile(scripFileName,fgrid->hybm,"hybm")
  addVarToFile(scripFileName,fgrid->P0,"P0")
end
