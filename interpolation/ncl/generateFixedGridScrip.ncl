load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

begin

  ;This should be set by the wrapper script
  ;gridFileName = "/projects/data/OISST/sst.wkmean.1990-present.nc"
  ;gridDirectory = "./"

  ;*****************************************
  ; Generate the 0.5 degree grid file
  ;*****************************************
  scripFileName = gridDirectory+"/cam-"+camDomainName+"_gridded-latxlon-SCRIP.nc"

  fgrid = addfile(gridFileName,"r")
  lat = fgrid->lat
  lon = fgrid->lon

  opt = True
  opt@ForceOverwrite = True
  opt@Title = "Lat/lon grid at approximate resolution of " + camDomainName
  opt@PrintTimings = True
  opt@Debug = True

  rectilinear_to_SCRIP(scripFileName,lat,lon,opt)

end
