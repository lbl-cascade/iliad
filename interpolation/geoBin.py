#!/usr/bin/env python
from numpy import *
from fortranModuleSource.mod_ftnbin import mod_ftnbin as ftnbin

class geoBin:

  def __init__(self,binMin,binMax,numBins=30,bCosBins=False,deltaBin=0,inDegrees=True):
    """ Create a set of bins with boundaries between binMin and binMax
    """
    
    #Set whether these bins are spaced such that cos(binCenter)*deltaBin is the
    #same for each bin (i.e., equal area latitude bins): bins are even in sin(bin) space
    self.isSinSpaced = bCosBins

    if(inDegrees):
      self.conversionFactor = pi/180.
    else:
      self.conversionFactor = 1.

    if(binMin > binMax):
      self.isFlipped = True
    else:
      self.isFlipped = False
  
    if(self.isSinSpaced):
      #First specify the bin boundaries in sin-space
      sinBinMin = sin(binMin*self.conversionFactor)
      sinBinMax = sin(binMax*self.conversionFactor)
      #Set deltaSinBin as deltaBin (gives deltaBin ~= deltaSinBin for bins
      #centered near 0)
      if(deltaBin != 0):
        deltaSinBin = deltaBin*self.conversionFactor
        numBins = len(arange(sinBinMin,sinBinMax,deltaSinBin))
      
      #Create evenly-spaced bins in sin space
      sinBinEdges = linspace(sinBinMin,sinBinMax,numBins+1)
      sinLeftEdge = sinBinEdges[:-1]
      sinRightEdge = sinBinEdges[1:]
      sinCenter = 0.5*(sinLeftEdge + sinRightEdge)

      #Set the bin edges and centers (convert back to real space)
      self.leftEdge = arcsin(sinLeftEdge)/self.conversionFactor
      self.rightEdge = arcsin(sinRightEdge)/self.conversionFactor
      #self.center = arcsin(sinCenter)/self.conversionFactor
      self.center = 0.5*(self.leftEdge + self.rightEdge)
    else:
      #If the bin spacing wasn't specified, estimate it from numBins
      if(deltaBin != 0):
        numBins = len(arange(binMin,binMax,deltaBin))

      binEdges = linspace(binMin,binMax,numBins+1)
      self.leftEdge = binEdges[:-1]
      self.rightEdge = binEdges[1:]
      self.center = 0.5*(self.leftEdge + self.rightEdge)

    #Set the bin count
    self.count = len(self.center)
    #Set the bin widths
    self.spacing = self.rightEdge - self.leftEdge

    #Set the spacing to pass to findbinindex for speed
    if(self.isSinSpaced):
      self.deltaBin = sin(self.rightEdge[0]) - sin(self.leftEdge[0])
    else:
      self.deltaBin = self.spacing[0]

  def getIndex(self,val):
    """ Return the bin in which the given value, val, falls """

    if(self.isFlipped):
      ibin = ftnbin.findbinindex(val,  \
                            self.rightEdge[::-1],  \
                            self.leftEdge[::-1], \
                            abs(self.deltaBin), \
                            blinlog = False,  \
                            blinsin = self.isSinSpaced) - 1
      ibin = self.count - ibin - 1
    else:
      ibin = ftnbin.findbinindex(val,  \
                            self.leftEdge,  \
                            self.rightEdge, \
                            self.deltaBin, \
                            blinlog = False,  \
                            blinsin = self.isSinSpaced) - 1

      
    return ibin
if( __name__ == "__main__"):
  mybin = geoBin(-90.,90.,numBins = 90,bCosBins=True)
  #mybin = Bin(1,128,numBins=1,bCosBins=True)

  for i in range(mybin.count):
    k = mybin.getIndex(1.0*mybin.center[i])
    print "{},{}:\t{}\t|\t{}\t|\t{}\t--{}".format(i,k,mybin.leftEdge[i],mybin.center[i],mybin.rightEdge[i],mybin.conversionFactor*mybin.spacing[i]*cos(mybin.conversionFactor*mybin.center[i]))

  mybin = geoBin(0,360.,numBins=180,bCosBins=False)
  #mybin = Bin(1,128,numBins=1,bCosBins=True)

  for i in range(mybin.count):
    k = mybin.getIndex(1.0*mybin.center[i])
    print "{},{}:\t{}\t|\t{}\t|\t{}\t--{}".format(i,k,mybin.leftEdge[i],mybin.center[i],mybin.rightEdge[i],mybin.spacing[i])
