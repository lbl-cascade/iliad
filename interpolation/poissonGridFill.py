from numpy import *
from fortranModuleSource.fill_msg_grid import poisxy1

def poisson_grid_fill(  inputArray,                         \
                        missingValue,                       \
                        useRowMeanGuess = True,             \
                        numberOfIterations = 1500,          \
                        errorTolerance = 1e-2,              \
                        relaxationConstant = 0.6,           \
                        gridIsPeriodicInX = True,           \
                    ):
    """Fills missing values in a 2D array by solving the Poisson equation for the missing value domain.

        Input Arguments:
        ----------------

            inputArray              :   A two-dimensional numeric array

            missingValue            :   The value that flags cells in inputArray as missing

            useRowMeanGuess         :   Specifies whether to initialize the
                                        missing values with their row mean (uses 0.0 if False)

            numberOfIterations      :   The maximum number of iterations to converge on a solution to
                                        the Poisson equation

            errorTolerance          :   The tolerance for ending iterations before numberOfIterations

            relaxationConstant      :   The relaxation coefficient; usually between 0.45 and 0.6

            gridIsPeriodicInX       :   Specifices whether the array is periodic in its first dimension

        Output:
        -------

        A copy of inputArray with missing values replaced with an approximate solution to the Poisson equation.

        Raises:
        -------

        ValueError if inputArray is not a 2D arraylike object

    """

    try:
        inputArray = array(inputArray)
    except BaseException as e:
        print "inputArray is not arraylike; calling array(inputArray) raised an exception:\n\t{}".format(e)
        raise ValueError


    if(len(shape(inputArray)) != 2):
        raise ValueError,"inputArray must be twodimensional"

    actualIterations = 0
    outArray = poisxy1( xio = transpose(inputArray), \
                        xmsg = missingValue, \
                        guess = int(useRowMeanGuess), \
                        gtype = int(gridIsPeriodicInX), \
                        nscan = numberOfIterations, \
                        epsx = errorTolerance, \
                        relc = relaxationConstant, \
                        mscan = actualIterations)

    return transpose(outArray)

if __name__ == "__main__":

    import netCDF4 as nc
    import Nio
    from iliad.utilities import ncl
    import os

    outfileName = "test_poissionGridFill.nc"
    nclOutfileName = "test_poissionGridFill_ncl.nc"
    fieldName = "TMP_P0_L1_GGA0"
    maskName = "LAND_P0_L1_GGA0"
    fillValue = -9999.

    try:
      import sys
      infileName = sys.argv[1]
      fin = Nio.open_file(infileName,"r")
    except:
      raise ValueError,"Please give the name of a CFS flx grib file as the argument to this script"

    inField = fin.variables[fieldName][:]
    mask = fin.variables[maskName][:]

    #Find land values
    indLand = nonzero(mask.ravel() != 0.0)[0]
    #Set the land values to missing
    inField.ravel()[indLand] = fillValue

    filledField = poisson_grid_fill(inField,fillValue)

    #Write the results to a netCDF file
    fout = nc.Dataset(outfileName,"w") 
    dimTuple = ()
    for r in range(len(shape(filledField))):
        dimName = "dim_{}".format(r)
        dimTuple += (dimName,)
        fout.createDimension(dimName,shape(filledField)[r])

    fout.createVariable(fieldName,'f8',dimTuple)
    fout.variables[fieldName][:] = filledField

    fout.close()

    nclCode = """

begin
    
    fin = addfile("{inputFile}","r")
    inField = fin->{varName}
    maskField = fin->{maskName}

    inField@_FillValue = {fillValue}

    inField = where(maskField.ne.0.0,inField@_FillValue,inField)
    poisson_grid_fill(inField,True,1,1500,1e-2,0.6,0)

    outfilename = "{outFile}"
    if(isfilepresent(outfilename))then
        system("rm " + outfilename)
    end if
    fout = addfile(outfilename,"c")
    fout->{varName} = inField

status_exit(0)
end
status_exit(1)

""".format(inputFile = infileName, varName = fieldName, outFile = nclOutfileName, maskName = maskName,fillValue = fillValue)

    nclOutput = ncl.execute(nclCode)

    ftest = nc.Dataset(nclOutfileName,"r")
    nclField = ftest.variables[fieldName]

    fieldDifference = nclField[:] - filledField[:]

    maxDiff = amax(abs(fieldDifference))
    avgDiff = average(fieldDifference)

    print "The filled field differs from the NCL-filled field by at most {} and by average {}".format(maxDiff,avgDiff)
    
    
