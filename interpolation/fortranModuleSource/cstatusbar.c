#include <stdio.h>
#include <string.h>

#define BARWIDTH 50

void cstatusbar_(float fPercent)
{
	/*Declare a character of width BARWIDTH*/
	char cBar[BARWIDTH];
	/*Set the indicator position to 0*/
	int ipos = 0;
	/*Integer version of fPercent*/
	int iPercent=0;

	/*Verify that the given percent is a valid fraction between 0 and 1*/
	/*(inclusive)*/
	if(!((fPercent >= 0) && (fPercent <= 1))){return -1;}

	/*Set the indicator position to be the closest integer to */
	/*fPercent of the status bar width*/
	ipos = (int)(fPercent*BARWIDTH);
	/*Fill in the bar with '#' up to the indicator position*/
	/*and blank it in other places*/
	/*TODO: Could this be done with some form of memcpy?*/
	memset(cBar,'#',ipos);
	memset(&cBar[ipos],' ',BARWIDTH-ipos);

	/*Terminate the bar string*/
	cBar[BARWIDTH] = '\0';

	/*A printable version of fPercent*/
	iPercent = (int)(100*fPercent);

	/*Write the status bar, and terminate the line with a carriage*/
	/*return (\xd) as opposed to a newline*/
	printf("[%s] %i%%\xd",cBar,iPercent);
	fflush(stdout);
	
	/*Return success*/
	return;
}
