module mod_ftnweights
  implicit none

  contains


  subroutine applyweights2d(  &
                            cfsrField,  &
                            weightMap,  &
                            clmField,   &
                            fillValue,  &
                            nlat,       &
                            nlon,       &
                            nhalo,      &
                            ncolumn)
  implicit none
    !Input variables
    integer,intent(in) :: nlat,nlon,nhalo,ncolumn
    double precision,dimension(nlat,nlon),intent(in) :: cfsrField
    double precision,dimension(ncolumn,nhalo,3),intent(in) :: weightMap
    double precision, intent(in) :: fillValue
    !Output variables
    double precision,intent(out),dimension(ncolumn) :: clmField
    !Local variables
    integer :: i,j,h,n,ninterp
    double precision :: wij,wsum
    double precision,parameter :: wsumtolerance = 1e-5

    columnloop: &
    do n = 1,ncolumn
      !Reset the interpolation field
      clmField(n) = 0.0d0
      !Reset the weight field
      wsum = 0.0d0
      !Reset the number-of-interpolants tracker
      ninterp = 0
      haloloop: &
      do h = 1,nhalo
       !If we have reached a negative weight, this flags that there are no more valid halo points for this column
       ! so we should break out of this loop since interpolation for the current n is done.
       wij = weightMap(n,h,3)
       if(wij < 0 )exit haloloop

       !Set the i/j index for the current halo point
       i = nint(weightMap(n,h,1)) + 1
       j = nint(weightMap(n,h,2)) + 1
       !Check the bounds of the current i/j index
       if(i > nlat .or. j > nlon .or. i < 1 .or. j < 1)then
         write(*,*)"Error: mapping indices out of bounds: i,j,nlat,nlon",i,j,nlat,nlon
         write(*,*)"       weightMap = ",weightMap(n,h,:)
         stop
       end if
       !If the input point isn't missing, include it in the interpolation
       !Note: if a value is missing, then wsum won't be 1 as assumed, so the interpolation
       !has to be renormalized
       if(cfsrField(i,j) /= fillValue)then
         !Increment the interpolant tracker
         ninterp = ninterp + 1
         !Track the running weight sum (of used weights)
         wsum = wsum + wij
         !Add the halo point's (weighted) value to the interpolation
         clmField(n) = clmField(n) + wij*cfsrField(i,j)
       end if
      end do haloloop
      !If the sum of weights is really far off then renormalize (i.e., b/c of a missing value)
      if((abs(wsum-1.0d0) > wsumtolerance).and.(wsum /= 0.0d0))clmField(n) = clmField(n)/wsum
      !If no interpolating values were provided, flag the field as missing
      if(ninterp.eq.0)clmField(n) = fillValue
      !if(wsum /= 0.0d0)clmField(n) = clmField(n)/wsum
    end do columnloop

    return

  end subroutine applyweights2d

  subroutine applyweights3d(  &
                            cfsrField,  &
                            weightMap,  &
                            clmField,   &
                            fillValue,  &
                            nlat,       &
                            nlon,       &
                            nlev,       &
                            nhalo,      &
                            ncolumn,    &
                            nlevclm,    &
                            ncell,      &
                            beverbose)
  implicit none
    !Input variables
    integer,intent(in) :: nlat,nlon,nlev,nhalo,ncell,ncolumn,nlevclm
    double precision,dimension(nlev,nlat,nlon),intent(in) :: cfsrField
    double precision,dimension(ncell,nhalo,6),intent(in) :: weightMap
    double precision, intent(in) :: fillValue
    logical,intent(in) :: beverbose
    !Output variables
    double precision,intent(out),dimension(ncolumn,nlevclm) :: clmField
    !Local variables
    integer :: i,j,k,h,n,nc,kc,ninterp
    double precision :: wijk,wsum
    double precision,parameter :: wsumtolerance = 1e-5
    logical :: doWarn
    real :: fracDone

    !Reset the interpolation field
    clmField = 0.0d0

    cellloop: &
    do n = 1,ncell
      if(beverbose)then
        fracDone = float(n)/float(ncell)
        !call cstatusbar_(fracDone)
      endif

      !Reset the weight field
      wsum = 0.0d0
      !Reset the number-of-interpolants tracker
      ninterp = 0
      haloloop: &
      do h = 1,nhalo
       !If we have reached a negative weight, this flags that there are no more valid halo points for this column
       ! so we should break out of this loop since interpolation for the current n is done.
       wijk = weightMap(n,h,6)
       if(wijk < 0 )exit haloloop

       !Set the nc/kc index for the CLM grid
       nc = nint(weightMap(n,h,1)) + 1
       kc = nint(weightMap(n,h,2)) + 1

       !Set the i/j index for the current halo point
       i = nint(weightMap(n,h,3)) + 1
       j = nint(weightMap(n,h,4)) + 1
       k = nint(weightMap(n,h,5)) + 1
       !Check the bounds of the current i/j/k index
       if(i > nlat .or. j > nlon .or. k > nlev .or. i < 1 .or. j < 1 .or. k < 1)then
         write(*,*)"Error: mapping indices out of bounds: i,j,k,nlat,nlon,nlev",i,j,k,nlat,nlon,nlev
         write(*,*)"       weightMap = ",weightMap(n,h,:)
         stop
       end if
       !Check the bounds of the current nc/kc index
       if(nc > ncolumn .or. kc > nlevclm .or. nc < 1 .or. kc < 1)then
         write(*,*)"Error: mapping indices out of bounds: nc,kc,ncolumn,nlevclm",nc,kc,ncolumn,nlevclm
         write(*,*)"       weightMap = ",weightMap(n,h,:)
         stop
       end if
       !If the input point isn't missing, include it in the interpolation
       !Note: if a value is missing, then wsum won't be 1 as assumed, so the interpolation
       !has to be renormalized
       if(cfsrField(k,i,j) /= fillValue)then
         !Increment the interpolant tracker
         ninterp = ninterp + 1
         !Track the running weight sum (of used weights)
         wsum = wsum + wijk
         !Add the halo point's (weighted) value to the interpolation
         clmField(nc,kc) = clmField(nc,kc) + wijk*cfsrField(k,i,j)
       end if
      end do haloloop
      !If the sum of weights is really far off then renormalize (i.e., b/c of a missing value)
      if((abs(wsum-1.0d0) > wsumtolerance).and.(wsum /= 0.0d0))then
        clmField(nc,kc) = clmField(nc,kc)/wsum
      end if
      !If no interpolating values were provided, flag the field as missing
      if(ninterp.eq.0)clmField(nc,kc) = fillValue
      !if(wsum /= 0.0d0)clmField(nc,kc) = clmField(nc,kc)/wsum
    end do cellloop

    return

  end subroutine applyweights3d

  subroutine applyesmfweights(inputfield,         &
                              outputfield,        &
                              inputindices,       &
                              outputindices,      &
                              weightvalues,       &
                              fractionaloverlap,  &
                              missingvalue,       &
                              handlemissingvalues, &
                              nval,                &
                              nfrac)
    implicit none

    !********************
    ! Input variables
    !********************
    integer,intent(in) :: nval, nfrac
    double precision,dimension(:),intent(in) :: inputfield
    double precision,dimension(nval),intent(in) :: weightvalues
    integer,dimension(nval),intent(in) :: inputindices, outputindices
    integer,dimension(nfrac) :: fractionaloverlap
    double precision,intent(in) :: missingvalue
    logical,intent(in) :: handlemissingvalues
    !********************
    ! Output variables
    !********************
    !NOTE: it is assumed that outputfield is predeclared as 0 everywhere
    double precision,dimension(:),intent(inout) :: outputfield  !The output field
    !********************
    ! Local variables
    !********************
    integer :: n,i,j

    if(handlemissingvalues)then
      !Do the sparse matrix multiplication that interpolates the input
      !field to the output grid
      weightloopmissing:  &
      do n = 1,nval
        j = outputindices(n)
        i = inputindices(n)
        !If any of the input are missing, set the output field at this index to missing
        if(inputfield(i) == missingvalue .or. outputfield(j) == missingvalue )then
          outputfield(j) = missingvalue
          cycle
        endif
        outputfield(j) = outputfield(j) + weightvalues(n)*inputfield(i)

        !if(outputfield(j) < 0)write(*,*)outputfield(j),inputfield(i)
      end do weightloopmissing
    else
      !Do the sparse matrix multiplication that interpolates the input
      !field to the output grid
      weightloop: do n = 1,nval
        j = outputindices(n)
        i = inputindices(n)
        outputfield(j) = outputfield(j) + weightvalues(n)*inputfield(i)
      end do weightloop
    endif


    !Adjust the output field to account for fractional overlap (this only does something if
    !conservative interpolation was used)
    fracloop: do j = 1,nfrac
      if(fractionaloverlap(j) /= 0.0)then
        if(.not.(handlemissingvalues .and. outputfield(j) == missingvalue))then
          outputfield(j) = outputfield(j)/fractionaloverlap(j)
        endif
      end if
    end do fracloop

  end subroutine applyesmfweights


end module mod_ftnweights
