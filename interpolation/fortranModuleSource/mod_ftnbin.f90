module mod_ftnbin
  implicit none

  contains

  !Given a bin value, finds the index of the bin into which the value should fall.
  ! Satisfies the condition dBinMin1D(n) <= dBinVal < dBinMax1D(n), where n is the
  ! return bin index.
  ! Assumes that the bins are linearly spaced either in linear or log space.
  function findBinIndex(  &
                        dBinVal,    & !The value to bin
                        dBinMin1D,  & !The left bin edges
                        dBinMax1D,  & !The right bin edges
                        dDeltaBin,  & !The (approximate) bin spacing
                        nbins,      & !The number of bins
                        bLinLog,    & !Bins are linearly spaced in log space
                        bLinSin     & !Bins are linearly spaced in sin space
                       )
  implicit none
    !Input variables
    !f2py integer,intent(hide),depend(dBinMin1D) :: nbins = len(dBinMin1D)
    integer,intent(in) :: nbins
    !f2py double precision,intent(in) :: dBinVal
    double precision,intent(in) :: dBinVal,dDeltaBin
    !f2py double precision,intent(in),dimension(nbins) :: dBinMin1D,dBinMax1D
    double precision,dimension(nbins),intent(in) :: dBinMin1D,dBinMax1D
    !f2py logical,intent(in),optional :: bLinLog = 1,bLinSin = 0
    logical,intent(in) :: bLinLog,bLinSin
    !Output variable
    !f2py integer,intent(out) :: findBinIndex
    integer :: findBinIndex
    !Local variables
    double precision :: dApproxInd
    integer :: n

      findBinIndex = -1

      if(dBinMin1D(1).lt.dBinMin1D(nbins))then
        !First check if we are out of the bin boundaries
        if(dBinVal < dBinMin1D(1))then
          findBinIndex = 1
          return
        end if
        if(dBinVal >= dBinMax1D(nbins))then
          findBinIndex = nbins
          return
        end if
      else !The bins are reversed
        !First check if we are out of the bin boundaries
        if(dBinVal > dBinMin1D(1))then
          findBinIndex = 1
          return
        end if
        if(dBinVal <= dBinMax1D(nbins))then
          findBinIndex = nbins
          return
        end if
      end if


      !Take a first guess at estimating the bin
      if(bLinLog)then
        dApproxInd = (log(dBinVal)-log(dBinMin1d(1)))/dDeltaBin + 1
      elseif(bLinSin)then
        dApproxInd = (sin(dBinVal)-sin(dBinMin1d(1)))/dDeltaBin + 1
      else
        dApproxInd = (dBinVal-dBinMin1d(1))/dDeltaBin + 1
      end if

      n = min(max(int(floor(dApproxInd)),1),nbins)

      if( (dBinVal >= dBinMin1D(n)) .and. &
          (dBinVal < dBinMax1D(n)) )then
        findBinIndex = n
        return
      else
        !If the above method didn't work, then loop through the bins
        !and find the right range
        do n = 1,nbins
          if( (dBinVal >= dBinMin1D(n)) .and. &
              (dBinVal < dBinMax1D(n)) )then
            findBinIndex = n
            return
          end if
        end do
      end if

      !It shouldn't be possible to reach this point, but return -1 if we do
      findBinIndex = -1

      return

    end function findBinIndex

end module mod_ftnbin
