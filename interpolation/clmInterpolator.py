#!/usr/bin/env python
from numpy import *
import geoBin
import netCDF4 as nc
import Nio
from iliad.physics import earthPhysics as phys
from fortranModuleSource.mod_ftnweights import mod_ftnweights as ftnweights
import pdb

class clmInterpolator():
  """
    Creates an object that interpolates values from a 2D/3D CFSR grid to a
    2D/3D CLM-like grid.

    The class performs two major operations: generating mapping weights and
    applying mapping weights (interpolating).  Since the generation of mapping
    weights is computationally intensive, the class can be configured to write
    a weight file that the class can later read.  Therefore the initialization
    of this class has two major operating modes:

    1)  Generating weights.

        In this operating mode, a CFSR netCDF grid file (with lat/lon
        information) must be supplied along with a CLM netCDF grid file (with
        lat/lon information).  Mapping weights are generated based on these
        grids, and they will be saved to a netCDF file if a weight file name is
        supplied.

        Optionally, interpolation can proceed after this stage by using the
        clmInterpolator.doInterpolation() routine.
        
        Presently, the 2D horizontal interpolation scheme is an
        inverse-distance weighting scheme limited to a 3x3 halo of CFSR points
        surrounding a CLM point and no extrapolation, and the vertical
        interpolation is a simple linear interpolation with constant-value
        extrapolation beyond the interpolation limits.  For interpolation of 3D
        variables, the mapping weights from these interpolation schemes are
        combined into a single, sparse 3D array.  The 2D and 3D weight mappings
        are stored in memory and in the netCDF files a array of subsections of
        the sparse 2D/3D weight arrays.

        Example:

            myInterp = clmInterpolator( cfsrFile = "cfsr-0.3deg-latxlon.nc", \
                                        clmFile = "clmi.IQmp17_2000-01-01_1.9x2.5_gx1v6_simyr2000_c090509.nc", \
                                        weightFile = "wgts_CFSR0.3deg-to-CLM-fv1.9x2.5-latxlon.nc")

            ...

            interpolatedSoilTemp = myInterp.doInterpolation(cfsrSoilTemp)

    2)  Reading weights and interpolating.

        In this operating mode, a grid file name is supplied and the mapping
        arrays in the clmInterpolator are intialized based on the data in the
        weight file.  Interpolation normally proceeds after this point.

        Example:
            
            myInterp = clmInterpolator( weightFile = "wgts_CFSR0.3deg-to-CLM-fv1.9x2.5-latxlon.nc")
        
            ...

            interpolatedSoilTemp = myInterp.doInterpolation(cfsrSoilTemp)
            
  """
    #TODO: Describe arguments in the docstring above

  def __init__(self,cfsrFile=None,clmFile=None,weightFile=None,fillValue=-1e20,beVerbose=True,ilevCLM=None,levCLM=None,thkCLM=None):

    self.beVerbose = beVerbose

    if(fillValue > 0):
      raise ValueError("fillValue must be negative for the regridding operation to function.")
    self.fillValue = fillValue

    if(cfsrFile != None and clmFile != None):

        #********************************************************
        #Open the CFSR file and set the lat/lon/lev
        #********************************************************
        fCFSR = Nio.open_file(cfsrFile,"r")
        latCFSR = fCFSR.variables["lat_0"][:]
        lonCFSR = fCFSR.variables["lon_0"][:]
        #Specify the CFSR soil level variables, since these aren't saved in the CFSR file
        #Determine the CFSR levels
        thkCFSR = array([0.0,0.1,0.3,0.6,1.0]) # Level thickness (m)
        ilevCFSR = cumsum(thkCFSR) #Interface level depth (m)
        levCFSR = (ilevCFSR[1:] + ilevCFSR[:-1])/2. #Mid-level depth (m)
        #Remove the 0-index of the thickness array (which was necessary for cumsum to work)
        thkCFSR = thkCFSR[1:]

        #Save the CFSR coordinates
        self.latCFSR = latCFSR
        self.lonCFSR = lonCFSR
        self.levCFSR = levCFSR
        self.thkCFSR = thkCFSR

        #********************************************************
        #Open the CLM file and set the lat/lon/lev
        #********************************************************
        fCLM = nc.Dataset(clmFile,"r")
        latCLM = fCLM.variables["cols1d_lat"][:]
        lonCLM = fCLM.variables["cols1d_lon"][:]


        #If any of the CLM level specifiers weren't provided, default to the standard CLM vertical grid
        #TODO: ilevCLM isn't ever used.  Remove it.
        if(ilevCLM == None or levCLM == None or thkCLM == None):
            #Set the CLM level depths and thicknesses (not provided in netCDF file)
            #   these values were provided by Charlie Koven in a 11/22/13 e-mail, and they match the values
            #   provided in the CLM4.5 technote.
            ilevCLM = [0.000000000000000, 1.7512817916255204e-002, 4.5091787175931458e-002, 9.0561820418344652e-002, 0.1655292314045532, 0.2891295965068337, 0.4929121475172655, 0.8288927739656982, 1.382831179334383, 2.296121210923444, 3.801881912322721, 6.284461609304053, 10.37754356192545, 17.12589483993117, 28.25204513413559, 42.10319727609919] #Interface level depth (m)
            levCLM = [7.1006354171935350e-003, 2.7925000415316870e-002, 6.2258573936546040e-002, 0.1188650669001433, 0.2121933959089632, 0.3660657971047043, 0.6197584979298266, 1.038027050001570, 1.727635308667197, 2.864607113179692, 4.739156711465750, 7.829766507142356, 12.92532061670855, 21.32646906315379, 35.17762120511739] #Level depth (m)
            thkCLM = [1.7512817916255204e-002, 2.7578969259676251e-002, 4.5470033242413201e-002, 7.4967410986208557e-002, 0.1236003651022805, 0.2037825510104317, 0.3359806264484326, 0.5539384053686849, 0.9132900315890611, 1.505760701399277, 2.482579696981332, 4.093081952621400, 6.748351278005718, 11.12615029420442, 13.85115214196360] #Level thickness (m)

        #Save the CLM coordinates
        self.latCLM = latCLM
        self.lonCLM = lonCLM
        self.levCLM = levCLM
        self.thkCLM = thkCLM

        #********************************************************
        # Generate latitude bins
        #********************************************************
        #Set the edges of the latitude grid as the midpoints of the lat values
        latTmp = (latCFSR[:-1] + latCFSR[1:])/2.
        latEdges = zeros([len(latTmp)+2])
        latEdges[1:-1] = latTmp
        #Set the endpoints of the lat grid edges as +/- 90 degrees
        latEdges[0] = 90.0
        latEdges[-1] = -90.0
        #Create a geoBin instance with the right number of bins (the edge/center values will be overwritten)
        latBins = geoBin.geoBin(latEdges[0],latEdges[-1],numBins = len(latEdges)-1,bCosBins=False)
        #Set the left edges of the latitude bins
        latBins.leftEdge = latEdges[:-1]
        #Set the right edges of the latitude bins
        latBins.rightEdge = latEdges[1:]
        #Set the latitude bin centers
        latBins.center = latCFSR
        #Estimate the bin width from the width of the centermost bin
        centerBin = len(latCFSR)/2 + 1
        latBins.deltaBin = latEdges[centerBin] - latEdges[centerBin-1]

        #********************************************************
        # Generate longitude bins
        #********************************************************
        #Set the edges of the longitude grid as the midpoints of the lon values
        lonTmp = (lonCFSR[:-1] + lonCFSR[1:])/2.
        lonEdges = zeros([len(lonTmp)+2])
        lonEdges[1:-1] = lonTmp
        #Se the edges of the longitude grid as the midpoint of the wraparound of the opposite edge
        lonEdges[0] = ((lonCFSR[-1]-360.) + lonCFSR[0])/2.
        lonEdges[-1] = ((lonCFSR[0] + 360.) + lonCFSR[-1])/2.
        #Create a geoBin instance with the right number of bins (the edge/center values will be overwritten)
        lonBins = geoBin.geoBin(lonEdges[0],lonEdges[-1],numBins = len(lonEdges)-1,bCosBins = False)
        #Set the edges of the longitude bins
        lonBins.leftEdge = lonEdges[:-1]
        lonBins.rightEdge= lonEdges[1:]
        #Set the centers of the longitude bins
        lonBins.center = lonCFSR
        centerBin = len(lonCFSR)/2 + 1
        #Estimate the bin width from the width of the centermost bin
        lonBins.deltaBin = lonEdges[centerBin] - lonEdges[centerBin-1]

        #********************************************************
        # Generate depth bins
        #********************************************************
        #Create a geoBin instance with the right number of bins (the edge/center values will be overwritten)
        depthBins = geoBin.geoBin(ilevCFSR[0],ilevCFSR[-1],numBins = len(levCFSR)+1,bCosBins = False)
        #Set the leftmost/rightmost bin edges as +/- infinity.
        depthBins.leftEdge[0] = -inf
        depthBins.rightEdge[-1] = inf
        #Set the bin edges to be the CFSR levels
        depthBins.leftEdge[1:] = levCFSR
        depthBins.rightEdge[:-1] = levCFSR
        #Set the bin centers to be the interfaces of
        #the CFSR levels
        depthBins.center[1:-1] = ilevCFSR[1:-1]
        #Set the edge bin centers to +/- infinity
        depthBins.center[0] = -inf
        depthBins.center[-1] = inf

        #********************************************************
        # Calculate the mapping weights
        #********************************************************
        #Generate the depth mapping
        depthMap = self.generateLevMap(levCLM,depthBins)

        #Generate the 2D weight map
        weightMap = self.generateWeightMap(latCLM,lonCLM,latBins,lonBins)
        
        #Combine the two maps to get 3D weights
        combinedMap = self.combineWeights3D(weightMap,depthMap)

        #Convert the weight maps to numpy arrays
        self.weightMap2D = self.weightMapToNumpyArray(weightMap)
        self.weightMap3D = self.weightMapToNumpyArray(combinedMap)

        if(weightFile != None):
            self.writeWeightFile(weightFile)

    elif(weightFile != None):
      self.loadWeightFile(weightFile)
    else:
      raise ValueError("clmInterpolator was initialized with no valid arguments.")

    
  def generateWeightMap(self,latCLM,lonCLM,latBins,lonBins,exponent = 3.,distanceTolerance = 1e-8,hi=3,hj=3):
    """Sets the weights that map values on the CFSR horizontal grid to values on 
       the CLM horizontal (unstructured) grid, using an inverse distance
       weighting scheme.

       latCLM     :  The latitudes of the CLM columns

       lonCLM     :  The longitudes of the CLM columns
       
       latBins    :  A geoBin instance where the edges of the bins correspond
                     to the latitudinal edges of the CFSR grid cells.

       lonBins    :  A geoBin instance where the edges of the bins correspond
                     to the longitudinal edges of the CFSR grid cells.

       exponent   :  The exponent in the inverse distance scheme; i.e. weights 
                     are proportional to d**(-exponent), where d is the 
                     geophysical distance between the CLM point and a given CFSR 
                     point.  Optional.

       distanceTolerance : The minimum distance below which a weight of 1.0 is
                           assigned to the nearest CFSR point.  Optional.

       hi, hj     :  The grid size of the halo (in the latitudinal and 
                     longitudinal directions, respectively), to which the 
                     inverse-distance interpolation will be restricted.  
                     Optional.

       ----------

        Returns:
          A list of coordinate mapping triads.  Each list item contains one or
          more list of [i,j,weight] pairs, where i corresponds to a CFSR
          latitude index, j corresponds to a CFSR longitude index, and weight
          corresponds to the weight given to the CFSR value at [i,j].  For list
          items with more than one i/j/weight triad, the weights should sum to
          1.0.

    """

    #Initialize the weight mapping to a null list
    weightMap = []

    #Get the index of each CLM lat/lon pair
    iArrayCLM = [latBins.getIndex(val) for val in latCLM]
    jArrayCLM = [lonBins.getIndex(val) for val in lonCLM]

    #Set the half-widths of the halo
    hih = (hi-1)/2
    hjh = (hj-1)/2

    #Loop through this set of indicies
    for n,i,j in zip(range(len(iArrayCLM)),iArrayCLM,jArrayCLM):
      #Create a 3x3 array of i,j points surrounding the current i,j pair
      #(this distance-based interpolation is restricted to this ni x nj halo
      ii2d,jj2d = meshgrid(asarray(range(i-hih,i+hih+1)),asarray(range(j-hjh,j+hjh+1)))
      weightList = []
      #Loop over the given halo of points
      for ii,jj in zip(ii2d.ravel(),jj2d.ravel()):
        if(ii >= 0 and ii < latBins.count): 
          #Deal with longitudinal wrapping for the halo points
          if(jj < 0):
            jj = lonBins.count-jj
          if(jj >= lonBins.count):
            jj -= lonBins.count

          #Set the (radian) lats/lons of the the current CLM point
          lat1 = phys.degtorad*latCLM[n]
          lon1 = phys.degtorad*lonCLM[n]
          #Set the (radian) lats/lons of the the current halo point
          lat2 = phys.degtorad*latBins.center[ii]
          lon2 = phys.degtorad*lonBins.center[jj]
          #Set the longitudinal distance between the CLM and halo points
          dlon = lon2-lon1
          #If the longitudinal distance is greater than a half-circle, then choose the smaller part of the
          #great circle as the distance
          if(dlon > pi):
            dlon = 2*pi - dlon
          if(dlon < -pi):
            dlon = 2*pi + dlon
          #Calculate the great-circle distance between the CLM point and the halo point
          distance = arccos(sin(lat1)*sin(lat2) + cos(lat1)*cos(lat2)*dlon)
          #If the two points are close enough, then just give the current halo point a weight of 1.0
          #and break out of the halo loop
          if(distance < distanceTolerance):
            weightList = [[ii,jj,1.0]]
            break
          #Otherwise, set the inverse-distance weight, and add the i/j/weight triad to the current weight
          #list for the given CLM point
          else:
            weight = 1./distance**exponent
            weightList.append([ii,jj,weight])

          #Check for nanage
          if(isnan(weightList[-1][-1])):
            raise ValueError,"""
            NaN detected in generateWeightMap():
            Point 1: ({} N, {} W)
            Point 2: ({} N, {} W) 
            dlon = {}
            distance = {} km
            """.format(lat1/phys.degtorad,lon1/phys.degtorad,lat2/phys.degtorad,lon2/phys.degtorad,dlon/phys.degtorad,phys.rearth*distance/1000.)

      #Normalize the weights for this set of i/j/weight triads
      weightSum = sum( [wl[2] for wl in weightList])
      for wl in weightList:
        wl[2] /= weightSum

      #And add this list of triads to the weight mapping
      weightMap.append(weightList)
    
    return weightMap
                

  def generateLevMap(self,levCLM,levBins):
    """Sets the weights that map values on the CFSR vertical grid to values on 
       the CLM vertical grid.

       levCLM     :  The CLM depth values
       
       levBins    :  A geoBin instance where the edges of the bins correspond
                     to the CFSR level depths.  The leftmost edge should be -inf 
                     and the rightmost edge should be inf.

        ----------

        Returns:
          A list of coordinate mapping pairs.  Each list item contains one or
          more list of [k,weight] pairs, where k corresponds to a CFSR level
          index, and weight corresponds to the weight given to the CFSR value
          at that level.  For list items with more than one k/weight pair, the
          weights should sum to 1.0.

    """


    #Generate a list of the CFSR level bins into which the CLM levels fall
    kArrayCLM = [levBins.getIndex(val) for val in levCLM]

    #Loop through this bin list
    depthMap = [] #Initialize the depth mapping to a null set
    for n,k in zip(range(len(kArrayCLM)),kArrayCLM):
      #Set the endpoints of the linear interpolation
      k1 = k - 1
      k2 = k
      #Initialize a list of the depth coordinate mappings
      depthList = []
      #If we are above the highest CFSR depth, then set the corresponding CLM level value to
      #the value of the topmost CFSR level
      if(levBins.leftEdge[k] == -inf):
        depthList = [[k2,1.0]]
      #If we are below the lowest CFSR depth, then set the corresponding CLM level value to
      #the value of the lowestmost CFSR level
      elif(levBins.rightEdge[k] == inf):
        depthList = [[k1,1.0]]
      #Otherwise, linearly interpolate between the two
      else:
        weight1 = (levBins.rightEdge[k] - levCLM[n])/(levBins.rightEdge[k] - levBins.leftEdge[k])
        weight2 = 1.0 - weight1
        depthList = [[k1,weight1],[k2,weight2]]
      
      #Add this set of k/weight pairs to the depth mapping
      depthMap.append(depthList)
      

    #Return the depth mapping
    return depthMap
  
      

  def combineWeights3D(self,weightMap2D,depthMap1D):
    """Combines a 2D horizontal weight mapping (generated by generateWeightMap)
    with a 1D depth mapping (generated by generateLevMap) into a single set of
    3D weights that can be used to map from a 3D CFSR soil field onto a 3D CLM
    soil field."""

    weightMap3D = []

    for nc,wm in zip(range(len(weightMap2D)),weightMap2D):
      for kc,dm in zip(range(len(depthMap1D)),depthMap1D):
        weightList = []
        for item2D in wm:
          for item1D in dm:
            i = item2D[0]
            j = item2D[1]
            wij = item2D[2]
            k = item1D[0]
            wk = item1D[1]
            weightList.append([nc,kc,i,j,k,wij*wk])

        weightMap3D.append(weightList)


    return weightMap3D


  def weightMapToNumpyArray(self,weightMap):
    """Converts a weightMap file (a list of arrays) to a numpy array."""
    #First, find the maximum number of halo points
    maxHalo = amax(array([len(wl) for wl in weightMap]))

    #Next, find the number of elements in a weight mapping
    mapSize = len(weightMap[0][0])

    #Declare a numpy array with the correct size, filled with -1
    outArray = self.fillValue*ones([len(weightMap),maxHalo,mapSize])

    for i,wl in zip(range(len(weightMap)),weightMap):
      for j,item in zip(range(len(wl)),wl):
          outArray[i,j,:] = array(item)


    return outArray

  def writeWeightFile(self,weightFile):
    """Writes 2D and 3D mapping weight information to a netCDF file."""
    #Extract dimensionality information from the weight files
    wmshape = shape(self.weightMap3D)
    ncolumn3d = wmshape[0]
    nhalo3d = wmshape[1]
    nmap3d = wmshape[2]
    wmshape = shape(self.weightMap2D)
    ncolumn2d = wmshape[0]
    nhalo2d = wmshape[1]
    nmap2d = wmshape[2]
     
    #Open the weight file for writing
    try:
      fout = nc.Dataset(weightFile,"w")
    except BaseException as e:
      print "Error opening {} for writing".format(weightFile)
      raise e

    #**********************************************
    # Create Dimensions
    #**********************************************
    fout.createDimension('columnCLM',ncolumn2d)
    fout.createDimension('levCLM',len(self.levCLM))
    fout.createDimension('latCFSR',len(self.latCFSR))
    fout.createDimension('lonCFSR',len(self.lonCFSR))
    fout.createDimension('levCFSR',len(self.levCFSR))
    fout.createDimension('cell',ncolumn3d)
    fout.createDimension('halopoint2d',nhalo2d)
    fout.createDimension('halopoint3d',nhalo3d)
    fout.createDimension('mapitem2d',nmap2d)
    fout.createDimension('mapitem3d',nmap3d)

    #**********************************************
    # Create variables
    #**********************************************
    fout.createVariable('halopoint2d','f8',('halopoint2d',))
    fout.createVariable('halopoint3d','f8',('halopoint3d',))
    fout.createVariable('mapitem2d','f8',('mapitem2d',))
    fout.createVariable('mapitem3d','f8',('mapitem3d',))
    fout.createVariable('weightMap2D','f8',('columnCLM','halopoint2d','mapitem2d',),fill_value=self.fillValue)
    fout.createVariable('weightMap3D','f8',('cell','halopoint3d','mapitem3d',),fill_value = self.fillValue)
    fout.createVariable('latCLM','f8',('columnCLM',))
    fout.createVariable('lonCLM','f8',('columnCLM',))
    fout.createVariable('levCLM','f8',('levCLM',))
    fout.createVariable('thkCLM','f8',('levCLM',))
    fout.createVariable('latCFSR','f8',('latCFSR',))
    fout.createVariable('lonCFSR','f8',('lonCFSR',))
    fout.createVariable('levCFSR','f8',('levCFSR',))
    fout.createVariable('thkCFSR','f8',('levCFSR',))

    #**********************************************
    # Set variable metadata
    #**********************************************
    #Mapping metadata
    fout.variables['mapitem2d'].long_name = "Mapping info for 2D mapping"
    fout.variables['mapitem2d'].units = "0=i, 1=j, 2=Wij"
    fout.variables['mapitem3d'].long_name = "Mapping info for 3D mapping"
    fout.variables['mapitem3d'].units = "0=columnCLM,1=lev,2=i, 3=j, 4=k, 5=Wijk"

    #CLM variable metadata
    fout.variables['latCLM'].long_name = "Latitude of CLM column"
    fout.variables['latCLM'].units = "degrees_north"
    fout.variables['lonCLM'].long_name = "Longitude of CLM column"
    fout.variables['lonCLM'].units = "degrees_east"
    fout.variables['levCLM'].long_name = "Depth of CLM layer"
    fout.variables['levCLM'].units = "m"
    fout.variables['thkCLM'].long_name = "Thickness of CLM layer"
    fout.variables['thkCLM'].units = "m"

    #CFSR variable metadata
    fout.variables['latCFSR'].long_name = "Latitude of CFSR column"
    fout.variables['latCFSR'].units = "degrees_north"
    fout.variables['lonCFSR'].long_name = "Longitude of CFSR column"
    fout.variables['lonCFSR'].units = "degrees_east"
    fout.variables['levCFSR'].long_name = "Depth of CFSR layer"
    fout.variables['levCFSR'].units = "m"
    fout.variables['thkCFSR'].long_name = "Thickness of CFSR layer"
    fout.variables['thkCFSR'].units = "m"

    fout.variables['mapitem2d'][:] = range(nmap2d)
    fout.variables['mapitem3d'][:] = range(nmap3d)

    #**********************************************
    # Write out variables
    #**********************************************

    #Mapping weights
    fout.variables['weightMap2D'][:] = self.weightMap2D
    fout.variables['weightMap3D'][:] = self.weightMap3D

    #CLM coordinates
    fout.variables['latCLM'][:] = self.latCLM
    fout.variables['lonCLM'][:] = self.lonCLM
    fout.variables['levCLM'][:] = self.levCLM
    fout.variables['thkCLM'][:] = self.thkCLM

    #CFSR coordinates
    fout.variables['latCFSR'][:] = self.latCFSR
    fout.variables['lonCFSR'][:] = self.lonCFSR
    fout.variables['levCFSR'][:] = self.levCFSR
    fout.variables['thkCFSR'][:] = self.thkCFSR

  def loadWeightFile(self,weightFile):
    """Loads a weight file created by writeWeightFile() back into memory."""
    #Open the weight file for reading
    fin = nc.Dataset(weightFile,"r")

    #Read in weights
    self.weightMap2D = fin.variables['weightMap2D'][:]
    self.weightMap3D = fin.variables['weightMap3D'][:]

    #Read in CFSR coordinates
    self.latCFSR = fin.variables['latCFSR'][:]
    self.lonCFSR = fin.variables['lonCFSR'][:]
    self.levCFSR = fin.variables['levCFSR'][:]
    self.thkCFSR = fin.variables['thkCFSR'][:]

    #Read in CLM coordinates
    self.latCLM = fin.variables['latCLM'][:]
    self.lonCLM = fin.variables['lonCLM'][:]
    self.levCLM = fin.variables['levCLM'][:]
    self.thkCLM = fin.variables['thkCLM'][:]


  def doInterpolation(self,cfsrVariable,fillValue=None):
    """Applies the calculated weights to interpolate the cfsrVariable from the CFSR grid to the CLM grid."""

    #Do some basic checks (variable size/rank, etc.)
    varShape = shape(cfsrVariable)
    varRank = len(varShape)
    nlonCFSR = len(self.lonCFSR)
    nlatCFSR = len(self.latCFSR)
    nlevCFSR = len(self.levCFSR)
    ncolCLM = len(self.latCLM)
    nlevCLM = len(self.levCLM)

    ncell = shape(self.weightMap3D)[0]
    nhalo3d = shape(self.weightMap3D)[1]
    nhalo2d = shape(self.weightMap2D)[1]

    if(fillValue == None):
        fillValue = self.fillValue

    #Check that the variable has a proper rank
    if(varRank != 2 and varRank != 3):
      raise ValueError,"cfsrVariable should have rank 2 or 3, but it instead has rank {}.".format(varRank) 

    #Check that the input variable's shape conforms to the CFSR grid used to generate the mapping
    if(varShape[-1] != nlonCFSR):
      raise ValueError,"Size of longitude dimension of input CFSR variable (nlon = {}) does not match that of the CFSR grid used to create mapping weights (nlon = {}).".format(varShape[-1],nlonCFSR)
    if(varShape[-2] != nlatCFSR):
      raise ValueError,"Size of latitude dimension of input CFSR variable (nlat = {}) does not match that of the CFSR grid used to create mapping weights (nlat = {}).".format(varShape[-2],nlatCFSR)
    if(varRank == 3):
      if(varShape[0] != nlevCFSR):
        raise ValueError,"Size of level dimension of input CFSR variable (nlev = {}) does not match that of the CFSR grid used to create mapping weights (nlev = {}).".format(varShape[0],nlevCFSR)

    #If the variable is 3D, use the 3D weight routine
    if(varRank == 3):
      return ftnweights.applyweights3d(cfsrVariable[:],\
                                       self.weightMap3D,\
                                       fillValue,\
                                       nlat = nlatCFSR, \
                                       nlon = nlonCFSR, \
                                       nlev = nlevCFSR, \
                                       ncolumn = ncolCLM, \
                                       nlevclm = nlevCLM, \
                                       ncell = ncell, \
                                       nhalo = nhalo3d, \
                                       beverbose = self.beVerbose, \
                                       )

    #If the variable is 2D, use the 2D weight routine
    if(varRank == 2):
      return ftnweights.applyweights2d(cfsrVariable[:],\
                                       self.weightMap2D,\
                                       fillValue,\
                                       nlat = nlatCFSR, \
                                       nlon = nlonCFSR, \
                                       ncolumn = ncolCLM, \
                                       nhalo = nhalo2d, \
                                       )

if(__name__ == "__main__"):
  import os

  doRemakeFile = True
  weightFile = 'wgts_CFSR0.3deg-to-CLM-fv1.9x2.5-latxlon.nc'
  if((not os.path.exists(weightFile)) or doRemakeFile):
    myInterpolator = clmInterpolator("cfsr-0.3deg-latxlon.nc", \
                                   "/usrCS/local/cesm-input/lnd/clm2/initdata/clmi.BCN.2000-01-01_1.9x2.5_gx1v6_simyr2000_c100309.nc", \
                                   weightFile=weightFile)
  else:
    myInterpolator = clmInterpolator(weightFile=weightFile)




