from numpy import *
import Nio
import netCDF4 as nc
from iliad.physics import earthPhysics as phys
from iliad.physics import standardAtmosphere as stdatm
from iliad.physics.camClouds import parkCloudFraction
from poissonGridFill import poisson_grid_fill
from esmfRegrid import esmfRegrid
from iliad.utilities.conformDimensions import conformDimensions
from verticalInterpolation import interpolatePressureLevels
import os

def printMinMax(**kwargs):
  for key,value in kwargs.iteritems():
    print "\t{}\t--\tmin:\t{}, max:\t{}".format(key,amin(value),amax(value))

def doRegridCFStoCAM(   cfsPgbFile,     \
                        cfsFlxFile,     \
                        wgtFileDict,    \
                        camTopoFile,    \
                        camSSTFile,     \
                        outFileName,    \
                        beVerbose = True,   \
                        unStaggeredGrid = False, \
                        doGarbageCollection = False):
    """Takes data from NCEP CFS output and regrids it into a CAM initial condition file.

    """

    #Set a fill value that will be used internally here
    #TODO: double check that this value is only used internally (it should be)
    fillValue = -9999.

    #Import the garbage collection module if garbage collection is called for
    if(doGarbageCollection):
        import gc

    #The coefficient to determine the pressure of Hybrid level 1 variables from the
    #CFSv2 flx files ( = cfsHybSig1 * PS )
    cfsHybSig1 = 0.99467117



    #*********************
    # Read 3D CFSv2 data
    #*********************
    if(beVerbose):
        print("Reading CFSv2 data from PGB file")
    fpgbh = Nio.open_file(cfsPgbFile,"r")
    #Read in the CFSv2 dynamical variables from the PGB file
    Ucfs = fpgbh.variables["UGRD_P0_L100_GLL0"]
    Vcfs = fpgbh.variables["VGRD_P0_L100_GLL0"]
    Tcfs = fpgbh.variables["TMP_P0_L100_GLL0"]
    Qcfs = fpgbh.variables["SPFH_P0_L100_GLL0"][:]
    QCcfs = fpgbh.variables["CLWMR_P0_L100_GLL0"][:]
    SLPcfs = fpgbh.variables["PRMSL_P0_L101_GLL0"][:]

    #*********************
    # Read 2D CFSv2 data
    #*********************
    if(beVerbose):
        print("Reading CFSv2 data from FLX file")
    fflxf = Nio.open_file(cfsFlxFile,"r")
    #Read in the surface/near-surface CFSv2 dynamical variables from the FLX file
    PScfs_f_h1 = fflxf.variables["PRES_P0_L1_GGA0"]
    Tcfs_f_h1 = fflxf.variables["TMP_P0_L105_GGA0"]
    Qcfs_f_h1 = fflxf.variables["SPFH_P0_L105_GGA0"]
    Ucfs_f_h1 = fflxf.variables["UGRD_P0_L105_GGA0"]
    Vcfs_f_h1 = fflxf.variables["VGRD_P0_L105_GGA0"]
    #Read in some skin temperatures
    MASKcfs_f = fflxf.variables["LAND_P0_L1_GGA0"][:]
    TScfs_f = fflxf.variables["TMP_P0_L1_GGA0"][:]
    ICEcfs_f = fflxf.variables["ICEC_P0_L1_GGA0"]

    #*********************
    # Read CAM Topo data
    #*********************
    if(beVerbose):
        print("Reading CAM topo data")
    ftopo = Nio.open_file(camTopoFile,"r")
    #Read in the CAM topo
    ZShoriz_latxlon = ftopo.variables["PHIS"][:]
    ZShoriz_latxlon = ZShoriz_latxlon/phys.grav

    #***********************************
    #Calculate staggerd-grid topography
    #***********************************
    if(unStaggeredGrid):
      #Simply copy the unstaggered grid variables if the CAM grid is unstructured,
      #since the staggered and unstaggered grids are the same
      ZShoriz_slatxlon = ZShoriz_latxlon
      ZShoriz_latxslon = ZShoriz_latxlon
    else:
      #Otherwise calculate the topography values on the staggered grid
      #Calculate ZS on staggered latitudes
      ZShoriz_slatxlon = 0.5*(ZShoriz_latxlon[1:,:] +  ZShoriz_latxlon[:-1,:])

      #Calculate ZS on staggered longitudes
      ZShoriz_latxslon = zeros(shape(ZShoriz_latxlon))
      #Deal with the wraparound
      ZShoriz_latxslon[:,1:] = 0.5*(ZShoriz_latxlon[:,1:] +  ZShoriz_latxlon[:,:-1])
      ZShoriz_latxslon[:,0] = 0.5*(ZShoriz_latxlon[:,0] +  ZShoriz_latxlon[:,-1])

    #****************************************
    # Prepare variables for interpolation
    #****************************************
    if(beVerbose):
      print("Preparing variables for interpolation")

    #Add cloud water to the water vapor mixing ratio to get total water
    #the QC variable is missing the top 5 model levels, hence the 5: slicing
    # and QC is a mixing ratio# the q/(1+q) converts it to specific form
    Qcfs[5:,...] = Qcfs[5:,...] + QCcfs/(1. + QCcfs)

#    #Mask the land points in the SST variable and do a poisson fill
    if(beVerbose):
      print("Doing grid fill on SST")
    ifill = where(MASKcfs_f != 0.0)
    TScfs_f[ifill] = fillValue
    TScfs_f = poisson_grid_fill(TScfs_f,fillValue) - phys.t0


    #****************************************
    # Regrid horizontally to the CAM grid
    #****************************************
    if(beVerbose):
        print "Regridding to the horizontal CAM grid"
    UShoriz = esmfRegrid(wgtFileDict["CFSh_CAMSLat"]).doRegrid(Ucfs)
    VShoriz = esmfRegrid(wgtFileDict["CFSh_CAMSLon"]).doRegrid(Vcfs)
    Thoriz = esmfRegrid(wgtFileDict["CFSh_CAM"]).doRegrid(Tcfs)
    Qhoriz = esmfRegrid(wgtFileDict["CFSh_CAM"]).doRegrid(Qcfs)
    SLPhoriz_latxlon = esmfRegrid(wgtFileDict["CFSh_CAM"]).doRegrid(SLPcfs)
    if(unStaggeredGrid):
      #Simply copy the unstaggered grid variables if the CAM grid is unstructured,
      #since the staggered and unstaggered grids are the same
      SLPhoriz_slatxlon = SLPhoriz_latxlon
      SLPhoriz_latxslon = SLPhoriz_latxlon
    else:
      #Otherwise regrid SLP to the staggered grid
      SLPhoriz_slatxlon = esmfRegrid(wgtFileDict["CFSh_CAMSLat"]).doRegrid(SLPcfs)
      SLPhoriz_latxslon = esmfRegrid(wgtFileDict["CFSh_CAMSLon"]).doRegrid(SLPcfs)

    UShoriz_h1 = esmfRegrid(wgtFileDict["CFSf_CAMSLat"]).doRegrid(Ucfs_f_h1)
    VShoriz_h1 = esmfRegrid(wgtFileDict["CFSf_CAMSLon"]).doRegrid(Vcfs_f_h1)
    Thoriz_h1 = esmfRegrid(wgtFileDict["CFSf_CAM"]).doRegrid(Tcfs_f_h1)
    Qhoriz_h1 = esmfRegrid(wgtFileDict["CFSf_CAM"]).doRegrid(Qcfs_f_h1)
    TShoriz = esmfRegrid(wgtFileDict["CFSf_CAM"]).doRegrid(TScfs_f)
    ICEhoriz = esmfRegrid(wgtFileDict["CFSf_CAM"]).doRegrid(ICEcfs_f)

    #Regrid to the SST grid
    TShoriz_sstgrid = esmfRegrid(wgtFileDict["CFSf_CESMSST"]).doRegrid(TScfs_f)
    ICEhoriz_sstgrid = esmfRegrid(wgtFileDict["CFSf_CESMSST"]).doRegrid(ICEcfs_f)

    #******************************************
    # Undo horizontal interpolation prep work 
    #******************************************
    if(beVerbose):
        print("Undoing interpolation prep")
    #Calculate PS from SLP
    PShoriz_latxlon = stdatm.calculatePS(SLPhoriz_latxlon,ZShoriz_latxlon)
    if(unStaggeredGrid):
      #Simply copy the unstaggered grid variables if the CAM grid is unstructured,
      #since the staggered and unstaggered grids are the same
      PShoriz_slatxlon = PShoriz_latxlon
      PShoriz_latxslon = PShoriz_latxlon
    else:
      #Otherwise calculate pressure on the staggered grid
      PShoriz_slatxlon = stdatm.calculatePS(SLPhoriz_slatxlon,ZShoriz_slatxlon)
      PShoriz_latxslon = stdatm.calculatePS(SLPhoriz_latxslon,ZShoriz_latxslon)

    #Place a bottom limit of -1.8 C on the SST
    itoolow = where(TShoriz < -1.8)
    TShoriz[itoolow] = -1.8

    #******************************************
    # Clean memory of unneeded vars
    #******************************************
    del(Ucfs)
    del(Vcfs)
    del(Tcfs)
    del(Qcfs)
    del(PScfs_f_h1)
    del(fpgbh)
    del(fflxf)
    del(ftopo)
    del(ZShoriz_latxlon)
    del(ZShoriz_slatxlon)
    del(ZShoriz_latxslon)
    del(Tcfs_f_h1)

    #Do garbage collection to free memory if necessary
    if(doGarbageCollection):
        gc.collect()


    #******************************************
    # Prepare for vertical regridding
    #******************************************
    if(beVerbose):
        print "Preparing variables for vertical regridding"

    #Load fields for calculation of the 3D CAM pressure field
    fwgt = Nio.open_file(wgtFileDict["CFSh_CAM"],"r")
    hyam = fwgt.variables["hyam"][:]
    hybm = fwgt.variables["hybm"][:]
    p0 = fwgt.variables["P0"].get_value()

    #Calculate some conforming variable shapes
    dsizeT = shape(Thoriz)
    dsizeUS = shape(UShoriz)
    dsizeVS = shape(VShoriz)
    try:
      dsize_latxlon = (len(hyam),dsizeT[1],dsizeT[2])
      dsize_slatxlon = (len(hyam),dsizeUS[1],dsizeUS[2])
      dsize_latxslon = (len(hyam),dsizeVS[1],dsizeVS[2])
    except:
      # if the above didn't work, assume the grid is unstructured
      dsize_latxlon = (len(hyam),dsizeT[1])
      dsize_slatxlon = (len(hyam),dsizeUS[1])
      dsize_latxslon = (len(hyam),dsizeVS[1])


    #Load the CFSv2 level variable
    Pcfs = fwgt.variables["lv_ISBL0"][:]


    #Create a couple of 3D versions of this variable
    Pcfs_latxlon = conformDimensions(Pcfs,dsizeT)
    Pcfs_slatxlon = conformDimensions(Pcfs,dsizeUS)
    Pcfs_latxslon = conformDimensions(Pcfs,dsizeVS)

    #Filter out levels below the hybrid 1 level, since these had to have been
    #extrapolated
    H13D_latxlon = conformDimensions(cfsHybSig1*PShoriz_latxlon,shape(Pcfs_latxlon))
    H13D_slatxlon = conformDimensions(cfsHybSig1*PShoriz_slatxlon,shape(Pcfs_slatxlon))
    H13D_latxslon = conformDimensions(cfsHybSig1*PShoriz_latxslon,shape(Pcfs_latxslon))
    #Find pressure values that are lower than the extrapolation level
    ibelow_latxlon = where(Pcfs_latxlon >= H13D_latxlon)
    ibelow_slatxlon = where(Pcfs_slatxlon >= H13D_slatxlon)
    ibelow_latxslon = where(Pcfs_latxslon >= H13D_latxslon)
    #Set the interpolants as missing for these indices
    Thoriz[ibelow_latxlon] = fillValue
    Qhoriz[ibelow_latxlon] = fillValue
    UShoriz[ibelow_slatxlon] = fillValue
    VShoriz[ibelow_latxslon] = fillValue
    Pcfs_latxlon[ibelow_latxlon] = fillValue
    Pcfs_slatxlon[ibelow_slatxlon] = fillValue
    Pcfs_latxslon[ibelow_latxslon] = fillValue

    #Delete the hybrid surface pressure variables
    del(H13D_latxlon)
    del(H13D_slatxlon)
    del(H13D_latxslon)

    #Concatenate hybrid level 1 variables onto the regridded dynamic variables so that we have data down
    #near to the surface
    Thoriz = concatenate((Thoriz,Thoriz_h1[newaxis,...],))
    Qhoriz = concatenate((Qhoriz,Qhoriz_h1[newaxis,...],))
    UShoriz = concatenate((UShoriz,UShoriz_h1[newaxis,...],))
    VShoriz = concatenate((VShoriz,VShoriz_h1[newaxis,...],))
    Pcfs_latxlon = concatenate((Pcfs_latxlon,cfsHybSig1*PShoriz_latxlon[newaxis,...],))
    Pcfs_slatxlon = concatenate((Pcfs_slatxlon,cfsHybSig1*PShoriz_slatxlon[newaxis,...],))
    Pcfs_latxslon = concatenate((Pcfs_latxslon,cfsHybSig1*PShoriz_latxslon[newaxis,...],))

    #Calculate the 3D CAM pressure fields
    P3D_latxlon = conformDimensions(hyam*p0,dsize_latxlon) + conformDimensions(hybm,dsize_latxlon)\
                            *conformDimensions(PShoriz_latxlon,dsize_latxlon)
    P3D_slatxlon = conformDimensions(hyam*p0,dsize_slatxlon) + conformDimensions(hybm,dsize_slatxlon)\
                            *conformDimensions(PShoriz_slatxlon,dsize_slatxlon)
    P3D_latxslon = conformDimensions(hyam*p0,dsize_latxslon) + conformDimensions(hybm,dsize_latxslon)\
                            *conformDimensions(PShoriz_latxslon,dsize_latxslon)


    #******************************************
    # Do vertical interpolation
    #******************************************
    if(beVerbose):
        print "Doing vertical interpolation"
    UScam = interpolatePressureLevels(UShoriz,Pcfs_slatxlon,P3D_slatxlon,fillValue=fillValue,interpolationDimension=0)
    VScam = interpolatePressureLevels(VShoriz,Pcfs_latxslon,P3D_latxslon,fillValue=fillValue,interpolationDimension=0)
    Qcam = interpolatePressureLevels(Qhoriz,Pcfs_latxlon,P3D_latxlon,fillValue=fillValue,interpolationDimension=0)
    Tcam = interpolatePressureLevels(Thoriz,Pcfs_latxlon,P3D_latxlon,fillValue=fillValue,useLogPressure=False,interpolationDimension=0)


    #******************************************
    # Do some post-interpolation processing
    #******************************************
    if(beVerbose):
        print "Inferring cloud water and amount"

    #Calculate relative humidity
    RHcam = phys.rhfromstate(P3D_latxlon,Tcam,Qcam)
    #Calculate supersaturation
    SScam = zeros(shape(Qcam))
    isupersat = where(logical_and(RHcam > 1, RHcam < 2))
    SScam[isupersat] = RHcam[isupersat] - 1.0

    #Calculate cloud water content
    QCcam = SScam*Qcam

    #Subtract cloud water content from total to get vapor water content
    Qcam -= QCcam

    #Calculate cloud fraction
    CLDcam = parkCloudFraction(RHcam,P3D_latxlon,Qcam)

    if(beVerbose):
        print "Writing to {}".format(outFileName)

    fout = nc.Dataset(outFileName,"r+")

    #Set the dimensionality of 3D variables
    dimensionTuple = fout.variables["T"].dimensions

    #Create a cloud variable if necessary
    if( not "CLOUD" in fout.variables ):
        fout.createVariable("CLOUD",'f8',dimensionTuple)
        fout.variables["CLOUD"].units = "fraction"
        fout.variables["CLOUD"].long_name = "Cloud Fraction"

    #Create a cloud water variable if necessary
    if( not "QCWAT" in fout.variables ):
        fout.createVariable("QCWAT",'f8',dimensionTuple)
        fout.variables["QCWAT"].units = "kg/kg"
        fout.variables["QCWAT"].long_name = "q associated with cloud water"

    # define a slice tuple that will help give the output variables
    # the proper rank
    outSlice3D = len(dimensionTuple)*[slice(None,None,None)]
    outSlice2D = (len(dimensionTuple)-1)*[slice(None,None,None)]
    # deal with the condition that the IC file has a time dimension for
    # the variables
    if len(dimensionTuple) > len(UScam.shape):
        outSlice3D[0] = newaxis
        outSlice2D[0] = newaxis
    outSlice3D = tuple(outSlice3D)
    outSlice2D = tuple(outSlice2D)

    #Write the interpolated variables to the cam initial condition file
    if(unStaggeredGrid):
      #The variable names are different in unstructured grid files
      fout.variables["U"][:] = UScam[outSlice3D]
      fout.variables["V"][:]= VScam[outSlice3D]
    else:
      fout.variables["US"][:] = UScam[outSlice3D]
      fout.variables["VS"][:] = VScam[outSlice3D]
    fout.variables["T"][:] = Tcam[outSlice3D]
    fout.variables["Q"][:] = Qcam[outSlice3D]
    fout.variables["QCWAT"][:] = QCcam[outSlice3D]
    fout.variables["CLDLIQ"][:] = QCcam[outSlice3D]
    fout.variables["CLDICE"][:] = 0.0
    fout.variables["CLOUD"][:] = CLDcam[outSlice3D]
    fout.variables["PS"][:] = PShoriz_latxlon[outSlice2D]
    # if the IC file has PSL, put it in; otherwise just go on
    try:
        fout.variables["PSL"][:] = SLPhoriz_latxlon[outSlice2D]
    except:
        pass

    fout.close()

    if(beVerbose):
        print "Writing to {}".format(camSSTFile)

    fsst = nc.Dataset(camSSTFile,"r+")

    #Write the SST values.
    for i in range(12):
        fsst.variables["SST_cpl"][i,...] = TShoriz_sstgrid
        fsst.variables["ice_cov"][i,...] = ICEhoriz_sstgrid

    fsst.close()

    if(beVerbose):
        print "Done."

#*******************************************************************************
#*******************************************************************************
#***************** Run unit tests **********************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":
    mappingDirectory = "regrid-preproc-fv1.9x2.5"

    wgtFile = {}
    wgtFile["CFSh_CAM"] = "{}/wgts_CFSR0.5deg-to-CAM-fv1.9x2.5-latxlon.nc".format(mappingDirectory)
    wgtFile["CFSh_CAMSLat"] = "{}/wgts_CFSR0.5deg-to-CAM-fv1.9x2.5-slatxlon.nc".format(mappingDirectory)
    wgtFile["CFSh_CAMSLon"] = "{}/wgts_CFSR0.5deg-to-CAM-fv1.9x2.5-latxslon.nc".format(mappingDirectory)
    wgtFile["CFSf_CAM"] = "{}/wgts_CFSR0.3deg-to-CAM-fv1.9x2.5-latxlon.nc".format(mappingDirectory)
    wgtFile["CFSf_CAMSLat"] = "{}/wgts_CFSR0.3deg-to-CAM-fv1.9x2.5-slatxlon.nc".format(mappingDirectory)
    wgtFile["CFSf_CAMSLon"] = "{}/wgts_CFSR0.3deg-to-CAM-fv1.9x2.5-latxslon.nc".format(mappingDirectory)

    if not all([ os.path.exists(wgtFile[key]) for key in wgtFile ]):
        raise ValueError,"not all weight files exist."

    doRegridCFStoCAM( \
        cfsPgbFile = "/projects/data/CFSv2/cdas.20140116/cdas1.t00z.pgrbh00.grib2", \
        cfsFlxFile = "/projects/data/CFSv2/cdas.20140116/cdas1.t00z.sfluxgrbf00.grib2", \
        wgtFileDict = wgtFile, \
        camTopoFile = "USGS-gtopo30_1.9x2.5_remap_c050602.nc",  \
        outFileName = "iniTestFile.nc", \
        camSSTFile = "sst-0000-01-01_fv1.9x2.5.nc", \
        beVerbose = True,   \
        doGarbageCollection = True, \
        )

