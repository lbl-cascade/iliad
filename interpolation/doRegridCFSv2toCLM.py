#!/usr/bin/env python
import os
from fortranModuleSource.mod_ftnweights import mod_ftnweights as ftnweights
import netCDF4 as nc
import Nio
from numpy import *
import clmInterpolator as clmInterpolator
from iliad.physics import earthPhysics as phys
import argparse

def printMinMax(**kwargs):
  for key,value in kwargs.iteritems():
    print "\t{}\t--\tmin:\t{}, max:\t{}".format(key,amin(value),amax(value))

class regrid:

  def __init__(self,weightFile, cfsrFile, clmFile):
    """Regrids data from a CFSR flxf file to a CLM grid """

    fillValueCLM = 1.e36


    #*******************************************************************************
    #*******************************************************************************
    #****************** Read in the weight file ************************************
    #*******************************************************************************
    #*******************************************************************************

    myInterpolator = clmInterpolator.clmInterpolator(weightFile = weightFile,beVerbose=False)

    #*******************************************************************************
    #*******************************************************************************
    #****************** Read and prepare CFSR data *********************************
    #*******************************************************************************
    #*******************************************************************************

    fcfsr_in = Nio.open_file(cfsrFile,"r")

    #Read in the soil temperature so we can find points with valid temps
    soilTempCFSR = fcfsr_in.variables["TMP_P0_2L106_GGA0"]
    fillValueCFSR = fcfsr_in.variables["TMP_P0_2L106_GGA0"]._FillValue

    #Read in the skin temperature
    skinTempCFSR = fcfsr_in.variables["TMP_P0_L1_GGA0"]

    #Read in the land mask
    landMaskCFSR = fcfsr_in.variables["LAND_P0_L1_GGA0"]

    doMaskOcean = True
    if(doMaskOcean):
      #Find land points in the skin temperature and set the ground temperature (missing elsewhere)
      iLand = where(landMaskCFSR[:] > 0)
      groundTempCFSR = fillValueCFSR*ones(shape(skinTempCFSR))
      groundTempCFSR[iLand] = skinTempCFSR[:][iLand]
    else:
      groundTempCFSR = skinTempCFSR[:]

    volSoilMoistureLiquidCFSR = fcfsr_in.variables["SOILL_P0_2L106_GGA0"][:]
    volSoilMoistureIceCFSR = fcfsr_in.variables["SOILW_P0_2L106_GGA0"][:]

    #Convert from total soil moisture to ice soil moisture
    #(Note: we want to calculate this here instead of after interpolation because
    #it is a positive-definite quantity if calculated now; that isn't guaranteed if
    #calculated after interpolation.)
    volSoilMoistureIceCFSR -= volSoilMoistureLiquidCFSR

    snowDepthCFSR = fcfsr_in.variables["SNOD_P0_L1_GGA0"]
    snowFractionCFSR = fcfsr_in.variables["SNOWC_P8_L1_GGA0_avg"]

    #*******************************************************************************
    #*******************************************************************************
    #*************** Interpolate to the CLM Grid ***********************************
    #*******************************************************************************
    #*******************************************************************************

    soilTempCLM = myInterpolator.doInterpolation(soilTempCFSR,fillValueCFSR)
    soilMoistureLiquidCLM = myInterpolator.doInterpolation(volSoilMoistureLiquidCFSR,fillValueCFSR)
    soilMoistureIceCLM = myInterpolator.doInterpolation(volSoilMoistureIceCFSR,fillValueCFSR)
    snowDepthCLM = myInterpolator.doInterpolation(snowDepthCFSR,fillValueCFSR)
    snowFractionCLM = myInterpolator.doInterpolation(snowFractionCFSR,fillValueCFSR)
    groundTempCLM = myInterpolator.doInterpolation(groundTempCFSR,fillValueCFSR)

    #*******************************************************************************
    #*******************************************************************************
    #*************** Prepare output variables for CLM ini file *********************
    #*******************************************************************************
    #*******************************************************************************

    #Convert volumetric soil moisture to mass burden
    for k in range(len(myInterpolator.levCLM)):
        #Find non-missing soil moisture points; only scale these (otherwise the fill
        # value gets screwed up
        igoodliq = where(soilMoistureLiquidCLM[:,k] != fillValueCFSR)
        igoodice = where(soilMoistureIceCLM[:,k] != fillValueCFSR)
        #Do the volumetric->massburden conversion
        soilMoistureLiquidCLM[igoodliq,k] *= phys.rhow*myInterpolator.thkCLM[k]
        soilMoistureIceCLM[igoodice,k] *= phys.rhoi*myInterpolator.thkCLM[k]

    #Split the snow into CLM levels
    #TODO: do this


    #*******************************************************************************
    #*******************************************************************************
    #*********** Write variables to CLM test file **********************************
    #*******************************************************************************
    #*******************************************************************************

    #Open the clm file

    fclm_out = nc.Dataset(clmFile,"r+")

    #Find points with valid temperatures in both the CLM ini file and in the interpolated dataset;
    #only overwrite points where there are valid data in both.
    tsoiTmp = fclm_out.variables["T_SOISNO"][:,5:]
    ivalid = where(logical_and(tsoiTmp != fillValueCLM,soilTempCLM != fillValueCFSR))
    tsoiTmp[ivalid] = soilTempCLM[ivalid]
    fclm_out.variables["T_SOISNO"][:,5:] = tsoiTmp

    #Find points with valid liquid soil moistures in both the CLM ini file and in the interpolated dataset;
    #only overwrite points where there are valid data in both.
    soilwTmp = fclm_out.variables["H2OSOI_LIQ"][:,5:]
    ivalid = where(logical_and(soilwTmp != fillValueCLM,soilMoistureLiquidCLM != fillValueCFSR))
    soilwTmp[ivalid] = soilMoistureLiquidCLM[ivalid]
    fclm_out.variables["H2OSOI_LIQ"][:,5:] = soilwTmp

    #Find points with valid ice soil moistures in both the CLM ini file and in the interpolated dataset;
    #only overwrite points where there are valid data in both.
    soiliTmp = fclm_out.variables["H2OSOI_ICE"][:,5:]
    ivalid = where(logical_and(soiliTmp != fillValueCLM,soilMoistureIceCLM != fillValueCFSR))
    soiliTmp[ivalid] = soilMoistureIceCLM[ivalid]
    fclm_out.variables["H2OSOI_ICE"][:,5:] = soiliTmp

    #Find points with valid ground temperatures in both the CLM ini file and in the interpolated dataset;
    #only overwrite points where there are valid data in both.
    tgrndTmp = fclm_out.variables["T_GRND"][:]
    ivalid = where(logical_and(tgrndTmp != fillValueCLM,groundTempCLM != fillValueCFSR))
    tgrndTmp[ivalid] = groundTempCLM[ivalid]
    fclm_out.variables["T_GRND"][:] = tgrndTmp

    #Write snow ouput
    #TODO: do this

    #Close this explicitly to deal with a corruption issue when metadata are written
    fclm_out.close()


if(__name__ == "__main__"):
  parser = argparse.ArgumentParser()

  parser.add_argument('--weightfile','-w',help="Weight file name (generated by clmInterpolator.py)",default=None)
  parser.add_argument('--cfsrfile','-r',help="CFSR flxf file name",default=None)
  parser.add_argument('--clmfile','-c',help="A sample CLM file into which to write interpolated data",default=None)

  parsedArgs = vars(parser.parse_args())
  weightFile = parsedArgs['weightfile']
  cfsrFile = parsedArgs['cfsrfile']
  clmFile = parsedArgs['clmfile']

  if(weightFile == None or cfsrFile == None or clmFile == None):
      print "Error: not enough arguments were provided"
      parser.print_help()
      quit()


  regrid(weightFile, cfsrFile, clmFile)
