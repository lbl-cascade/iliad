from numpy import *
from iliad.physics import earthPhysics as phys
from poissonGridFill import poisson_grid_fill
from esmfRegrid import esmfRegrid
import netCDF4 as nc


def doRegridOISSTtoCAM( oisst_sstFile,  \
                        oisst_iceFile,  \
                        weightFile,     \
                        outFile,        \
                        beVerbose,      \
                        forecastTimeUnits):
    """Regrids from the OISST weekly dataset to a CAM SST grid"""


    #Get a datetime object for the requested time
    forecastDateTime = nc.num2date(0.0,forecastTimeUnits)

    #Open the SST and ice files
    sstfile = nc.Dataset(oisst_sstFile,"r")
    icefile = nc.Dataset(oisst_iceFile,"r")

    #Get the time indices for the the requested date
    timesst = nc.date2index(forecastDateTime,sstfile.variables["time"],select='before')
    timeice = nc.date2index(forecastDateTime,icefile.variables["time"],select='before')

    sst_oisst = sstfile.variables["sst"][timesst,:,:]
    ice_oisst = icefile.variables["icec"][timeice,:,:]

    #Fill missing values to avoid missing values leaking in to the interpolation
    icevar = icefile.variables["icec"]
    fillValue = icevar.missing_value*icevar.scale_factor + icevar.add_offset
    ice_oisst = poisson_grid_fill(ice_oisst,fillValue)
    #Convert to fractional ice coverage
    ice_oisst /= 100.

    if(beVerbose):
        print "Regridding to the horizontal CAM grid"
    #Do the regridding
    TShoriz = esmfRegrid(weightFile).doRegrid(sst_oisst)
    ICEhoriz = esmfRegrid(weightFile).doRegrid(ice_oisst)

    if(beVerbose):
        print "Writing to {}".format(outFile)

    fout = nc.Dataset(outFile,"r+")

    for i in range(12):
        fout.variables["SST_cpl"][i,:,:] = TShoriz
        fout.variables["ice_cov"][i,:,:] = ICEhoriz

if __name__ == "__main__":
  oisst_sstFile = "/projects/data/OISST/sst.wkmean.1990-present.nc"
  oisst_iceFile = "/projects/data/OISST/icec.wkmean.1990-present.nc"
  wgtFile_latxlon = "regrid-preproc-fv1.9x2.5/wgts_OISST1deg-to-CAM-fv1.9x2.5-latxlon.nc"
  outFile = "sstIceTestFile.nc"
  beVerbose = True
  forecastTimeUnits = "days since 2014-01-04 00:00:00"

  doRegridOISSTtoCAM(   oisst_sstFile = oisst_sstFile, \
                        oisst_iceFile = oisst_iceFile, \
                        weightFile = wgtFile_latxlon, \
                        outFile = outFile, \
                        beVerbose = beVerbose, \
                        forecastTimeUnits = forecastTimeUnits)
                        
