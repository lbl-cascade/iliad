cimport numpy as np
import numpy as np
cimport cython
import cython.parallel as cpar
cimport openmp

@cython.boundscheck(False)
cpdef np.float_t [:] applyESMFWeights( \
                                    np.float_t [:] inputField, \
                                    int [:] inputIndices, \
                                    int [:] outputIndices, \
                                    np.float_t [:] weightValues, \
                                    np.float_t [:] fractionalOverlap, \
                                    int numOutput, \
                                    missingValue = None):
    """Does a sparse matrix multiplication to apply the ESMF weights.

    Performs the sparse matrix multiplation OF = W * IF

    :param inputField: a flattened version of the input variables

    :param inputIndices : a vector of indicies in the input array for each weight element

    :param outputIndices : a vector of indicies in the output array for each weight element 

    :param weightValues: a vector of weight values

    :param fractionalOverlap: a fractional overlap parameter used for normalization

    :param numOutput: the number of output elements

    :param missingValue: a missing value (if no argument is provided, missing values are ignored)

    :return: A flattened output array of shape [numOutput]

    """

    cdef int nval = weightValues.shape[0]
    cdef int i,j,n
    cdef np.float_t floatNaN = float(np.nan)

    cdef np.float_t fillValue = -1.0

    #Deal with missing values
    if missingValue is None:
        handleMissing = False
    else:
        handleMissing = True
        fillValue = float(missingValue)

    #Initialize the output field
    cdef np.float_t [:] outputField
    outputField = np.zeros([numOutput],dtype=np.float)

    #Turn on dynamic use of threads
    openmp.omp_set_dynamic(1)

    if not handleMissing:
    #Write a loop for simply performing the matrix multiplication
    #This can simply use OpenMP
        for n in cpar.prange(nval,nogil=True):

            #Get the current output array index
            j = outputIndices[n]-1
            #Get the current input array index
            i = inputIndices[n]-1

            #Update the output variable
            outputField[j] += weightValues[n] * inputField[i]
    else:
    #Write a loop for performing the matrix multiplication in the case of
    #having to deal with missing values
            for n in cpar.prange(nval,nogil=True):
                #Get the current output array index
                j = outputIndices[n]-1
                #Get the current input array index
                i = inputIndices[n]-1

                if inputField[i] == fillValue or outputField[j] == floatNaN : 
                    #If either the input field or the output field are missing,
                    #simply set the output field to missing and do the next iteration
                    outputField[j] = floatNaN
                else:
                    #Update the output variable
                    outputField[j] += weightValues[n] * inputField[i]


    return outputField
