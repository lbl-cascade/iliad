import numpy as np
from numpy import *
import netCDF4 as nc

class esmfRegrid:
    """
    Uses regridding weights created by the ESMF regridding program
    ESMFRegridWeightGen to interpolate from an input grid to an output
    grid.
    """

    def __init__(   self,  \
                    weightFile, \
                    useCython = False, \
                    ):
        """
        Uses regridding weights created by the ESMF regridding program
        ESMFRegridWeightGen to interpolate from an input grid to an output
        grid.

        Input Arguments:
        ----------------

            weightFile  :   A NetCDF weight file created by ESMFRegridWeightGen

            useCython   :   Flag whether to use the Cython implementation of matrix multiplication

        The routine doRegrid() applys a sparse matrix multiplcation (coded in
        Fortran for speed) of weights in weightFile to an input field.

        Example:
        --------

            regriddedPressure = esmfRegrid("CFS0.5degree-to-fv1.9x2.5-weghts.nc").doRegrid(cfsSeaLevelPressure)


        """

        #Flag that we do not have weights currently
        self.haveWeights = False

        #Set input arguments
        self.weightFile = weightFile

        self.useCython = useCython

        try:
            #Read the weights
            self.__readWeightFile()
        except BaseException as e:
            print "Reading the weight file '{}' failed with the following exception:\n{}".format(weightFile,e)
            raise e

    def __readWeightFile(self):
        #******************************
        # Open the weight file
        #******************************
        #Open the file
        fin = nc.Dataset(self.weightFile,"r")

        #******************************
        # Read the sparse weight matrix
        #******************************
        self.destinationIndices = array(fin.variables["row"][:])
        self.sourceIndices = array(fin.variables["col"][:])
        self.weightValues = array(fin.variables["S"][:])
        self.fractionalOverlap = array(fin.variables["frac_b"][:])
        self.haveWeights = True

        #******************************
        # Get grid characteristics
        #******************************
        self.sourceGridShape = tuple(fin.variables["src_grid_dims"][:])
        self.destinationGridShape = tuple(fin.variables["dst_grid_dims"][:])

        #If there is only 1 dimension in the source or  dest grid,  simply use n_a or n_b as its length
        #to guarantee the proper shape
        if len(self.sourceGridShape) == 1:
          self.sourceGridShape = (len(fin.dimensions['n_a']),)
        if len(self.destinationGridShape) == 1:
          self.destinationGridShape = (len(fin.dimensions['n_b']),)

        #Reverse the shape of the output dimensions for the final output (they put lon first)
        self.finalGridShape = self.destinationGridShape[::-1]

        self.sourceGridRank = len(self.sourceGridShape)
        self.destinationGridRank = len(self.destinationGridShape)
    
        self.sourceGridIsUnstructured = False

        if(self.sourceGridRank < self.destinationGridRank):
          self.sourceGridIsUnstructured = True

        #******************************
        # Read metadata
        #******************************
        #TODO

    def doRegrid(self,inputField,fillValue = None):
        """ Perform a regridding operation.

        The inputField must be 'weakly congruent,' meaning that its rightmost dimension(s) must match
        the the dimension(s) of the input grid (stored in self.sourceGridShape).

        Input Arguments:
        ----------------

            inputField      :   an arraylike field whose rightmost dimension(s)
                                must match the dimension(s) of the source grid stored in
                                self.weightFile

            fillValue       :   if provided indicates the fillValue of the incoming data

        Returns:
        --------

            A numpy array whose rightmost dimensions match the dimensionality
            of the destination grid stored in self.weightFile.  Other dimensions will match those of inputField.

            If fillValue is provided, the return array is a masked numpy array.

        Example:
        --------

            TODO            

        """
        #******************************
        # Check whether we have weights
        #******************************
        if(not self.haveWeights):
            raise ValueError,"No weights have been read yet"

        #******************************
        # Check for field conformance
        #******************************
        #Get input field dimensionality
        inputFieldShape = shape(inputField)
        inputFieldRank = len(inputFieldShape)

        #Check that the rank is appropriate
        if(inputFieldRank < self.sourceGridRank):
            raise ValueError,"inputField does not conform to the dimensionality of the source grid: it has rank {}, but the source grid has rank {}.".format(inputFieldRank,self.sourceGridRank)

        #Check that dimensions conform
        self.doFlipSourceDimensions = False #Flag that we don't need to flip the source dimensions
        #Check if the rightmost dimensions match
        if(any(inputFieldShape[-self.sourceGridRank:] != self.sourceGridShape)):
            #If they don't, try flipping the source dimenions and check again
            if(any(inputFieldShape[-self.sourceGridRank:] != self.sourceGridShape[::-1])):
                #If still not, then the grid isn't conformant, so raise an error
                raise ValueError,"inputField does not conform to the dimensionality of the source grid: its rightmost dimensions are {}, but the source grid has dimensionality {}.".format(inputFieldShape[-self.sourceGridRank:],self.sourceGridShape[::-1])
            else:
                #If so, flag that the the source dimensions need to be flipped
                self.doFlipSourceDimensions = True
                self.destinationGridShape = self.destinationGridShape[::-1]

        #Check whether a missing value is provided
        if(fillValue is not None):
            handleMissing = True
        else:
            handleMissing = False
            fillValue = -9999.0

        
        #******************************
        # Apply the weights
        #******************************

        #If the input and source grids are the same, there are no leftmost dimensions to deal with
        #and so the output shape will be the destination grid shape
        if(inputFieldRank == self.sourceGridRank):
            #Flag that there are no leftmost dimensions
            leftmostDimensionShape = None
            leftmostDimensionRank = None
            leftmostIndicies = [None]

            #Set the output shape
            outputFieldShape = self.finalGridShape

        #Otherwise, we need to create a list of leftmost dimension indicies over which to loop
        else:

            #Determine output field dimensions
            leftmostDimensionShape = inputFieldShape[:-self.sourceGridRank]
            leftmostDimensionRank = len(leftmostDimensionShape)
            #The output field shape is a combination of the leftmost dimensions of the input variable and the
            #shape of the destination grid
            outputFieldShape = leftmostDimensionShape + self.finalGridShape

            #Create a list of all possible indicies of the leftmost dimensions
            rangeList = [ range(s) for s in leftmostDimensionShape] 
            if(len(rangeList) == 1):
                #If there is only one leftmost dimension, just return the indices of this dimension
                flattenedIndArrays = rangeList
            else:
                #Use meshgrid to create a set of grid indices, for each of the leftmost dimensions, that spans
                #all the possible combinations of leftmost dimension indicies
                leftmostIndArrays = meshgrid(*rangeList)
                #Meshgrid returns these indices as a list of arrays; flatten each list of arrays using ravel()
                flattenedIndArrays = [ grid.ravel() for grid in leftmostIndArrays ] 
                
            #Get the length of the flattened index lists (it is the same for each of the lists, so the length of the
            # first one suffices)
            flattendIndLength = len(flattenedIndArrays[0])

            #Use a nested list comprehension to combine the grid lists into a list of tuples; the result is a list
            # of tuples of all possible index combinations for the leftmost dimensions
            leftmostIndicies = [ tuple([ gridR[i] for gridR in flattenedIndArrays ]) \
                                            for i in range(flattendIndLength) ]

        #Pre-define the output field; initialization with zeros is necessary for interpolation to work
        if handleMissing:
            outputField = ma.zeros(outputFieldShape,dtype=np.float)
        else:
            outputField = zeros(outputFieldShape,dtype=np.float)

        #Loop over the leftmost dimension indicies (or only do one loop if there are none)
        for indTuple in leftmostIndicies:
            #Create a list of index slices in order to access the rightmost array for each of the leftmost index
            #combinations
            if(indTuple is not None):
                #First generate the slices for the leftmost dimensions
                leftSliceTuple = tuple([ slice(ii,ii+1) for ii in indTuple ])
            else:
                #If indTuple is None, then there are no leftmost dimensions, so we only need to generate an
                #empty tuple to represent the lack of leftmost dimensions
                leftSliceTuple = ()

            #Create a tuple of slices that access the entirety of the rightmost dimensions of the input field
            inputSliceTuple = ()
            for r in range(self.sourceGridRank):
                inputSliceTuple += (slice(0,None),)

            #Create a tuple of slices that access the entirety of the rightmost dimensions of the output field
            outputSliceTuple = ()
            for r in range(self.destinationGridRank):
                outputSliceTuple += (slice(0,None),)

            #Get the current slice from the input variable
            inputFieldSlice = inputField[leftSliceTuple + inputSliceTuple]

            #Get the current slice from the output variable
            outputFieldSlice = outputField[leftSliceTuple + outputSliceTuple]

            #Flatten the slices
            if(not self.doFlipSourceDimensions):
                flatInputSlice = transpose(inputFieldSlice).ravel()
                flatOutputSlice = transpose(outputFieldSlice).ravel()
            else:
                flatInputSlice = inputFieldSlice.ravel()
                flatOutputSlice = outputFieldSlice.ravel()


            #Check if weight indicies are conformant
            if(any(self.destinationIndices > len(flatOutputSlice))):
              raise ValueError,"Error in weight file. Some indices in the `row' variable are greater than the length of the flattened output variable: (> {})".format(len(flatOutputSlice))

            if(any(self.sourceIndices > len(flatInputSlice))):
              raise ValueError,"Error in weight file. Some indices in the `col' variable are greater than the length of the flattened input variable: (> {})".format(len(flatInputSlice))


            if self.useCython:
              import esmfRegrid_cython as cweights
              #Call the weight application routine
              flatOutputSlice = cweights.applyESMFWeights(  inputField = flatInputSlice.astype(np.float), \
                                                            inputIndices = self.sourceIndices, \
                                                            outputIndices = self.destinationIndices, \
                                                            weightValues = self.weightValues, \
                                                            fractionalOverlap = self.fractionalOverlap, \
                                                            numOutput = len(flatOutputSlice), \
                                                            missingValue = fillValue)

              if handleMissing:
                flatOutputSlice = ma.masked_equal(flatOutputSlice,(fillValue))

            else:
              from fortranModuleSource.mod_ftnweights import mod_ftnweights as ftnweights
              #Call the weight application routine
              ftnweights.applyesmfweights(  inputfield = flatInputSlice, \
                                            outputfield = flatOutputSlice, \
                                            inputindices = self.sourceIndices, \
                                            outputindices = self.destinationIndices, \
                                            weightvalues = self.weightValues, \
                                            fractionaloverlap = self.fractionalOverlap, \
                                            handlemissingvalues = handleMissing, \
                                            missingvalue = fillValue)

              if handleMissing:
                flatOutputSlice = ma.masked_invalid(flatOutputSlice)

            #Unflatten the output field
            outputFieldSlice = reshape(flatOutputSlice,self.finalGridShape)
            if(not(self.doFlipSourceDimensions or self.sourceGridIsUnstructured)):
                outputFieldSlice = transpose(outputFieldSlice)

            outputField[leftSliceTuple + outputSliceTuple] = outputFieldSlice

        return outputField 


#*******************************************************************************
#*******************************************************************************
#************* Run some tests on the class *************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__" :


    import os
    import Nio
    from iliad.utilities import ncl
    import sys

    if len(sys.argv) == 1 :
      #Set the input file name
      inputFiles = ["flxf06.gdas.2010090100.grb2"]
    else:
      inputFiles = sys.argv[1:]


    for inputFile in inputFiles:
      #Open the input file
      try:
          fin = Nio.open_file(inputFile,"r")
      except BaseException as e:
          print "Error opening {}.  Exception is:\n\t{}".format(inputFile,e)
          raise e

      #Set the weight file
      #weightFile = "regrid-preproc-fv1.9x2.5/wgts_CFSR0.5deg-to-CAM-fv1.9x2.5-latxlon.nc"
      weightFile = "regrid-preproc-fv1.9x2.5/wgts_CFSR0.3deg-to-CAM-fv1.9x2.5-latxlon.nc"

      #Set the variable to interpolate
      #varName = "POT_P0_L104_GLL0"
      #varName = "VVEL_P0_L100_GLL0"
      varName = "HGT_P0_L1_GGA0"

      #Access the input variable
      inField = fin.variables[varName]

      #Interpolate to the output grid
      outField = esmfRegrid(weightFile,useCython=True).doRegrid(inField)

      #Write the results to a netCDF file
      fout = nc.Dataset("test_esmfRegrid.nc","w") 
      dimTuple = ()
      for r in range(len(shape(outField))):
          dimName = "dim_{}".format(r)
          dimTuple += (dimName,)
          fout.createDimension(dimName,shape(outField)[r])

      fout.createVariable(varName,'f8',dimTuple)
      fout.variables[varName][:] = outField

      fout.close()

      #Run an NCL-based test
      nclOutputFile = "test_esmfRegrid_nclstandard.nc"

      nclCode = """
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

  begin

      fin = addfile("{inputFile}","r")
      inField = fin->{varName}

      weightFile = "{weightFile}"

      opt = True
      opt@PrintTimings = True

      outField = ESMF_regrid_with_weights(inField,weightFile,opt)

      outfilename = "{outFile}"
      if(isfilepresent(outfilename))then
          system("rm " + outfilename)
      end if
      fout = addfile(outfilename,"c")
      fout->{varName} = outField

  status_exit(0)
  end
  status_exit(1)

  """.format(inputFile = inputFile, weightFile = weightFile, outFile = nclOutputFile, varName = varName)

      nclOutput = ncl.execute(nclCode)

      ftest = nc.Dataset(nclOutputFile,"r")
      nclField = ftest.variables[varName]

      fieldDifference = nclField[:] - outField[:]

      maxDiff = amax(abs(fieldDifference))
      avgDiff = average(fieldDifference)

      print "Field difference for {}: max = {}, average = {}".format(inputFile,maxDiff,avgDiff)

    print "Done."
