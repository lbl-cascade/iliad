#!/bin/bash
module load iliad

if [ ! -z "${NERSC_HOST}" ]; then
  CESM_INPUT=/project/projectdirs/ccsm1/inputdata
  SSTDIR=/project/projectdirs/m1949/observationDatasets/OISST/
  CFSDIR=/project/projectdirs/m1949/reanalyses/CFSv2/ 
  CFSRDIR=/project/projectdirs/m1949/reanalyses/CFSR/ 
else
  CESM_INPUT=/buffalo/data/cesm-input
  SSTDIR=/buffalo/data/OISST
  CFSDIR=/home/taobrien/mnt/dtn01/project/projectdirs/m1949/reanalyses/CFSv2/ 
  CFSRDIR=/home/taobrien/mnt/dtn01/project/projectdirs/m1949/reanalyses/CFSR/ 
fi

GENWEIGHTS=0
if [ "$GENWEIGHTS" -eq "1" ]; then
  python ${ILIAD_BASE}/core/generateRegriddingWeights.py \
    -p ${CFSRDIR}/pgbh06/pgbh06.gdas.2010090100.grb2  \
    -f ${CFSRDIR}/flxf06/flxf06.gdas.2010090100.grb2  \
    -o ${SSTDIR}/sst.wkmean.1990-present.nc  \
    -a ${CESM_INPUT}/atm/cam/chem/ic/f2000_tropmam3_alpha03c_chem2_1.9x2.5_L30_0009-01-01-00000_c130329.nc  \
    -l ${CESM_INPUT}/lnd/clm2/initdata/clmi.BCN.2000-01-01_1.9x2.5_gx1v6_simyr2000_c100309.nc \
    -s ${CESM_INPUT}/atm/cam/sst/sst_HadOIBl_bc_1.9x2.5_clim_c061031.nc \
    -t ${CESM_INPUT}/atm/cam/topo/USGS-gtopo30_1.9x2.5_remap_c050602.nc \
    -g fv1.9x2.5
fi

GENWEIGHTS2=1
if [ "$GENWEIGHTS2" -eq "1" ]; then
  python ${ILIAD_BASE}/core/generateRegriddingWeights.py \
    -p ${CFSDIR}/pgbh06/pgbh06.cdas1.2012082612.grib2 \
    -f ${CFSDIR}/flxf06/flxf06.cdas1.2012082612.grib2 \
    -o ${SSTDIR}/sst.wkmean.1990-present.nc  \
    -a ${CESM_INPUT}/atm/cam/chem/ic/f2000_tropmam3_alpha03c_chem2_1.9x2.5_L30_0009-01-01-00000_c130329.nc  \
    -l ${CESM_INPUT}/lnd/clm2/initdata/clmi.BCN.2000-01-01_1.9x2.5_gx1v6_simyr2000_c100309.nc \
    -s ${CESM_INPUT}/atm/cam/sst/sst_HadOIBl_bc_1.9x2.5_clim_c061031.nc \
    -t ${CESM_INPUT}/atm/cam/topo/USGS-gtopo30_1.9x2.5_remap_c050602.nc \
    -g fv1.9x2.5 \
    -n
fi


TESTCFSR=0
if [ "$TESTCFSR" -eq "1" ]; then
  #Test standard regridding from the CFSR dataset
  python ${ILIAD_BASE}/core/generateInitialConditions.py \
      -m regrid-preproc-fv1.9x2.5/  \
      -d /home/taobrien/mnt/dtn01/project/projectdirs/m1949/reanalyses/CFSR/  \
      -t 2001:09:30:06 
fi

TESTCFSV2=1
if [ "$TESTCFSV2" -eq "1" ]; then
  python ${ILIAD_BASE}/core/generateInitialConditions.py \
      -m ./regrid-preproc-fv1.9x2.5/  \
      -d ${CFSDIR}  \
      -t 2013:12:27:06 \
      -o ${SSTDIR}/sst.wkmean.1990-present.nc \
      -i ${SSTDIR}/icec.wkmean.1990-present.nc \
      --forecast
fi


TESTHALFDEGREE=0
if [ "$TESTHALFDEGREE" -eq "1" ]; then
  python ${ILIAD_BASE}/core/generateInitialConditions.py \
      -m ./regrid-preproc-fv0.47x0.63/  \
      -d ${CFSDIR}  \
      -t 2013:12:27:06 \
      -o ${SSTDIR}/sst.wkmean.1990-present.nc \
      -i ${SSTDIR}/icec.wkmean.1990-present.nc \
      -c \
      --forecast
fi

TESTPARALLELGIC=1
if [ "$TESTPARALLELGIC" -eq "1" ]; then
  mpirun -n 2 python ${ILIAD_BASE}/core/parallelGenerateICs.py \
      -m ./regrid-preproc-fv1.9x2.5/  \
      -d ${CFSDIR}  \
      -t 2013:01:01:00 \
      -e 2013:01:01:12 \
      --forecast
      #-c \
fi
