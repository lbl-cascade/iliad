from numpy import *
import netCDF4 as nc
from fortranModuleSource.mod_ftnweights import mod_ftnweights as ftnweights
from esmfRegrid import esmfRegrid

#*******************************************************************************
#*******************************************************************************
#************* Run some tests on the class *************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__" :


    import os
    import Nio
    import ncl
    import sys
    import standardAtmosphere as stdatm
    import earthPhysics as phys

    if len(sys.argv) == 1 :
      #Set the input file name
      inputFiles = ["pgbhnl.gdas.2010090100.grb2"]
    else:
      inputFiles = sys.argv[1:]

    for inputFile in inputFiles:
      #Open the input file
      try:
          fin = Nio.open_file(inputFile,"r")
      except BaseException as e:
          print "Error opening {}.  Exception is:\n\t{}".format(inputFile,e)
          raise e

      #Set the weight file
      #weightFile = "regrid-preproc-fv1.9x2.5/wgts_CFSR0.5deg-to-CAM-fv1.9x2.5-latxlon.nc"
      weightFile = "regrid-preproc-fv1.9x2.5/wgts_CFSR0.3deg-to-CAM-fv1.9x2.5-latxlon.nc"
      camTopoFile = "regrid-preproc-fv1.9x2.5/topo-0000-01-01_fv1.9x2.5.nc"

      #Set the variable to interpolate
      #varName = "POT_P0_L104_GLL0"
      #varName = "VVEL_P0_L100_GLL0"
      varName = "PRES_P0_L1_GGA0"

      #Access the input variable
      #inField = fin.variables[varName]

      psField = fin.variables["PRES_P0_L1_GGA0"]
      zsField = fin.variables["HGT_P0_L1_GGA0"]

      inField = stdatm.calculateSLP(psField[:],zsField[:])

      #Interpolate to the output grid
      #outField = esmfRegrid(weightFile).doRegrid(inField)
      slpOutField = esmfRegrid(weightFile).doRegrid(inField)

      ftopo = Nio.open_file(camTopoFile,"r")
      camZSField = ftopo.variables["PHIS"][:]
      camZSField /= phys.grav
      #outField = stdatm.calculatePS(slpOutField,camZSField)
      outField = slpOutField

      #Write the results to a netCDF file
      fout = nc.Dataset("test_esmfRegrid.nc","w") 
      dimTuple = ()
      for r in range(len(shape(outField))):
          dimName = "dim_{}".format(r)
          dimTuple += (dimName,)
          fout.createDimension(dimName,shape(outField)[r])

      fout.createVariable(varName,'f8',dimTuple)
      fout.variables[varName][:] = outField

      fout.close()

      #Run an NCL-based test
      nclOutputFile = "test_esmfRegrid_nclstandard.nc"

      nclCode = """
  load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
  load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"

  begin

      fin = addfile("{inputFile}","r")
      inField = fin->{varName}

      weightFile = "{weightFile}"

      opt = True
      opt@PrintTimings = True

      outField = ESMF_regrid_with_weights(inField,weightFile,opt)

      outfilename = "{outFile}"
      if(isfilepresent(outfilename))then
          system("rm " + outfilename)
      end if
      fout = addfile(outfilename,"c")
      fout->{varName} = outField

  status_exit(0)
  end
  status_exit(1)

  """.format(inputFile = inputFile, weightFile = weightFile, outFile = nclOutputFile, varName = varName)

      nclOutput = ncl.execute(nclCode)

      ftest = nc.Dataset(nclOutputFile,"r")
      nclField = ftest.variables[varName]

      fieldDifference = nclField[:] - outField[:]

      maxDiff = amax(abs(fieldDifference))
      avgDiff = average(fieldDifference)

      print "Field difference for {}: max = {}, average = {}".format(inputFile,maxDiff,avgDiff)

    print "Done."
