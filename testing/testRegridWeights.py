#!/bin/bash
from iliad.utilities import dateensemble
import argparse
from iliad.utilities import ncl

def executeNCLRegrid(
                mappingDirectory=mappingDirectory,\
                cfsDirectory = cfsDirectory, \
                gmtDateTime = callLineDate, \
                doClobber = doClobber, \
                beVerbose = beVerbose,  \
                isCFSv2Forecast = isCFSv2Forecast):
  pass

def testRegridWeights(    cfsDirectory, \
                          mappingDirectory, \
                          startDate,  \
                          endDate,  \
                          initialConditionDir=None, \
                          useMPI=True,  \
                          doClobber = False,  \
                          beVerbose = False,  \
                          beReallyVerbose = False,  \
                          isCFSv2Forecast = False):

  #TODO: Check that startdate and enddate are datetime instances
  #TODO: Check that enddate is after startdate
  #TODO: Check that the mapping directory exists
  #TODO: Check that the CFS directory exists

  #Initialize MPI if we are using it; otherwise set rank and mpisize to 0,1
  if(useMPI):
    from mpi4py import MPI
    #Initialize MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    mpisize = comm.Get_size()
  else:
    rank = 0
    mpisize = 1

  if(rank == 0):
    #Create date combinations
    dateEnsemble = dateensemble.ensemble(startDate,endDate).dates

    dateListScatter = divideListForScattering(dateEnsemble,mpisize)
  else:
    dateListScatter = []

  if(useMPI):
    #Scatter the date list 
    myDateList = comm.scatter(dateListScatter,root=0)
  else:
    myDateList = dateListScatter[0]

  #Loop over the dates provided
  for currentDate in myDateList:

    #Combine the times into a string with properly 0 paddings
    currentDateString = "{:04}{:02}{:02}{:02}".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)

    #Combine the times into a readable date string
    callLineDate = "{:04}:{:02}:{:02}:{:02}".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)
    fileCoreString = "{:04}-{:02}-{:02}-{:02}Z".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)

    #Run the command
    if(beVerbose):
      print "(rank {}): Testing regrid for {}".format(rank,fileCoreString)
    icgen = executeNCLRegrid(  \
                mappingDirectory=mappingDirectory,\
                cfsDirectory = cfsDirectory, \
                gmtDateTime = callLineDate, \
                doClobber = doClobber, \
                beVerbose = beVerbose,  \
                isCFSv2Forecast = isCFSv2Forecast)


if(__name__ == "__main__"):
  import argparse
  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--cfsdirectory','-d', \
                      help="Directory containing CFSR files",default=None)
  parser.add_argument('--mappingdirectory','-m', \
                      help="Directory containing the weight files",default="./")
  parser.add_argument('--gmtstartdate','-t', \
                      help="A GMT date/time for the start date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--gmtenddate','-e', \
                      help="A GMT date/time for the end date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--clobber','-c', \
                      help="Do not clobber any existing files",default=False,action='store_true')
  parser.add_argument('--forecast','-w', \
                      help="Flag that we are using a CFSv2 forecast",default=False,action='store_true')
  parser.add_argument('-varname', '-v', \
                      help="The NCL name of a variable in a CFS file",default=False,action='store_true')
  parser.add_argument('--serial','-s', \
                      help="Flag that we shouldn't use mpi4py",default=True,action='store_false')
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')

  parsedArgs = vars(parser.parse_args())
  cfsDirectory = parsedArgs['cfsdirectory']
  beVerbose = parsedArgs['quiet']
  doClobber = parsedArgs['clobber']
  gmtStartDate = parsedArgs['gmtstartdate']
  gmtEndDate = parsedArgs['gmtenddate']
  mappingDirectory = parsedArgs['mappingdirectory']
  isCFSv2Forecast = parsedArgs['forecast']
  useMPI = parsedArgs['serial']


  requiredSet = [cfsDirectory,mappingDirectory,gmtStartDate,gmtEndDate]

  #Check if any of the arguments are missing
  if(any( v is None for v in requiredSet)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  def dateStringToDatetime(dateString):
    import datetime as dt
    """Transforms a date string of the format "YYYY:MM:DD:HH" into a datetime object"""
    #Transform the dates into datetime objects
    try:
      return dt.datetime(*tuple([int(mystr) for mystr in dateString.split(":")]))
    except:
      raise ValueError,"Could not parse dateString.  Expecting date of form YYYY:MM:DD:HH, but got {}".format(dateString)

  #Run the generator class initializer
  testRegridWeights(  \
                  cfsDirectory = cfsDirectory,    \
                  mappingDirectory = mappingDirectory,    \
                  startDate = dateStringToDatetime(gmtStartDate),    \
                  endDate = dateStringToDatetime(gmtEndDate),    \
                  beVerbose = beVerbose,    \
                  doClobber = doClobber,    \
                  isCFSv2Forecast = isCFSv2Forecast,  \
                  useMPI = useMPI)

