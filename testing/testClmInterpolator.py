#!/usr/bin/env python
import os
from ilad.interpolation.fortranModuleSource.mod_ftnweights import mod_ftnweights as ftnweights
import netCDF4 as nc
from numpy import *
import iliad.interpolation.clmInterpolator as clmInterpolator
import pdb
"""
    Runs a sanity test on the clmInterpolator module.

    Takes a CFSR input and generates a CLM-like grid (a list of columns at
    specific lats/lons) based on valid data in the CFSR file.  It then perturbs
    the locations of these columns by a random amount and runs the
    interpolation routine on them.  Verification of the interpolation happens
    in two ways: (1) The code prints the average and RMS difference between the
    original and interpolated field (these values should be small, but not zero
    since the locations are perturbed). And (2) the code outputs a netCDF file
    with the interpolated values.  These can be visually compared with the
    original CFSR file.

"""


#*******************************************************************************
#*******************************************************************************
#**** Generate a CLM-like grid based on a slightly shifted CFSR file ***********
#*******************************************************************************
#*******************************************************************************
cfsrFile = "flxf06.gdas.2010090100.nc"
clmLikeFile = "clmlike.latlon.from.CFSRgrid.nc"

print "Reading CFSR file"
fcfsr_in = nc.Dataset(cfsrFile,"r")

#Read in CFSR lat/lons
nSubSet = 4
latCfsr = fcfsr_in.variables["lat_0"][::nSubSet]
lonCfsr = fcfsr_in.variables["lon_0"][::nSubSet]

#Read in the soil temperature so we can find points with valid temps
#soilTempCFSR = fcfsr_in.variables["TMP_P0_2L106_GGA0"][:]
#cfsrVariable3D = soilTempCFSR
#Read in soil ice
soilWaterCFSR = fcfsr_in.variables["SOILW_P0_2L106_GGA0"][:]
soilLiquidCFSR = fcfsr_in.variables["SOILL_P0_2L106_GGA0"][:]
soilICECFSR = soilWaterCFSR - soilLiquidCFSR
cfsrVariable3D = soilICECFSR
fillValue = fcfsr_in.variables["TMP_P0_2L106_GGA0"]._FillValue

#Find cells with data
iNotMissing = nonzero(cfsrVariable3D[0,::nSubSet,::nSubSet].mask.ravel() == False)[0]

#Create 2D grids
lon2DCfsr,lat2DCfsr = meshgrid(lonCfsr,latCfsr)

#Extract the lats/lons from cells that have data
latCLMLike = lat2DCfsr.ravel()[iNotMissing]
lonCLMLike = lon2DCfsr.ravel()[iNotMissing]

#Shift the grids by a tiny, random amount
stdDevShift = 0.05 #The RMS size of the random shift (in degrees)
ncolCLMLike = len(latCLMLike)
latCLMLike += stdDevShift*random.randn(ncolCLMLike)
lonCLMLike += stdDevShift*random.randn(ncolCLMLike)

doRemakeClmFile=False
if((not os.path.exists(clmLikeFile)) or doRemakeClmFile):
    print "Generating {}".format(clmLikeFile)
    #*******************************
    #Create a CLM-like file
    #*******************************
    fclmlike_out = nc.Dataset(clmLikeFile,"w")

    #Create the column dimension
    fclmlike_out.createDimension("column",ncolCLMLike)

    #Create the lat/lon variables
    fclmlike_out.createVariable("cols1d_lat",'f4',('column',))
    fclmlike_out.createVariable("cols1d_lon",'f4',('column',))

    #Write latitude metadata
    fclmlike_out.variables["cols1d_lat"].long_name = "Latitude"
    fclmlike_out.variables["cols1d_lat"].units = "degrees_north"
    #Write longitude metadata
    fclmlike_out.variables["cols1d_lon"].long_name = "Longitude"
    fclmlike_out.variables["cols1d_lon"].units = "degrees_east"

    fclmlike_out.variables["cols1d_lat"][:] = latCLMLike
    fclmlike_out.variables["cols1d_lon"][:] = lonCLMLike

    fclmlike_out.close()

#*******************************************************************************
#*******************************************************************************
#****************** Generate weights for the CLM-like grid *********************
#*******************************************************************************
#*******************************************************************************

doRemakeWgtFile = False
weightFile = 'wgts_CFSR0.3deg-to-CLMLike-CFSR-latxlon.nc'
if((not os.path.exists(weightFile)) or doRemakeWgtFile):
    print "Generating {}".format(weightFile)

    #Create a set of level variables to test the vertical extrapolation scheme
    thkCLMLike = array([0.0,0.05,0.05,0.3,0.6,1.0,1.0]) # Level thickness (m)
    ilevCLMLike = cumsum(thkCLMLike) #Interface level depth (m)
    levCLMLike = (ilevCLMLike[1:] + ilevCLMLike[:-1])/2. #Mid-level depth (m)
    #Drop the 0-thickness at the beginning of thkCLMLike (it was necessary for
    #cumsum to function properly)
    thkCLMLike = thkCLMLike[1:]

    myInterpolator = clmInterpolator.clmInterpolator(cfsrFile, \
                                                     clmLikeFile, \
                                                     weightFile=weightFile, \
                                                     ilevCLM = ilevCLMLike, \
                                                     levCLM = levCLMLike, \
                                                     thkCLM = thkCLMLike, \
                                                     )
else:
    print "Reading {}".format(weightFile)
    myInterpolator = clmInterpolator.clmInterpolator(weightFile=weightFile)

nlevCLMLike = len(myInterpolator.levCLM)

#*******************************************************************************
#*******************************************************************************
#***** Interpolate soil temp from the CFSR grid to the CLM-like grid ***********
#*******************************************************************************
#*******************************************************************************
print "Interpolating soil temperatures"

cfsrVariableInterp = myInterpolator.doInterpolation(cfsrVariable3D,fillValue=fillValue)

print ""

#*******************************************************************************
#*******************************************************************************
#****** Generate a copy of the CFSR file and overwrite with the new temps ******
#*******************************************************************************
#*******************************************************************************
standardFile = "interpolatedCFSRTemps.nc"

print "Generating {}".format(standardFile)

fstandard = nc.Dataset(standardFile,"w")

nlatCLM = len(latCfsr)
nlonCLM = len(lonCfsr)
nlevCLM = len(myInterpolator.levCLM)

#Create dimensions
fstandard.createDimension("lat_0",nlatCLM)
fstandard.createDimension("lon_0",nlonCLM)
fstandard.createDimension("lev_0",nlevCLM)

#Create the soil temperature variable
fstandard.createVariable("VAR_INTERPOLATED",'f4',("lev_0","lat_0","lon_0",),fill_value=fillValue)

fstandard.createVariable("lat_0",'f4',("lat_0",))
fstandard.variables["lat_0"].long_name = "latitude"
fstandard.variables["lat_0"].units = "degrees_north"
fstandard.createVariable("lon_0",'f4',("lon_0",))
fstandard.variables["lon_0"].long_name = "longitude"
fstandard.variables["lon_0"].units = "degrees_east"
fstandard.createVariable("lev_0",'f4',("lev_0",))
fstandard.variables["lev_0"].long_name = "level depth"
fstandard.variables["lev_0"].units = "m"

cfsrVariableInterp3D = fillValue*ones([nlevCLM,nlatCLM,nlonCLM])
#Load the interpolated points back into the cells that had data in the original field
for k in range(nlevCLMLike):
    cfsrVariableInterp3D[k,:,:].ravel()[iNotMissing] =  cfsrVariableInterp[:,k]

fstandard.variables["VAR_INTERPOLATED"][:] = cfsrVariableInterp3D
fstandard.variables["lat_0"][:] = latCfsr
fstandard.variables["lon_0"][:] = lonCfsr
fstandard.variables["lev_0"][:] = myInterpolator.levCLM

fstandard.close()

#*******************************************************************************
#*******************************************************************************
#***************** Print some summary statistics *******************************
#*******************************************************************************
#*******************************************************************************

rawDiff = cfsrVariableInterp3D[2,:,:] - cfsrVariable3D[1,::nSubSet,::nSubSet]
avgDiff = ma.average(rawDiff)
stdDiff = ma.std(rawDiff)

print "Average difference@ lev=0.25 m is {}, and the RMS difference is {}".format(avgDiff,stdDiff)
print ""
print "Done."
