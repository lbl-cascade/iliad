#!/usr/bin/env python
from numpy import *
import netCDF4 as nc
from iliad.utilities.conformDimensions import conformDimensions

fin = nc.Dataset("regrid-preproc-fv1.9x2.5/cami-0000-01-01_fv1.9x2.5.nc","r")

varName = "PS"
ps = fin.variables[varName]
tshape = shape(fin.variables["T"][:])
outDims = fin.variables["T"].dimensions

outField = conformDimensions(ps,tshape)

#Write the results to a netCDF file
fout = nc.Dataset("test_conform.nc","w")
dimTuple = ()
for r in range(len(shape(outField))):
    dimName = outDims[r]
    dimTuple += (dimName,)
    fout.createDimension(dimName,shape(outField)[r])

fout.createVariable(varName,'f8',dimTuple)
fout.variables[varName][:] = outField

fout.close()




