#!/usr/bin/env python
import netCDF4 as nc
from numpy import *

fin = nc.Dataset("cami-2013-01-01-00Z_fv1.9x2.5.nc","r")

lat = fin.variables["lat"][:]
slat = fin.variables["slat"][:]
lon = fin.variables["lon"][:]
slon = fin.variables["slon"][:]


slat_test = 0.5*(lat[1:] + lat[:-1])
print lat[:5]
print slat[:5]
print slat_test[:5]

print ""

slon_test = zeros(shape(lon))
slon_test[1:] = 0.5*(lon[1:] + lon[:-1])
slon_test[0] = 0.5*(lon[0] + lon[-1]) - 180.
print lon[:5]
print slon[:5]
print slon_test[:5]

