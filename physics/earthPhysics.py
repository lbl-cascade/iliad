#!/usr/bin/env python
from numpy import *

pi = 3.14159265358979323846     #pi
degtorad = arctan(1.)/45.       #pi/180
lv0 = 2.501e6                   # J/kg    Latent enthalpy of vap. at T0
li0 = 3.337e5                   # J/kg    Latent enthalpy of vap. at T0
ls0 = lv0 + li0                 # J/kg    Latent enthalpy of sublim. at T0
navo = 6.02214e26               #    Avogadro's #
kboltz = 1.38065e-23            # Boltzmann constant
mwv = 18.016                    # Molecular weight of water
mwdair = 28.966                 # Molecular weight of dry air
rvap = navo*kboltz/mwv          # J/kg/K  Dry gas constant for pure water vapor
rgas = navo*kboltz/mwdair       # J/kg/K  Dry gas constant for Earthy air
cpd = 1.00464e3                 # J/kg/K  Specific heat of dry Earthy air
cpv = 1.810e3                   # J/kg/K  Specific heat of H2O vapor
cpw = 4.188e3                   # J/kg/K  Specific heat of H2O liquid
cpi = 2.11727e3                 # J/kg/K  Specific heat of H2O solid
t0 = 273.16                     # K       Triple point of H20
es0 = 611.7                     # Pa      Vapor pressure over water at the triple pt
tmelt = 273.15                  # K       Melting point of H20
tice = 233.16                   # K       Homogenous ice nucleation temp
pref = 100000                   # Pa      Reference pressure for exner function
epsl = mwv/mwdair               #         Ratio of molar masses of water 
                                #         vapor and dry air
delt = (1.-epsl)/epsl           #         (1-epsl)/epsl
rhow = 1000.                    # kg/m^3  Density of pure liquid water at 4C
rhoi = 917.                     # kg/m^3  Density of solid ice
grav = 9.80616                  # m/s/s   Mean gravitational acceleration on Earth
rearth = 6.37122e6              # m    Mean radius of earth

#Constants for the saturation vapor pressure equations
useConstantsToCalculate = False
if(useConstantsToCalculate):
    Aw = (lv0+(cpw-cpv)*t0)/rvap
    Bw = (cpw - cpv)/rvap
    Ai = (ls0+(cpi-cpv)*t0)/rvap
    Bi = (cpi - cpv)/rvap
else:
    Aw = 6808.
    Bw = 5.09
    Ai = 6293.
    Bi = 0.555

def esatw(T):
  """
  Returns the saturation vapor pressure over liquid water at temperature T
  Following Bohren and Albrecht
  """
  return es0*exp(Aw*(1/t0 - 1/T) - Bw*log(T/t0))

def esati(T):
  """
  Returns the saturation vapor pressure over ice at temperature T
  Following Bohren and Albrecht
  """
  return es0*exp(Ai*(1/t0 - 1/T) - Bi*log(T/t0))

def esat(T):
  """
  Returns the saturation vapor pressure over water or ice, depending on the
  given temperature
  """
  try:
      tarray = array(T)
  except:
      raise ValueError,"T is not arraylike"

  #Create an array-like version of T
  tarray = asarray(T)

  #Calculate a weighting variable
  wgt = (tarray - tice)/(t0 - tice)

  #Create a return value with the same shape as T
  #initialize it with the saturation vapor pressure over ice
  esatRet = esati(T)

  #Find all values at or above freezing
  iGeT0 = nonzero(tarray.ravel() >= t0)[0]

  #...and calculate the saturation vapor pressure over water for these points
  esatRet.ravel()[iGeT0] = esatw(T.ravel()[iGeT0])

  #Find all values between freezing and the homogenous iceing point
  ibtwn = nonzero( logical_and( tarray.ravel() < t0,
                                tarray.ravel() >= tice  ))[0]

  esatRet.ravel()[ibtwn] = wgt.ravel()[ibtwn]*esatw(T.ravel()[ibtwn]) + (1.-wgt.ravel()[ibtwn])*esati(T.ravel()[ibtwn])

  #Return the saturation vapor pressure
  return esatRet

def efromq(q,p):
  """
  Converts specific humidity (q) to vapor pressure (e), given ambient pressure, p
  """
  return p*q/(q + epsl*(1-q))

def rhfromstate(p,T,q):
  """
  Calculates relative humidity relative to ice or water, given pressure (p), 
  emperature (T), and specific humidity (q)
  """
  return efromq(q,p)/esat(T)


if( __name__ == "__main__"):
  from esat import *
  tw = esatw_array[:,0] + tmelt
  esatw_ba = esatw_array[:,1]
  esatw_mod = esat(tw)/100

  esati_ba = esati_array[:,1]
  ti = esati_array[:,0] + tmelt
  esati_mod = esat(ti)/100

  for i in range(len(tw)):
    print "T = {} K, es_ba = {} mb, es = {} mb".format(tw[i],esatw_ba[i],esatw_mod[i])

  for i in range(len(ti)):
    print "T = {} K, es_ba = {} mb, es = {} mb".format(ti[i],esati_ba[i],esati_mod[i])
