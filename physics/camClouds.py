from numpy import *
from earthPhysics import *

def parkCloudFraction(U,P3D,Q):
    """Calculates cloud fraction following the Park et al. method from the CAM5 physics package"""

    c1 = -3./sqrt(2.)
    c2 = 2./3.
    c3 = 1./3.
    c4 = -c1/2.
    c5 = 2*pi
    cldrh = 1.0

    premib = 75000.
    premit = 75000.
    rhminl = 0.91
    rhminh = 0.80

    #Define a shorthand for the arccos() function
    acos = arccos

    igepremib = nonzero(P3D.ravel() >= premib)[0]
    iltpremit = nonzero(P3D.ravel() < premib)[0]

    if(premib != premit):
        rhwgt = (premib - P3D)/(premib-premit)
        rhwgt.ravel()[iltpremit] = 1.0
    else:
        rhwgt = zeros(shape(P3D))


    #Initialize the minimum RH value
    rhmin = rhminh*rhwgt + rhminl*(1.0-rhwgt)
    #Set RHmin for the lowest atmospheric levels
    rhmin.ravel()[igepremib] = rhminl
    #Set RHmin for the highest atmospheric levels
    rhmin.ravel()[iltpremit] = rhminh

    dV = cldrh - rhmin
    
    #Initialize cloud fraction
    cldFrac = zeros(shape(U))

    #Find supersaturated regions
    isupersat = nonzero(U.ravel() >= 1)[0]
    cldFrac.ravel()[isupersat] = 1.0

    #Calculate cloud fraction for RH region 1
    ireg1 = nonzero( logical_and(U.ravel() > (cldrh -dV.ravel()/6.), \
                                U.ravel() < 1                       ))[0]
    cldFrac.ravel()[ireg1] =  1. - (c1*(U.ravel()[ireg1]-cldrh)/dV.ravel()[ireg1])**c2

    #Calculate cloud fraction for RH region 2
    ireg2 = nonzero( logical_and(U.ravel() > (cldrh - dV.ravel()), \
                                U.ravel() <= (cldrh - dV.ravel()/6.)    ))[0]
    cldFrac.ravel()[ireg2] = 4.*(cos(c3*(acos(c4*(1. + (U.ravel()[ireg2] - cldrh)/dV.ravel()[ireg2] )) - c5 )))**2

    #Calculate the freezedrying coefficient
    freezeDry = Q/0.003
    #Set the freezeDry coefficient to 1 for the free troposphere (and don't let it be larger than 1)
    idum = nonzero(logical_or(  freezeDry.ravel() > 1.0,
                                P3D.ravel() < premib        ))[0]
    freezeDry.ravel()[idum] = 1.0
    
    #And don't let it go below 0.15
    idum = nonzero(freezeDry.ravel() < 0.15)[0]
    freezeDry.ravel()[idum] = 0.15

    #Multiply cloud fraction by the freezedrying coefficient
    cldFrac = cldFrac * freezeDry

    return cldFrac


if __name__ == "__main__":

    #import netCDF4 as nc
    import Nio
    from conformDimensions import conformDimensions
    import earthPhysics as phys

    #ftestio = nc.Dataset("camCldTest.nc","r+")
    ftestio = Nio.open_file("camCldTest.nc","w")

    Q = ftestio.variables["Q"][:]
    T = ftestio.variables["T"][:]
    PS = ftestio.variables["PS"][:]
    hyam = ftestio.variables["hyam"][:]
    hybm = ftestio.variables["hybm"][:]
    P0 = ftestio.variables["P0"].get_value()


    #Calculate pressure
    P = conformDimensions(P0*hyam,shape(T)) +   conformDimensions(hybm,shape(T))\
                                                *conformDimensions(PS,shape(T))

    RH = phys.rhfromstate(P,T,Q)

    CLD = parkCloudFraction(RH,P,Q)

    ftestio.variables["CLOUD_CALC"][:] = CLD
