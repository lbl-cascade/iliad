from earthPhysics import *

#Calculate and define some additional constants
stdLapseRate = 6.5e-3 # standard lapse rate [K/m]
stdTemperature = 288.15 # Kelvin
stdPressure = 101325. # Pa
stdInvHeight = stdLapseRate/stdTemperature # 1/m
stdNegExponent = -grav/(rgas*stdLapseRate)
stdPosExponent = grav/(rgas*stdLapseRate)
stdPosExnerExponent = rgas/cpd
stdNegExnerExponent = -rgas/cpd


def calculateSLP(surfacePressure,surfaceElevation):
    """Returns the surface sea level pressure from the raw surface pressure and surface height, assuming a standard atmosphere
    """
    return surfacePressure*(1.0 - stdInvHeight*surfaceElevation)**stdNegExponent

def calculatePS(seaLevelPressure,surfaceElevation):
    """Returns the surface pressure from the sea level pressure and surface height, assuming a standard atmosphere
    """
    return seaLevelPressure*(1.0 - stdInvHeight*surfaceElevation)**stdPosExponent

def calculateSLT(temperature,pressure):
    """Returns the sea-level temperature from the actual temperature and pressure, assuming a standard atmosphere
    """
    return temperature*(pressure/stdPressure)**stdNegExnerExponent

def calculateT(seaLevelTemperature,pressure):
    """Returns the temperature given the sea level temperature and pressure, assuming a standard atmosphere
    """
    return seaLevelTemperature*(pressure/stdPressure)**stdPosExnerExponent


if __name__ == "__main__":
    import os
    import netCDF4 as nc

    #Do some code validation based on the ATM 120 homework 18 Q3b (from 2010)
    ps = 94030. #Pa
    zs = 639.8  #m

    slpAnswer = 101500. 
    slpFunc = calculateSLP(ps,zs)
    psFunc = calculatePS(slpAnswer,zs)

    print("From calculateSLP(), and from HW 18 3b Key: {} Pa, {} Pa".format(slpFunc,slpAnswer))

    print("From calculatePS(), and from HW 18 3b Key: {} Pa, {} Pa".format(psFunc,ps))

    print("")

    ts = 280.   #K
    p = 90000.  #Pa
    sltFunc = calculateSLT(ts,p)
    tsFunc = calculateT(sltFunc,p)
    print "From calculateSLT(): {} K".format(sltFunc)
    print "From calculateT() and original T: {} K, {} K".format(tsFunc,ts)

    print("")

    print("Running 2D test")

    psFileName = "cami-mam3_0000-01-01_1.9x2.5_L30_c090306.nc"
    phisFileName = "USGS-gtopo30_1.9x2.5_remap_c050602.nc"

    if(not os.path.exists(psFileName)):
        print("Warning: "+psFileName+" not found.  Skipping 2D SLP calculation test.")
        quit()
    if(not os.path.exists(phisFileName)):
        print("Warning: "+phisFileName+" not found.  Skipping 2D SLP calculation test.")
        quit()

    fps = nc.Dataset(psFileName,"r")
    fphis = nc.Dataset(phisFileName,"r")

    ps = fps.variables["PS"][0,:,:]
    phis = fphis.variables["PHIS"][:,:]

    zs = phis/grav
    slp = ps
    slp = calculateSLP(ps,zs)

    outFileName = "slp2DTest.nc"
    fout = nc.Dataset(outFileName,"w")

    dimTuple = ()
    for r in range(len(shape(slp))):
        dimName = "dim_{}".format(r)
        dimTuple += (dimName,)
        fout.createDimension(dimName,shape(slp)[r])

    fout.createVariable("SLP",'f8',dimTuple)
    
    fout.variables["SLP"][:] = slp

    print("2D test complete")
