import netCDF4 as nc
from numpy import *

def printMinMax(**kwargs):
  for key,value in kwargs.iteritems():
    print "\t{}\t--\tmin:\t{}, max:\t{}".format(key,amin(value),amax(value))

if __name__ == "__main__":
  fin = nc.Dataset("./clmi-2013-01-01-00Z_fv1.9x2.5.nc","r")

  tg = fin.variables["T_GRND"][:]

  printMinMax(T_GRND = tg)

