#!/usr/bin/env python
#PBS -l walltime=48:00:00
#PBS -q regular
#PBS -l mppwidth=1
"""
Archives one or more ILIAD case directories to HPSS.
"""

import os
import sys
import argparse
import subprocess

#*******************************************************************************
#*******************************************************************************
#********************** Parse command line arguments ***************************
#*******************************************************************************
#*******************************************************************************
parser = argparse.ArgumentParser( \
                                description = "Archive ILIAD case directories", \
                                formatter_class = argparse.RawDescriptionHelpFormatter, \
                                epilog = __doc__)

parser.add_argument('--clobber','-c', \
                  help="Clobber existing output file (unused)",default=False,action='store_true')
parser.add_argument('--quiet','-q', \
                  help="Suppress diagnostic printing output",default=False,action='store_true')
parser.add_argument('--globus','-g', \
                  help="Use globus as the transfer backend (default is htar)",default=False,action='store_true')
parser.add_argument('--username','-u', \
                  help="Globus username",default='taobrienlbl')
parser.add_argument('--token','-t', \
                  help="Globus goauth token",default=None)
parser.add_argument('--auth','-a', \
                  help="Globus authorization method",default='goauth')
parser.add_argument('--scratchdir','-s', \
                  help="Staging directory for tar files",default='{scratch}/cascade/scratch'.format(scratch=os.environ['SCRATCH']))
parser.add_argument('--procs','-n',type=int, \
                  help="Number of processes to use for archiving",default=9)

parser.add_argument('inputdir',nargs='*',help="ILIAD case directory names")

parsedArgs = vars(parser.parse_args())
inDirs = parsedArgs['inputdir'][:]
doClobber = parsedArgs['clobber']
beVerbose = not parsedArgs['quiet']
username = parsedArgs['username']
goauth_token = parsedArgs['token']
auth_method = parsedArgs['auth']
tar_procs = int(parsedArgs['procs'])
tar_scratch = parsedArgs['scratchdir']
use_globus = parsedArgs['globus']

if len(inDirs) == 0:
    print "Error: at least 1 ILIAD case directory is required"
    print ""
    parser.print_help()
    quit()

#Set the default argument for the goauth_token
if goauth_token is None:
    goauth_token_file = '{home}/.globus/goauth.token'.format(home=os.environ['HOME'])
    #Read the token
    if auth_method == 'goauth':
        with open(goauth_token_file,'r') as fin:
            goauth_token = fin.read().rstrip()

if use_globus:
    from iliad.archival import archive_iliad as arch
else:
    from iliad.archival import archive_iliad_htar as arch

#Initialize the archival system (may prompt for Globus password if globus method is used)
archiveOBJ = arch.ArchiveILIAD(username=username, \
                               be_verbose = beVerbose, \
                               auth_method = auth_method, \
                               goauth_token = goauth_token, \
                               tar_scratch = tar_scratch, \
                               tar_procs = tar_procs)

#Loop through all the input directories and archive them
failed_archive_calls = []
for archDir in inDirs:
    new_failures = archiveOBJ.archive(source_path = archDir)
    try:
        failed_archive_calls += new_failures
        if len(new_failures) > 0 and beVerbose:
            print "{} failures in archiving {}:\n".format(len(new_failures),archDir)
            for call in new_failures:
                print call,'\n'
    except:
        pass

if beVerbose:
    print "All archivals complete."

if len(failed_archive_calls) > 0:
    print   """
############################################
Failures from the following archive calls:
############################################

            """
    for call in failed_archive_calls:
        print call,'\n'

