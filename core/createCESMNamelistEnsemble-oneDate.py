#!/usr/bin/env python
from iliad.utilities import dateensemble as de
from mako.template import Template
import os

class updateNamelistEnsemble:

  def __init__( self, \
                startDate = None,   \
                endDate = None,     \
                forecastHours = None,  \
                dateEnsemble = None,  \
                templateDirectory = "./", \
                ):

    #Generate the date ensemble
    if not isinstance(dateEnsemble,de.ensemble):
      dateEnsembleArgs = {}

      #Check that startDate and endDate were given
      if startDate is not None and endDate is not None:
        #Add them to the keyword arguments
        dateEnsembleArgs['startdate'] = startDate
        dateEnsembleArgs['enddate'] = endDate
      else:
        raise ValueError,"A start date and end date both need to be provided, or a dateensemble object needs to be provided."

      #If forecastHours was provided, add it to the keyword arguments
      if forecastHours is not None:
        dateEnsembleArgs['forecastHours'] = forecastHours

      #Create the dateensemble object
      try:
        dateEnsemble = de.ensemble(**dateEnsembleArgs)
      except BaseException as e:
        raise BaseException,e


    #Set the dateEnsemble
    self.dateEnsemble = dateEnsemble
    #Set the number of dates
    self.numberOfDates = len(dateEnsemble.dates)

    #Set the namelist files
    templateFileNames = [ \
                                "atm_in_0001",   \
                                "atm_modelio.nml_0001",  \
                                "docn_in_0001",  \
                                "docn_ocn_in_0001", \
                                "docn.streams.txt.prescribed_0001", \
                                "glc_modelio.nml_0001",  \
                                "ice_in_0001",   \
                                "ice_modelio.nml_0001",  \
                                "lnd_in_0001",   \
                                "lnd_modelio.nml_0001",  \
                                "ocn_modelio.nml_0001",  \
                                "rof_in_0001",   \
                                "rof_modelio.nml_0001",  \
                                "wav_modelio.nml_0001",  \
                            ]


    #Check for template file; raise an error if they're not present
    for value in templateFileNames:
      candidateFileName = "{}/{}".format(templateDirectory,value)
      if not os.path.exists(candidateFileName):
        raise ValueError,"{} does not exist.".format(candidateFileName)

    #Save the template file names and directory
    self.templateFileNames = templateFileNames
    self.templateDirectory = templateDirectory


  def createNamelistTemplates(self,\
                              initialConditionDirectory,  \
                              domainName, \
                              namelistDirectory = "./"):
    """Uses mako to create namelist template files for a concurrent CESM simulation"""

    for i,date in zip(range(self.numberOfDates),self.dateEnsemble.dates):
      ensembleNumber = "{:04}".format(i+1)

      niceDateString = "{:04}-{:02}-{:02}-{:02}Z".format(date.year,date.month,date.day,date.hour)

      #Set initial condition file names
      camifilename = "cami-{}_{}.nc".format(niceDateString,domainName)
      clmifilename = "clmi-{}_{}.nc".format(niceDateString,domainName)
      sstfilename = "sst-{}_{}.nc".format(niceDateString,domainName)

      #Loop over the template files and use mako to input values for the current date
      for j in range(len(self.templateFileNames)):
        #****************************************
        # Use mako to generate namelist files
        #****************************************
        pendingTemplate = Template(filename="{}/{}".format(self.templateDirectory,self.templateFileNames[j]))

        #Use mako to render the namelist file
        pendingNamelist = pendingTemplate.render( \
                                    inidir = initialConditionDirectory, \
                                    camifilename = camifilename,        \
                                    clmifilename = clmifilename,        \
                                    sstfilename = sstfilename,          \
                                    ensemblenumber = ensembleNumber, \
                                    startyyyymmdd = "{:04}{:02}{:02}".format(date.year,date.month,date.day),    \
                                    )

        #*************************************
        # Write the template file to disk
        #*************************************
        #Set the file name
        templateFileName = "{}/{}_{}".format( namelistDirectory, \
                                              self.templateFileNames[j][:-5],  \
                                              ensembleNumber)
        #Write to disk
        with open(templateFileName,"w") as fout:
          fout.write(pendingNamelist)

#*******************************************************************************
#*******************************************************************************
#************************ Execute from the command line ************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":

  import datetime as dt
  import argparse

  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--icdirectory','-i', \
                      help="Directory containing CESM initial condition files (generated by parallelGICs.py)",default=None)
  parser.add_argument('--domain','-m', \
                      help="The ILIAD domain name (e.g., fv1.9x2.5)",default=None)
  parser.add_argument('--gmtstartdate','-t', \
                      help="A GMT date/time for the start date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--gmtenddate','-e', \
                      help="A GMT date/time for the end date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--templatedir','-d', \
                      help="The directory containing template CESM namelist files",default=None)
  parser.add_argument('--outputdir','-o', \
                      help="The directory in which to write templated namelist files",default=None)
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')
  parser.add_argument('--forecasthours','-f', \
                      help="The hours over which to run the hindcasts (should be a comma separated list)",default="0,6,12,18")

  parsedArgs = vars(parser.parse_args())
  initialConditionDirectory = parsedArgs['icdirectory']
  beVerbose = parsedArgs['quiet']
  gmtStartDate = parsedArgs['gmtstartdate']
  gmtEndDate = parsedArgs['gmtenddate']
  templateDirectory = parsedArgs['templatedir']
  namelistDirectory = parsedArgs['outputdir']
  domainName = parsedArgs['domain']
  try:
    forecastHours = [int(hour) for hour in parsedArgs['forecasthours'].split(",")]
  except:
    parser.print_help()
    print "Error: could not parse arguments for --forecasthours: {}".format(parsedArgs['forecasthours'])
    quit()

  requiredSet = [ initialConditionDirectory, gmtStartDate, gmtEndDate, templateDirectory, namelistDirectory, domainName ]

  #Check if any of the arguments are missing
  if(any( v is None for v in requiredSet)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  #***************************
  # initialize the templater
  #***************************
  updateObject = updateNamelistEnsemble(  \
                        startDate = de.dateStringToDatetime(gmtStartDate),  \
                        endDate = de.dateStringToDatetime(gmtEndDate),      \
                        forecastHours = forecastHours, \
                        templateDirectory = templateDirectory)

  #***************************
  # do the templating
  #***************************
  updateObject.createNamelistTemplates(   \
                        initialConditionDirectory = initialConditionDirectory,  \
                        domainName = domainName,  \
                        namelistDirectory = namelistDirectory)
                        

