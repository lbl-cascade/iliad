#!/usr/bin/env python
import os
import stat
import shutil
import argparse
from iliad.utilities import ncl
from iliad.interpolation import clmInterpolator
import netCDF4 as nc
from iliad.utilities.codeversion import codeVersionID
import iliad.physics.earthPhysics as phys
import tempfile
import numpy as np

class generateRegriddingWeights():

    def __init__(   self, \
                    cfsHFile,               \
                    cfsFFile,               \
                    camFile,                \
                    clmFile,                \
                    oisstFile,              \
                    camGridName,            \
                    camSSTFile,             \
                    topoFile,               \
                    beVerbose = True,       \
                    doClobber = False,      \
                    doUseNCL = False,       \
                    camGridIsUnstructured=False,  \
                    sstGridIsUnstructured=False,  \
                ):

#*******************************************************************************
#*******************************************************************************
#********************* Copy required files *************************************
#*******************************************************************************
#*******************************************************************************
        #Create the domain directory
        gridDirectory = "./regrid-preproc-{}".format(camGridName)
        if( not os.path.isdir(gridDirectory) ):
          os.mkdir(gridDirectory)

        #Create a mapping of file names that are regular and predictable
        fileMapping = [ \
            [camFile,"cami-0000-01-01_{}.nc".format(camGridName)], \
            [clmFile,"clmi-0000-01-01_{}.nc".format(camGridName)], \
            [camSSTFile,"sst-0000-01-01_{}.nc".format(camGridName)], \
            [topoFile,"topo-0000-01-01_{}.nc".format(camGridName)] \
            ]

        for srcFile,destFile in fileMapping:
          #Prepend the grid directory to the new file path
          newFile = "{}/{}".format(gridDirectory,destFile)

          #Copy the file to the grid directory
          if(not os.path.exists(newFile) or doClobber):
            if(beVerbose):
              print "Copying {} to {}".format(srcFile,newFile)
            shutil.copy(srcFile,newFile)

            #Make sure that the file is modifiable
            os.chmod(newFile, \
                 stat.S_IREAD | stat.S_IWRITE | stat.S_IRGRP | stat.S_IWGRP |stat.S_IROTH )

          #Update the path the match the newly copied file
          srcFile = newFile

#*******************************************************************************
#*******************************************************************************
#********************* Generate SCRIP files ************************************
#*******************************************************************************
#*******************************************************************************

        self.getNCLScriptPaths()

        if(camGridIsUnstructured):
        #If this is an unstructured grid then we need to generate ESMF grid description files
        #instead of SCRIP files
          #Set the expected SCRIP (ESMF) file names
          camLatLonScrip = "{}/cam-{}-ESMF.nc".format(gridDirectory,camGridName)
          camSLatLonScrip = None
          camLatSLonScrip = None
          if( not os.path.exists(camLatLonScrip) or doClobber ):
            if(beVerbose):
              print "Generating the CAM ESMF file"
            #Generate the CAM ESMF file
            ncl.execute(self.scriptDict['camESMF'], \
                        gridFileName = camFile,     \
                        camDomainName = camGridName,\
                        gridDirectory = gridDirectory)

          #*******************************************************************
          # Create a grid with nearly the same resolution as the unstructured
          # grid (for interpolation later)
          #*******************************************************************
          #Get the minimum area of the grid
          minimumGridArea = np.amin(nc.Dataset(camLatLonScrip,"r").variables["elementArea"][:])
          #Get the grid resolution
          gridResolution = 2*np.sqrt(minimumGridArea)/phys.degtorad
          #Create lat/lon variables
          gridLat = np.arange(-90+gridResolution,90,gridResolution)
          gridLon = np.arange(0.0,360.,gridResolution)
          #Create a temporary netCDF file with the lat/lon variables
          tempgridfile = tempfile.NamedTemporaryFile(delete=False)
          #Open the fiel
          fgrid = nc.Dataset(tempgridfile.name,"w")
          #Create dimensions
          fgrid.createDimension('lat',len(gridLat))
          fgrid.createDimension('lon',len(gridLon))
          #Create variables
          fgrid.createVariable('lat','f8',('lat',))
          fgrid.createVariable('lon','f8',('lon',))
          #Set variable attributes
          fgrid.variables['lat'].long_name = "latitude"
          fgrid.variables['lat'].units = "degrees_north"
          fgrid.variables['lon'].long_name = "longitude"
          fgrid.variables['lon'].units = "degrees_east"
          #Write variables
          fgrid.variables['lat'][:] = gridLat
          fgrid.variables['lon'][:] = gridLon
          #Close the file
          fgrid.sync()
          fgrid.close()

          #Create the scrip file
          camStructuredGridScrip = "{}/cam-{}_gridded-latxlon-SCRIP.nc".format(gridDirectory,camGridName)
          ncl.execute(  self.scriptDict['fixedScrip'], \
                        gridFileName = tempgridfile.name, \
                        camDomainName = camGridName, \
                        gridDirectory = gridDirectory)

          #Allow deletion of tempgridfile
          tempgridfile.delete = True

        else:
          #Set the expected SCRIP file names
          camLatLonScrip = "{}/cam-{}-latxlon-SCRIP.nc".format(gridDirectory,camGridName)
          camSLatLonScrip = "{}/cam-{}-slatxlon-SCRIP.nc".format(gridDirectory,camGridName)
          camLatSLonScrip = "{}/cam-{}-latxslon-SCRIP.nc".format(gridDirectory,camGridName)
          if( not all(os.path.exists(sfile) for sfile in [camLatLonScrip,camSLatLonScrip,camLatSLonScrip]) \
             or doClobber):
            if(beVerbose):
              print "Generating the CAM Scrip files"
            #Generate the CAM scrip files
            ncl.execute(self.scriptDict['camScrip'],  \
                        gridFileName = camFile, \
                        camDomainName = camGridName,  \
                        gridDirectory = gridDirectory)

        #Set the expected SCRIP file names
        cfsHScrip = "{}/cfsv2-0.5deg-SCRIP.nc".format(gridDirectory)
        cfsFScrip = "{}/cfsv2-0.3deg-SCRIP.nc".format(gridDirectory)
        if( not all(os.path.exists(sfile) for sfile in [cfsHScrip,cfsFScrip]) or doClobber ):
          if(beVerbose):
            print "Generating the CFS Scrip files"
          #Generate the CFS scrip files
          ncl.execute(self.scriptDict['cfsScrip'],  \
                      HgridFileName = cfsHFile,\
                      FgridFileName = cfsFFile, \
                      gridDirectory = gridDirectory)


        #Generate a SCRIP file for the CESM SSTs
        cesmSSTScrip = "{}/cesmsst-{}-latxlon-SCRIP.nc".format(gridDirectory,camGridName)
        if not os.path.exists(cesmSSTScrip) or doClobber:
          if(beVerbose):
            print "Generating the CESM SST Scrip files"
          #Generate the CFS scrip files
          ncl.execute(self.scriptDict['cesmSSTScrip'],  \
                      gridFileName = camSSTFile,\
                      camDomainName = camGridName, \
                      gridDirectory = gridDirectory)


        if(oisstFile is not None):
          #Set the expected SCRIP file name
          sstScrip = "{}/oisst-1deg-SCRIP.nc".format(gridDirectory) 
          if(not os.path.exists(sstScrip) or doClobber):
            if(beVerbose):
              print "Generating the OISST Scrip file"
            #Generate the SST scrip file if a SST file was provided
            ncl.execute(self.scriptDict['sstScrip'],  \
                        gridFileName = oisstFile, \
                        gridDirectory = gridDirectory)
        else:
          sstScrip = None

#*******************************************************************************
#*******************************************************************************
#********************* Generate Weight files ***********************************
#*******************************************************************************
#*******************************************************************************

        #Create a list of src/dest/wgt file triads
        fileQuads = [ \
            [cfsFFile,clmFile,"{}/wgts_CFSR0.3deg-to-CLM-{}-latxlon.nc".format(gridDirectory,camGridName),False], \
            [cfsHScrip,camLatLonScrip,"{}/wgts_CFSR0.5deg-to-CAM-{}-latxlon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [cfsHScrip,camSLatLonScrip,"{}/wgts_CFSR0.5deg-to-CAM-{}-slatxlon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [cfsHScrip,camLatSLonScrip,"{}/wgts_CFSR0.5deg-to-CAM-{}-latxslon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [cfsFScrip,camLatLonScrip,"{}/wgts_CFSR0.3deg-to-CAM-{}-latxlon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [cfsFScrip,camSLatLonScrip,"{}/wgts_CFSR0.3deg-to-CAM-{}-slatxlon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [cfsFScrip,camLatSLonScrip,"{}/wgts_CFSR0.3deg-to-CAM-{}-latxslon.nc".format(gridDirectory,camGridName),camGridIsUnstructured], \
            [sstScrip,cesmSSTScrip,"{}/wgts_OISST1deg-to-CESMSST-{}-latxlon.nc".format(gridDirectory,camGridName),sstGridIsUnstructured], \
            [cfsFScrip,cesmSSTScrip,"{}/wgts_CFSR0.3deg-to-CESMSST-{}-latxlon.nc".format(gridDirectory,camGridName),sstGridIsUnstructured], \
        ]

        #Add the weight file for the near-resolution structured grid
        if(camGridIsUnstructured):
          fileQuads.append( \
              [camLatLonScrip,camStructuredGridScrip,"{}/wgts_CAM-{}-unstructured-to-latlon.nc".format(gridDirectory,camGridName),False ])


        #Get the revision ID
        revision = codeVersionID(file=__file__)

        for src,dest,wgt,isUnstructured in fileQuads:
          if(src is not None and dest is not None):
            if(not os.path.exists(wgt) or doClobber):
              if(beVerbose):
                print "Generating {} from {} and {}".format(wgt,src,dest)
              #Check if this is a CLM file--we need to use a different weight generation method if so
              if("CLM" in wgt):
                #Generate the CLM weight file
                clmInterpolator.clmInterpolator(cfsrFile=src,clmFile=dest,weightFile=wgt)
              #Otherwise, use the esmf regridding method
              else:
                #Flag that we don't need information about the vertical grids
                doNeedVertical = False
                srcIsUnstructured = False

                #Check if this is a 0.5deg file (3D); if so, we need vertical information
                if("0.5deg" in wgt):
                  doNeedVertical = True

                if("unstructured" in wgt):
                  srcIsUnstructured = True

                #Execute the ESMF regridder script to generate the weight file
                #ncl.execute(scriptBasePath+"/esmfGenerateWeights.ncl", \
                self.esmfGenerateWeights(            \
                            srcSCRIPFile = src, \
                            destSCRIPFile = dest, \
                            wgtFileName = wgt, \
                            camIsUnstructured = isUnstructured, \
                            srcIsUnstructured = srcIsUnstructured, \
                            doNeedVertical = doNeedVertical, \
                            gridDirectory = gridDirectory,  \
                            doUseNCL = doUseNCL)

              #Add metadata to the weight file
              if(beVerbose):
                print "Adding metadata to {}".format(wgt)
              fio = nc.Dataset(wgt,"r+")
              fio.ILIAD_author = "Travis A. O'Brien"
              fio.ILIAD_contact = "TAOBrien@lbl.gov"
              fio.ILIAD_master_script = "generateRegriddingWeights.py"
              if("CLM" in wgt):
                fio.ILIAD_weight_script = "clmInterpolator.py"
              else:
                fio.ILIAD_weight_script = "esmfGenerateWeights.ncl"
              fio.ILIAD_script_revision = str(revision.ID)
              fio.ILIAD_revision_branch = str(revision.branch)
              fio.ILIAD_canonical_repository = "ssh://hg@bitbucket.org/taobrienlbl/iliad"
              fio.source_file = src
              fio.dest_file = src

              fio.close()


#*******************************************************************************
#*******************************************************************************
#*************** Generate NCL Script Names (as dict) ***************************
#*******************************************************************************
#*******************************************************************************

    def getNCLScriptPaths(self,scriptBasePath=None):
      if(scriptBasePath is None):
          #Set the paths of scripts
          try:
            #First try to get it from the environment
            scriptBasePath = os.environ['CFSNCL']
          except KeyError:
            #Second try to discern the ncl directory name from this
            #file's path (assuming that it is in the iliad package's
            #standard directory structure)
            interpPackagePath = os.path.dirname(__file__)
            #Get the directory that is one up
            iliadBasePath = "/".join(interpPackagePath.split('/')[:-1])
            scriptBasePath = "{}/interpolation/ncl".format(iliadBasePath)

            #Check that this exists
            if not os.path.isdir(scriptBasePath):
              #Otherwise, default to ./ 
              scriptBasePath = './ncl'
              
            #set the environment variable so that the NCL scripts 
            #will work
            os.environ['CFSNCL'] = scriptBasePath


      self.scriptDict = { \
          'camScrip' : scriptBasePath+'/generateCAMScripRect.ncl', \
          'camESMF' : scriptBasePath+'/generateCAMSEUnstructuredESMF.ncl', \
          'cfsScrip' : scriptBasePath+'/generateCFSv2Scrip.ncl', \
          'sstScrip' : scriptBasePath+'/generateOISSTScrip.ncl', \
          'esmfWeight' : scriptBasePath+'/esmfGenerateWeights.ncl', \
          'cesmSSTScrip' : scriptBasePath+'/generateCESMSSTScripRect.ncl', \
          'fixedScrip' : scriptBasePath+'/generateFixedGridScrip.ncl', \
      }

#*******************************************************************************
#*******************************************************************************
#******************* Do the weight generation **********************************
#*******************************************************************************
#*******************************************************************************
    def esmfGenerateWeights(self,    \
                        srcSCRIPFile, \
                        destSCRIPFile, \
                        wgtFileName, \
                        camIsUnstructured, \
                        doNeedVertical, \
                        gridDirectory,  \
                        srcIsUnstructured = False, \
                        doUseNCL = False):
        """Generate ESMF Regridding weights.
        
        If doUseNCL is True, it uses the NCL code in esmfGenerateWeights.ncl
        file, which ultimately uses the ESMF 5.2 weight generation program.  If
        it is false, we directly call the ESMF weight generation program
        available in the path.

        """

        if(doUseNCL):

            #Execute the ESMF regridder script to generate the weight file
            ncl.execute(self.scriptDict['esmfWeight'],   \
                        srcSCRIPFile = srcSCRIPFile, \
                        destSCRIPFile = destSCRIPFile, \
                        wgtFileName = wgtFileName, \
                        camIsUnstructured = camIsUnstructured, \
                        srcIsUnstructured = srcIsUnstructured, \
                        doNeedVertical = doNeedVertical, \
                        gridDirectory = gridDirectory)





if( __name__ == '__main__' ):

#*******************************************************************************
#*******************************************************************************
#********************** Parse command line arguments ***************************
#*******************************************************************************
#*******************************************************************************
    parser = argparse.ArgumentParser()

    parser.add_argument('--cfsh','-p', \
                        help="An existing CFSv2 3D file (on h grid)",default=None)
    parser.add_argument('--cfsf','-f', \
                        help="An existing CFSv2 2D file (on f grid)",default=None)
    parser.add_argument('--oisst','-o', \
                        help="An OISST file (optional)",default=None)
    parser.add_argument('--cam','-a', \
                        help="An existing CAM initial condition file for desired grid",default=None)
    parser.add_argument('--clm','-l', \
                        help="An existing CLM initial condition file for desired grid",default=None)
    parser.add_argument('--sst','-s', \
                        help="An existing CAM SST file for desired grid",default=None)
    parser.add_argument('--unstructured','-u', \
                        help="CAM grid is unstructured",default=False,action='store_true')
    parser.add_argument('--camgrid','-g', \
                        help="The name of the CAM grid",default="unnamedCAMgrid")
    parser.add_argument('--topo','-t', \
                        help="An existing CAM topography file",default=None)
    parser.add_argument('--clobber','-c', \
                        help="Do not clobber any existing files",default=False,action='store_true')
    parser.add_argument('--quiet','-q', \
                        help="Don't print status messages",default=True,action='store_false')
    parser.add_argument('--ncl','-n', \
                        help="Use NCL to do the weight generation",default=True,action='store_false')

    parsedArgs = vars(parser.parse_args())
    cfsHFile = parsedArgs['cfsh']
    cfsFFile = parsedArgs['cfsf']
    camFile = parsedArgs['cam']
    clmFile = parsedArgs['clm']
    oisstFile = parsedArgs['oisst']
    camGridName = parsedArgs['camgrid']
    beVerbose = parsedArgs['quiet']
    doClobber = parsedArgs['clobber']
    camGridIsUnstructured = parsedArgs['unstructured']
    camSSTFile = parsedArgs['sst']
    topoFile = parsedArgs['topo']
    doUseNCL = parsedArgs['ncl']

    requiredArguments = [ cfsHFile,   \
                          cfsFFile,   \
                          camFile,    \
                          clmFile,    \
                          topoFile,   \
                          camSSTFile]

    #Check if any of the arguments are missing
    if(any( v is None for v in requiredArguments)):
      print "Error: not enough arguments were provided"
      parser.print_help()
      quit()

    generateRegriddingWeights(
                    cfsHFile=cfsHFile,      \
                    cfsFFile=cfsFFile,      \
                    camFile=camFile,        \
                    clmFile=clmFile,        \
                    oisstFile=oisstFile,    \
                    camGridName=camGridName,\
                    camSSTFile=camSSTFile,  \
                    topoFile=topoFile,      \
                    beVerbose=beVerbose,    \
                    doClobber=doClobber,    \
                    doUseNCL = doUseNCL,    \
                    camGridIsUnstructured=camGridIsUnstructured,  \
                    )




