#!/usr/bin/env python
from iliad.utilities import dateensemble
import generateInitialConditions as gic

def divideListForScattering(inlist,outlistsize):
  #Create a list that explicitly has the proper size
  outlist = [ [] for i in range(outlistsize) ]

  n = 0
  #Go through each item in the input list and append it to the output list
  #Cycle through the indices of the output list so that they input list is
  #dividided as evenly as possible
  for i in range(len(inlist)):
    outlist[n].append(inlist[i])
    n = n + 1
    if(n >= outlistsize):
      n = 0

  #Return the list
  return outlist

def parallelGenerateICs(  cfsDirectory, \
                          mappingDirectory, \
                          startDate,  \
                          endDate,  \
                          initialConditionDir=None, \
                          useMPI=True,  \
                          camGridIsUnstructured = False, \
                          doClobber = False,  \
                          beVerbose = False,  \
                          beReallyVerbose = False,  \
                          isCFSv2Forecast = False,  \
                          forecastHours = [0,6,12,18]):

  #TODO: Check that startdate and enddate are datetime instances
  #TODO: Check that enddate is after startdate
  #TODO: Check that the mapping directory exists
  #TODO: Check that the CFS directory exists

  #Initialize MPI if we are using it; otherwise set rank and mpisize to 0,1
  if(useMPI):
    from mpi4py import MPI
    #Initialize MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    mpisize = comm.Get_size()
  else:
    rank = 0
    mpisize = 1

  if(rank == 0):
    #Create date combinations
    dateEnsemble = dateensemble.ensemble(startDate,endDate,forecastHours=forecastHours).dates

    dateListScatter = divideListForScattering(dateEnsemble,mpisize)
  else:
    dateListScatter = []

  if(useMPI):
    #Scatter the date list 
    myDateList = comm.scatter(dateListScatter,root=0)
  else:
    myDateList = dateListScatter[0]

  #Loop over the dates provided
  for currentDate in myDateList:

    #Combine the times into a string with properly 0 paddings
    currentDateString = "{:04}{:02}{:02}{:02}".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)

    #Combine the times into a readable date string
    callLineDate = "{:04}:{:02}:{:02}:{:02}".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)
    fileCoreString = "{:04}-{:02}-{:02}-{:02}Z".format(currentDate.year,currentDate.month,currentDate.day,currentDate.hour)

    #Run the command
    if(beVerbose):
      print "(rank {}): Creating files for {}".format(rank,fileCoreString)
    icgen = gic.generateInitialConditions(  \
                mappingDirectory=mappingDirectory,\
                cfsDirectory = cfsDirectory, \
                gmtDateTime = callLineDate, \
                doClobber = doClobber, \
                beVerbose = beVerbose,  \
                camGridIsUnstructured = camGridIsUnstructured, \
                isCFSv2Forecast = isCFSv2Forecast)


if(__name__ == "__main__"):
  import argparse
  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--cfsdirectory','-d', \
                      help="Directory containing CFSR files",default=None)
  parser.add_argument('--mappingdirectory','-m', \
                      help="Directory containing the weight files",default="./")
  parser.add_argument('--gmtstartdate','-t', \
                      help="A GMT date/time for the start date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--gmtenddate','-e', \
                      help="A GMT date/time for the end date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--clobber','-c', \
                      help="Do not clobber any existing files",default=False,action='store_true')
  parser.add_argument('--forecast','-w', \
                      help="Flag that we are using a CFSv2 forecast",default=False,action='store_true')
  parser.add_argument('--serial','-s', \
                      help="Flag that we shouldn't use mpi4py",default=True,action='store_false')
  parser.add_argument('--unstructured','-u', \
                        help="CAM grid is unstructured",default=False,action='store_true')
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')

  parser.add_argument('--forecasthours','-f', \
                      help="The hours over which to run the hindcasts (should be a comma separated list)",default="0,6,12,18")

  parsedArgs = vars(parser.parse_args())
  cfsDirectory = parsedArgs['cfsdirectory']
  beVerbose = parsedArgs['quiet']
  doClobber = parsedArgs['clobber']
  gmtStartDate = parsedArgs['gmtstartdate']
  gmtEndDate = parsedArgs['gmtenddate']
  mappingDirectory = parsedArgs['mappingdirectory']
  isCFSv2Forecast = parsedArgs['forecast']
  camGridIsUnstructured = parsedArgs['unstructured']
  useMPI = parsedArgs['serial']

  try:
    forecastHours = [int(hour) for hour in parsedArgs['forecasthours'].split(",")]
  except:
    parser.print_help()
    print "Error: could not parse arguments for --forecasthours: {}".format(parsedArgs['forecasthours'])
    quit()

  requiredSet = [cfsDirectory,mappingDirectory,gmtStartDate,gmtEndDate]

  #Check if any of the arguments are missing
  if(any( v is None for v in requiredSet)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  #Run the generator class initializer
  parallelGenerateICs(  \
                  cfsDirectory = cfsDirectory,    \
                  mappingDirectory = mappingDirectory,    \
                  startDate = dateensemble.dateStringToDatetime(gmtStartDate),    \
                  endDate = dateensemble.dateStringToDatetime(gmtEndDate),    \
                  beVerbose = beVerbose,    \
                  doClobber = doClobber,    \
                  camGridIsUnstructured = camGridIsUnstructured, \
                  isCFSv2Forecast = isCFSv2Forecast,  \
                  useMPI = useMPI, \
                  forecastHours = forecastHours)


