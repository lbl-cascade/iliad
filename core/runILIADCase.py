#!/usr/bin/env python
"""
Runs an iliad hindcast over the specified time period.
"""

import os
import copy
import subprocess
import datetime as dt
import mako.template as mako
import iliad.utilities.dateensemble as de

import os, errno

def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc: # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else: raise

def scriptFromTemplate(scriptFile, \
                            configDict, \
                            machine, \
                            resolution, \
                            numInst, \
                            gmtStartDate = None, \
                            gmtEndDate = None, \
                            doSubmit = False):
  """Takes a script (either workflow/doCreateCase.bash or workflow/doCloneCase.bash) and
     fills in values specific to the current machine/resolution/instance combination"""
  #Read the script, but skip any lines starting with '#ILIAD'
  with open(scriptFile,'r') as fin:
    scriptLines = [ line for line in fin.readlines() if line[:6] != '#ILIAD' ]

  #Remove the shebang line and put it at the start of the new lines that we're inserting
  newLines = [scriptLines.pop(0),'\n']

  if gmtStartDate is not None:
    newLines.append('GMT_START={}\n'.format(gmtStartDate))
  if gmtEndDate is not None:
    newLines.append('GMT_END={}\n'.format(gmtEndDate))

  newLines.append('DO_SUBMIT={}\n'.format(int(doSubmit)))

  #Add the machine, resolution, and number of instances
  newLines.append('MACHINE="{}"\n'.format(machine))
  newLines.append('ILIAD_DOMAIN="{}"\n'.format(resolution))
  newLines.append('NUM_INST="{}"\n'.format(numInst))

  #Append the location and runtime lines to the script
  for key,val in configDict['locations'].iteritems():
    newLines.append("{}={}\n".format(key,val))
  newLines.append('\n')
  for key,val in configDict['runtime'].iteritems():
    newLines.append("{}={}\n".format(key,val))
  newLines.append('\n')

  #Insert the new lines into the script and return
  return newLines + scriptLines


def add_months(sourcedate,months):
  """Adds an arbitrary number of months to a datetime object"""
  month = sourcedate.month - 1 + months
  year = sourcedate.year + month / 12
  month = month % 12 + 1
  day = sourcedate.day
  return dt.datetime(year,month,day)

def parseConfigFile(fileContentList):
  """Parses a configuration file"""

  parseDict = {}
  for line in fileContentList:
    lsplit = line.split('=')
    try:
      parseDict[lsplit[0].strip()] = lsplit[1].rstrip().strip()
    except:
      pass
  return parseDict

def runILIADCase( gmtStartDate, \
                  gmtEndDate,   \
                  resolution = None, \
                  monthsPerBatch = 4, \
                  beVerbose = True, \
                  doClobber = False, \
                  runDirectory = None):
  """
  Runs an iliad hindcast over the specified time period.

    :param gmtStartDate: the start date for the simulations (in the form 2005:01:01:00)
    :param gmtEndDate: the end date for the simulations (in the form 2005:01:01:00)
    :param resolution: the iliad resolution code for the simulations
    :param monthsPerBatch: the number of months to run per PBS batch (if the requested length is less than monthsPerBatch, the entire set is run)
    :param beVerbose: flag whether to print status messages
    :param doClobber: flag whether to clobber existing files
    :param runDirectory: the directory in which to do the simulations
  """

  #Move to the run directory
  os.chdir(runDirectory)

  #*****************************************
  # Parse the machine-specific config files
  #*****************************************
  #First determine the machine
  try:
    machine = os.environ['NERSC_HOST']
  except:
    raise RuntimeError,"Could not determine machine name via the NERSC_HOST environment variable"

  #Set the directory of the machine configuration files
  configDirectory = "{iliad}/workflow/machines/{machine}".format( \
                      iliad = "/".join(os.path.dirname(__file__).split('/')[:-1]), \
                      machine = machine)

  configDict = {}

  #Attempt to load the location file
  locationFile = configDirectory + '/locations.config'
  try:
    with open(locationFile,'r') as fin:
      configDict['locations'] = parseConfigFile(fin.readlines())
  except:
    raise RuntimeError,"{} does not exist; ILIAD needs to be set up for this machine".format(locationFile)

  #Attempt to load the resolution file
  resolutionFile = configDirectory + '/{}.config'.format(resolution)
  try:
    with open(resolutionFile,'r') as fin:
      configDict['runtime'] = parseConfigFile(fin.readlines())
  except:
    raise RuntimeError,"{} does not exist; ILIAD needs to be set up for this resolution".format(resolutionFile)

  #Check that the parsing worked
  if configDict['locations'] == {}:
    raise RuntimeError,"Parsing of {} failed; ILIAD needs to be set up for this machine".format(locationFile)
  if configDict['runtime'] == {}:
    raise RuntimeError,"Parsing of {} failed; ILIAD needs to be set up for this resolution".format(resolutionFile)

  #*********************************************
  # Determine the start/end dates of each batch
  #*********************************************
  #Parse the start/end dates
  startDate = de.dateStringToDatetime(gmtStartDate)
  endDate = de.dateStringToDatetime(gmtEndDate)

  #Determine if we need to use CFSR or CFSv2
  _CFSv2StartDate = dt.datetime(2011,04,01)
  usingCFSV2 = False
  if startDate >= _CFSv2StartDate:
    usingCFSV2 = True
  else:
    #Check that the dates don't straddle the CFSv2 transition
    if endDate >= _CFSv2StartDate:
      raise RuntimeError, "Requested dates straddle the transition from CFSR to CFSv2.  Please request start/end dates that do not contain {}".format(str(_CFSv2StartDate))

  #Loop from the start date, and add monthsPerBatch increments to the date each time until we
  #meet/pass the end date
  incrementLoop = True
  runBatches = []
  dumStart = startDate
  while incrementLoop:
    dumEnd = add_months(dumStart,monthsPerBatch)
    if dumEnd >= endDate:
      dumEnd = endDate
      #Flag that we're done searching
      incrementLoop = False
  
    runBatches.append( \
        { 'start' : copy.copy(dumStart),  \
          'end'   : copy.copy(dumEnd),    \
          'ndates': len(de.ensemble(dumStart,dumEnd,forecastHours=[0]).dates) \
        })

    #Set the new startdate to be the end of the last one
    dumStart = dumEnd

  #**********************************************
  # Determine if we need to build case templates
  #**********************************************
  #Get the base of the template directory
  caseTemplateBase = configDict['locations']['ILIAD_CASE_TEMPLATE_DIRECTORY'][1:-1].replace("${MACHINE}",machine).replace("${ILIAD_DOMAIN}",resolution)
  #Get the number of processors and threads
  numProc = int(configDict['runtime']['NUM_PES_PER_INST'])
  numThreads = int(configDict['runtime']['NUM_THRDS'])
  for rb in runBatches:
    #Get the number of instances
    numInst = rb['ndates']
    #Set the template directory
    templateDir = caseTemplateBase + '/{res}-{ninst}inst-{nproc}proc{nthrd}thrds'.format( \
                                      res = resolution, \
                                      ninst = numInst, \
                                      nproc = numProc, \
                                      nthrd = numThreads)

    doBuild = False
    #First try to read a CaseStatus file in that directory
    try:
      with open(templateDir + '/CaseStatus','r') as fin:
        csDum = fin.readlines()

      #Check if the build was completed; raise an error to exit this try block
      if not 'build complete' in csDum[-1]:
        raise RuntimeError,'Build is not complete'
    except:
      if not os.path.isdir(templateDir):
        doBuild = True
      else:
        if not doClobber:
          raise RuntimeError,"doClobber was set to True, but {} exists and the build is not completed successfully".format(templateDir)
        else:
          doBuild = True

    #If we need to build, run the build script
    if doBuild:
      buildScript = '{iliad}/workflow/doCreateCase.bash'.format( \
                      iliad = "/".join(os.path.dirname(__file__).split('/')[:-1]), \
                      )
      buildScriptLines = scriptFromTemplate(buildScript,configDict,machine,resolution,numInst)

      #Write the build script
      outputBuildScript = "{}/doCreateCase.{}.bash".format(caseTemplateBase,os.getpid())
      with open(outputBuildScript,'w') as fout:
        [ fout.write(bl) for bl in buildScriptLines ]

      #Run the build script
      _cwd = os.getcwd()
      #Change to the template directory
      os.chdir(caseTemplateBase)
      if beVerbose:
        print "Creating CESM case in {}".format(templateDir)
      #Create the case
      output = subprocess.check_output( 'bash {}'.format(os.path.basename(outputBuildScript)).split(), \
                                        stderr=subprocess.STDOUT)
      if beVerbose:
        print "Building CESM case in {}".format(templateDir)
      #Build it
      os.chdir(templateDir)
      output = subprocess.check_output('./{}.build'.format(templateDir.split('/')[-1]).split(), \
                                        stderr=subprocess.STDOUT)
      #Remove the build script
      os.remove(outputBuildScript)
      #Change back to the initial directory
      os.chdir(_cwd)


  #Get the initial condition file directory
  icFileDirectory = configDict['locations']['ILIAD_IC_DIR'][1:-1].replace('${ILIAD_DOMAIN}',resolution)
  #Create it if necessary
  mkdir_p(icFileDirectory)


  #Create a function to generate the case directory name
  def genCaseDirectoryName(rb):
    """Generates the case directory name"""
    gmtStartDate = '{:04}_{:02}_{:02}_{:02}'.format(rb['start'].year,rb['start'].month,rb['start'].day,rb['start'].hour)
    gmtEndDate = '{:04}_{:02}_{:02}_{:02}'.format(rb['end'].year,rb['end'].month,rb['end'].day,rb['end'].hour)
    return "{}/iliad-{}-{}-{}".format(runDirectory,resolution,gmtStartDate,gmtEndDate)
  
  #******************************************
  # Loop over batchs and clone the templates
  #******************************************
  for rb in runBatches:
      cloneScript = '{iliad}/workflow/doCloneCaseDir.bash'.format( \
                      iliad = "/".join(os.path.dirname(__file__).split('/')[:-1]), \
                      )

      #Set the start end date lines
      gmtStartDate = '{:04}:{:02}:{:02}:{:02}'.format(rb['start'].year,rb['start'].month,rb['start'].day,rb['start'].hour)
      gmtEndDate = '{:04}:{:02}:{:02}:{:02}'.format(rb['end'].year,rb['end'].month,rb['end'].day,rb['end'].hour)
      cloneScriptLines = scriptFromTemplate(cloneScript,configDict,machine,resolution,numInst,gmtStartDate,gmtEndDate)

      #Write the clone script
      outputCloneScript = "{}/doCloneCaseDir.{}.bash".format(runDirectory,os.getpid())
      with open(outputCloneScript,'w') as fout:
        [ fout.write(bl) for bl in cloneScriptLines ]

      #Check if the case directory exists
      caseDirectory = genCaseDirectoryName(rb)
      if os.path.isdir(caseDirectory) and not doClobber:
        raise RuntimeError,"doClobber was set to False, but {} already exists.".format(caseDirectory)

      #Run the clone script
      _cwd = os.getcwd()
      #Change to the template directory
      os.chdir(runDirectory)
      if beVerbose:
        print "Cloning CESM case in {}".format(caseDirectory)

      #Create the case
      output = subprocess.check_output( 'bash {}'.format(os.path.basename(outputCloneScript)).split(), \
                                        stderr=subprocess.STDOUT)
      #Remove the clone script
      os.remove(outputCloneScript)
      #Go back to the original directory
      os.chdir(_cwd)



  for rb in runBatches:

    #************************************
    # Create the initial condition files
    #************************************
    #Load the PBS template into mako
    pbsICTemplateFile = "{iliad}/workflow/machines/{machine}/runParallelGIC.pbs.template".format( \
                        iliad = "/".join(os.path.dirname(__file__).split('/')[:-1]), \
                        machine = machine)
    pbsICTemplate = mako.Template(filename=pbsICTemplateFile)

    #Set the template variables
    #TODO: Variables here are hardcoded and should not be
    runningProcPerNode = 2
    actualProcPerNode = 24
    numInst = rb['ndates']
    #Set the total number of processors
    totalProc = numInst * actualProcPerNode/runningProcPerNode
    walltime = '00:29:00'
    queue = 'regular'
    if usingCFSV2:
      forecastFlag = '--forecast'
      cfsBaseDirectory = '/project/projectdirs/m1949/reanalyses/CFSv2'
      cfsVersion = 'CFSv2'
    else:
      forecastFlag = ''
      cfsBaseDirectory = '/project/projectdirs/m1949/reanalyses/CFSR'
      cfsVersion = 'CFSR'
    #Set the start/end date flags
    gmtStartDate = '{:04}:{:02}:{:02}:{:02}'.format(rb['start'].year,rb['start'].month,rb['start'].day,rb['start'].hour)
    gmtEndDate = '{:04}:{:02}:{:02}:{:02}'.format(rb['end'].year,rb['end'].month,rb['end'].day,rb['end'].hour)

    #Render the template
    pendingICTemplate = pbsICTemplate.render( \
                            queue = queue, \
                            totalproc = totalProc, \
                            walltime = walltime, \
                            cfsVersion = cfsVersion, \
                            resolution = resolution, \
                            cfsBaseDirectory = cfsBaseDirectory, \
                            gmtStartDate = gmtStartDate, \
                            gmtEndDate = gmtEndDate, \
                            forecastFlag = forecastFlag, \
                            numInst = numInst, \
                            procsPerNode = runningProcPerNode, \
                            )
    #Remove offset brackets used for environment variables (to avoid clashing with mako)
    pendingICTemplate = pendingICTemplate.replace(r'\{','{').replace(r'\}','}')
  
    #Go to the IC file directory and write the PBS script
    _cwd = os.getcwd()
    os.chdir(icFileDirectory)
    #Set the filename
    pbsFileName = '.'.join(os.path.basename(pbsICTemplateFile).split('.')[:-1])
    if(beVerbose):
      print "Writing IC generation PBS script to {}".format(icFileDirectory + '/' + pbsFileName)
    with open(pbsFileName,'w') as fout:
      fout.write(pendingICTemplate)

    #Submit the job and get its ID
    icJobID = subprocess.check_output('qsub {}'.format(pbsFileName).split())
    icJobID = icJobID.rstrip()

    #***************************************
    # Submit the cloned job with dependency
    #***************************************
    #Move into the case directory
    caseDirectory = genCaseDirectoryName(rb)
    os.chdir(caseDirectory)
    #Modify the qsub line so that the job depends on the IC job completing successfully
    #NOTE: the doCloneCaseDir.bash script changes BATCHSUBMIT back to qsub as part of the *.prestage script
    newBatchLine = '<entry id="BATCHSUBMIT"   value="qsub -W depend=afterok:{}"  />\n'.format(icJobID)
    with open('env_run.xml','r') as fin:
      envRunLines = fin.readlines()
    #Substitute the new batch submit line
    envRunLines = [ l if not "BATCHSUBMIT" in l else newBatchLine for l in envRunLines  ]
    #Write the new xml file
    with open('env_run.xml','w') as fout:
      fout.write(''.join(envRunLines))


    #Submit the new job
    submitLine = './{}.submit'.format(os.path.basename(caseDirectory))
    if(beVerbose):
      "Running " + submitLine
    subprocess.check_call(submitLine.split(),stderr=subprocess.STDOUT)

    #Go back to the original directory
    os.chdir(_cwd)

#*****************************
# Command line execution code
#*****************************
if __name__ == "__main__":
  import argparse
  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--months', \
                      help="The number of months to run in each PBS batch",default=4,type=int)
  parser.add_argument('--rundir', \
                      help="The directory in which to run",default=None)
  parser.add_argument('--resolution','-r', \
                      help="The resolution to run",default=None)
  parser.add_argument('--clobber','-c', \
                      help="Clobber existing files",default=False,action='store_true')
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')

  #The python file on which to operate
  parser.add_argument('gmtstartdate',nargs=1,help="A GMT date/time for the start date in the form YYYY:MM:DD:HH")
  parser.add_argument('gmtenddate',nargs=1,help="A GMT date/time for the end date in the form YYYY:MM:DD:HH")

  parsedArgs = vars(parser.parse_args())
  beVerbose = parsedArgs['quiet']
  doClobber = parsedArgs['clobber']
  gmtStartDate = parsedArgs['gmtstartdate'][0]
  gmtEndDate = parsedArgs['gmtenddate'][0]
  monthsPerBatch = parsedArgs['months']
  runDirectory = parsedArgs['rundir']
  resolution = parsedArgs['resolution']

  requiredSet = [gmtStartDate,gmtEndDate]

  #Check if any of the arguments are missing
  if(any( v is None for v in requiredSet)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  #Determine the run directory if necessary
  if runDirectory is None:
    runDirectory = os.getcwd()

  #Attempt to parse the resolution from the run directory
  if resolution is None:
    resolution = os.path.abspath(runDirectory).split('/')[-1]
    if not 'ne' in resolution:
      raise RuntimeError,"Resolution was not specified on the command line, and it couldn't be determined from the run directory"

  runILIADCase(gmtStartDate = gmtStartDate, \
               gmtEndDate = gmtEndDate,   \
               monthsPerBatch = monthsPerBatch, \
               beVerbose = beVerbose, \
               runDirectory = runDirectory, \
               resolution = resolution, \
               doClobber = doClobber)


