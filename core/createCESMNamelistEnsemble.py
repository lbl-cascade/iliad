#!/usr/bin/env python
from iliad.utilities import dateensemble as de
from mako.template import Template
import os

class updateNamelistEnsemble:

  oceanDomainFiles = { \
      "fv1.9x2.5" : "domain.camocn.1.9x2.5_gx1v6_090403.nc", \
      "fv0.9x1.25" : "domain.camocn.0.9x1.25_gx1v6_090403.nc", \
      }

  def __init__( self, \
                startDate = None,   \
                endDate = None,     \
                forecastHours = None,  \
                dateEnsemble = None,  \
                cesmInputDir = None, \
                oceanDomainDir = None, \
                oceanDomainFile = None, \
                templateDirectory = "./", \
                ):

    """Creates an ensemble of CESM namelist files for ILIAD hindcasts, given a
    set of templates and hindcast information.

      Input:
      ------
      
      startDate       :     The start date of the hindcast ensemble (a
                            datetime-like object)

      endDate         :     The end date of the ensemble (a datetime-like
                            object)

      dateEnsemble    :     A pre-existing dateensemble object; this can be
                            provided in lieu of startDate and endDate

      cesmInputDir    :     The directory that holds the default CESM input
                            data. The program attempts to construct the path
                            and filename of the ocean domain file based on
                            this.

      oceanDomainDir, :     Instead of cesmInputDir, oceanDomainDir and
      oceanDomainFile       oceanDomainFile can be provided explicitly.

      templateDirectory :   The directory containing the namelist template
                            files.


      Output:
      -------

      An updateNamelistEnsemble object.  The createNamelistTemplates() class
      method does the actual work of creating the namelist files.

    """

    #Generate the date ensemble
    if not isinstance(dateEnsemble,de.ensemble):
      dateEnsembleArgs = {}

      #Check that startDate and endDate were given
      if startDate is not None and endDate is not None:
        #Add them to the keyword arguments
        dateEnsembleArgs['startdate'] = startDate
        dateEnsembleArgs['enddate'] = endDate
      else:
        raise ValueError,"A start date and end date both need to be provided, or a dateensemble object needs to be provided."

      #If forecastHours was provided, add it to the keyword arguments
      if forecastHours is not None:
        dateEnsembleArgs['forecastHours'] = forecastHours

      #Create the dateensemble object
      try:
        dateEnsemble = de.ensemble(**dateEnsembleArgs)
      except BaseException as e:
        import pdb
        pdb.set_trace()
        raise BaseException,e


    #Set the dateEnsemble
    self.dateEnsemble = dateEnsemble
    #Set the number of dates
    self.numberOfDates = len(dateEnsemble.dates)

    #Set the namelist files
    templateFileNames = [ \
                                "atm_in_0001",   \
                                "atm_modelio.nml_0001",  \
                                "docn_in_0001",  \
                                "docn_ocn_in_0001", \
                                "docn.streams.txt.prescribed_0001", \
                                "glc_modelio.nml_0001",  \
                                "ice_in_0001",   \
                                "ice_modelio.nml_0001",  \
                                "lnd_in_0001",   \
                                "lnd_modelio.nml_0001",  \
                                "ocn_modelio.nml_0001",  \
                                "rof_in_0001",   \
                                "rof_modelio.nml_0001",  \
                                "wav_modelio.nml_0001",  \
                            ]


    #Check for template file; raise an error if they're not present
    for value in templateFileNames:
      candidateFileName = "{}/{}".format(templateDirectory,value)
      if not os.path.exists(candidateFileName):
        raise ValueError,"{} does not exist.".format(candidateFileName)

    #Save the template file names and directory
    self.templateFileNames = templateFileNames
    self.templateDirectory = templateDirectory

    #Set the CESM input directory
    self.cesmInputDir = cesmInputDir

    #Set the ocean domain file directory and name
    self.oceanDomainDir = oceanDomainDir
    self.oceanDomainFile = oceanDomainFile

  def createNamelistTemplates(self,\
                              initialConditionDirectory,  \
                              domainName, \
                              namelistDirectory = "./"):
    """Uses mako to create namelist template files for a concurrent CESM simulation"""

    for i,date in zip(range(self.numberOfDates),self.dateEnsemble.dates):
      ensembleNumber = "{:04}".format(i+1)

      niceDateString = "{:04}-{:02}-{:02}-{:02}Z".format(date.year,date.month,date.day,date.hour)

      #Set initial condition file names
      camifilename = "cami-{}_{}.nc".format(niceDateString,domainName)
      clmifilename = "clmi-{}_{}.nc".format(niceDateString,domainName)
      sstfilename = "sst-{}_{}.nc".format(niceDateString,domainName)

      if(self.oceanDomainDir is None):
        #Construct the base directory for the ocn domain file
        self.oceanDomainDir = "{}/atm/cam/ocnfrac".format(self.cesmInputDir)

      if(self.oceanDomainFile is None):
        #Attempt to set the domain file name
        try:
          self.oceanDomainFile = self.oceanDomainFiles[domainName]
        except:
          #Fall back to a default value and issue a warning
          raise Warning,"""
          
          domainInfo/fileNames is being set to a default (and possibly wrong)
          value in the `docn.streams.txt.prescribed' files.

          This can be circumvented by setting oceanDomainFile to be the proper
          file name (or by using the --oceandomainfile option).

          """
          self.oceanDomainFile = "domain.camocn.1.9x2.5_gx1v6_090403.nc"

      #Loop over the template files and use mako to input values for the current date
      for j in range(len(self.templateFileNames)):
        #****************************************
        # Use mako to generate namelist files
        #****************************************
        pendingTemplate = Template(filename="{}/{}".format(self.templateDirectory,self.templateFileNames[j]))

        #Use mako to render the namelist file
        pendingNamelist = pendingTemplate.render( \
                                    inidir = initialConditionDirectory, \
                                    camifilename = camifilename,        \
                                    clmifilename = clmifilename,        \
                                    sstfilename = sstfilename,          \
                                    ensemblenumber = ensembleNumber, \
                                    oceandomaindir = self.oceanDomainDir, \
                                    oceandomainfile = self.oceanDomainFile, \
                                    startyyyymmdd = "{:04}{:02}{:02}".format(date.year,date.month,date.day),    \
                                    )

        #*************************************
        # Write the template file to disk
        #*************************************
        #Set the file name
        templateFileName = "{}/{}_{}".format( namelistDirectory, \
                                              self.templateFileNames[j][:-5],  \
                                              ensembleNumber)
        #Write to disk
        with open(templateFileName,"w") as fout:
          fout.write(pendingNamelist)

#*******************************************************************************
#*******************************************************************************
#************************ Execute from the command line ************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":

  import datetime as dt
  import argparse

  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--icdirectory','-i', \
                      help="Directory containing CESM initial condition files (generated by parallelGICs.py)",default=None)
  parser.add_argument('--domain','-m', \
                      help="The ILIAD domain name (e.g., fv1.9x2.5)",default=None)
  parser.add_argument('--gmtstartdate','-t', \
                      help="A GMT date/time for the start date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--gmtenddate','-e', \
                      help="A GMT date/time for the end date in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--templatedir','-d', \
                      help="The directory containing template CESM namelist files",default=None)
  parser.add_argument('--outputdir','-o', \
                      help="The directory in which to write templated namelist files",default=None)
  parser.add_argument('--cesminputdir','-c', \
                      help="The directory containing CESM input data",default=None)
  parser.add_argument('--oceandomainfile', \
                      help="The ocean domain file",default=None)
  parser.add_argument('--oceandomaindir', \
                      help="The ocean domain file's directory",default=None)
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')
  parser.add_argument('--forecasthours','-f', \
                      help="The hours over which to run the hindcasts (should be a comma separated list)",default="0,6,12,18")

  parsedArgs = vars(parser.parse_args())
  initialConditionDirectory = parsedArgs['icdirectory']
  beVerbose = parsedArgs['quiet']
  gmtStartDate = parsedArgs['gmtstartdate']
  gmtEndDate = parsedArgs['gmtenddate']
  templateDirectory = parsedArgs['templatedir']
  namelistDirectory = parsedArgs['outputdir']
  domainName = parsedArgs['domain']
  cesmInputDir = parsedArgs['cesminputdir']
  oceanDomainFile = parsedArgs['oceandomainfile']
  oceanDomainDir = parsedArgs['oceandomaindir']
  try:
    forecastHours = [int(hour) for hour in parsedArgs['forecasthours'].split(",")]
  except:
    parser.print_help()
    print "Error: could not parse arguments for --forecasthours: {}".format(parsedArgs['forecasthours'])
    quit()

  requiredSet = [ initialConditionDirectory, gmtStartDate, gmtEndDate, templateDirectory, namelistDirectory, domainName , cesmInputDir]

  #Check if any of the arguments are missing
  if(any( v is None for v in requiredSet)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  #***************************
  # initialize the templater
  #***************************
  updateObject = updateNamelistEnsemble(  \
                        startDate = de.dateStringToDatetime(gmtStartDate),  \
                        endDate = de.dateStringToDatetime(gmtEndDate),      \
                        forecastHours = forecastHours, \
                        cesmInputDir = cesmInputDir, \
                        oceanDomainFile = oceanDomainFile, \
                        oceanDomainDir = oceanDomainDir, \
                        templateDirectory = templateDirectory)

  #***************************
  # do the templating
  #***************************
  updateObject.createNamelistTemplates(   \
                        initialConditionDirectory = initialConditionDirectory,  \
                        domainName = domainName,  \
                        namelistDirectory = namelistDirectory)
                        

