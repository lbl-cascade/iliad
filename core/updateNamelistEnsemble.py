#!/usr/bin/env python
from iliad.utilities import dateensemble as de
from mako.template import Template
import os

class updateNamelistEnsemble:

  def __init__( self, \
                startDate = None,   \
                endDate = None,     \
                forecastHours = None,  \
                dateEnsemble = None,  \
                templateDirectory = "./", \
                ):

    #Generate the date ensemble
    if not isinstance(dateEnsemble,de.ensemble):
      dateEnsembleArgs = {}

      #Check that startDate and endDate were given
      if startDate is not None and endDate is not None:
        #Add them to the keyword arguments
        dateEnsembleArgs['startdate'] = startDate
        dateEnsembleArgs['enddate'] = endDate
      else:
        raise ValueError,"A start date and end date both need to be provided, or a dateensemble object needs to be provided."

      #If forecastHours was provided, add it to the keyword arguments
      if forecastHours is not None:
        dateEnsembleArgs['forecastHours'] = forecastHours

      #Create the dateensemble object
      try:
        dateEnsemble = de.ensemble(**dateEnsembleArgs)
      except BaseException as e:
        raise BaseException,e


    #Set the dateEnsemble
    self.dateEnsemble = dateEnsemble
    #Set the number of dates
    self.numberOfDates = len(dateEnsemble.dates)

    #Set the template file names
    templateFileNames = { \
        "cam" : "user_nl_cam" ,\
        "clm" : "user_nl_clm" ,\
        "ice" : "user_nl_cice" ,\
        "drv" : "user_nl_drv" ,\
        "ocn" : "user_docn.streams.txt.prescribed"}
    
    #Check for template file; raise an error if they're not present
    for key,value in templateFileNames.iteritems():
      candidateFileName = "{}/{}".format(templateDirectory,value)
      if not os.path.exists(candidateFileName):
        raise ValueError,"{} does not exist.".format(candidateFileName)

    #Save the template file names and directory
    self.templateFileNames = templateFileNames
    self.templateDirectory = templateDirectory


  def createNamelistTemplates(self,\
                              initialConditionDirectory,  \
                              domainName, \
                              namelistDirectory = "./"):
    """Uses mako to create namelist template files for a concurrent CESM simulation"""

    for i,date in zip(range(self.numberOfDates),self.dateEnsemble.dates):
      namelistExtension = "{:04}".format(i+1)

      niceDateString = "{:04}-{:02}-{:02}-{:02}Z".format(date.year,date.month,date.day,date.hour)

      #Set initial condition file names
      camifilename = "cami-{}_{}.nc".format(niceDateString,domainName)
      clmifilename = "clmi-{}_{}.nc".format(niceDateString,domainName)
      sstfilename = "sst-{}_{}.nc".format(niceDateString,domainName)

      pendingTemplates = {}
      pendingNamelists = {}
      #*************************************
      # Use mako to generate namelist files
      #*************************************
      for key in self.templateFileNames:
        pendingTemplates[key] = Template(filename="{}/{}".format(self.templateDirectory,self.templateFileNames[key]))

      #*************************************
      # Render the template files into 
      # variables
      #*************************************
      key = "cam"
      pendingNamelists[key] = pendingTemplates[key].render( \
                                    inidir = initialConditionDirectory, \
                                    camifilename = camifilename)

      key = "clm"
      pendingNamelists[key] = pendingTemplates[key].render( \
                                    inidir = initialConditionDirectory, \
                                    clmifilename = clmifilename)

      key = "ice"
      pendingNamelists[key] = pendingTemplates[key].render( \
                                    inidir = initialConditionDirectory, \
                                    sstfilename = sstfilename)

      key = "drv"
      pendingNamelists[key] = pendingTemplates[key].render( \
          startyyyymmdd = "{:04}{:02}{:02}".format(date.year,date.month,date.day))

      key = "ocn"
      pendingNamelists[key] = pendingTemplates[key].render( \
                                    inidir = initialConditionDirectory, \
                                    sstfilename = sstfilename)

      #*************************************
      # Write the template files to disk
      #*************************************
      for key,value in self.templateFileNames.iteritems():
        templateFileName = "{}/{}_{}".format( namelistDirectory, \
                                              value,  \
                                              namelistExtension)
                                              
        #Write to disk
        with open(templateFileName,"w") as fout:
          fout.write(pendingNamelists[key])

#*******************************************************************************
#*******************************************************************************
#************************ Execute from the command line ************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":
  import datetime as dt

  #get the present working directory
  pwd = os.path.dirname(os.path.realpath(__file__))

  startDate = dt.datetime(2013,1,1,0)
  endDate = dt.datetime(2013,1,1,12)
  dateEnsemble = de.ensemble(startDate,endDate)

  updateObject = updateNamelistEnsemble(  \
                        startDate = startDate,  \
                        endDate = endDate,      \
                        dateEnsemble=dateEnsemble,  \
                        templateDirectory = "{}/{}".format(pwd,"namelistTemplates"))

  updateObject.createNamelistTemplates(   \
                        initialConditionDirectory = pwd,  \
                        domainName = "fv1.9x2.5")
                        

