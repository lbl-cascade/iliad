#!/usr/bin/env python
import os
import shutil
import argparse
import datetime as dt
from iliad.utilities.codeversion import codeVersionID
from iliad.interpolation.doRegridCFSv2toCAM import doRegridCFStoCAM
from iliad.interpolation.doRegridOISSTtoCAM import doRegridOISSTtoCAM
import Nio

class generateInitialConditions:

  def __init__( self, \
                oisstFile = None,  \
                oiiceFile = None,  \
                cfsDirectory = None,  \
                cfsHFile = None,  \
                cfsFFile = None,  \
                camGridName = None,  \
                beVerbose = False,  \
                doClobber = False,  \
                gmtDateTime = None,  \
                mappingDirectory = "./",  \
                isCFSv2Forecast = False,  \
                unStaggeredGrid = True):

    requiredSet1 = [cfsDirectory,mappingDirectory,gmtDateTime]
    requiredSet2 = [cfsHFile,cfsFFile,mappingDirectory]

    #Check if any of the arguments are missing
    #There are two sets of valid arguments that could work together; check if either set has
    # all the required arguments
    if(any( v is None for v in requiredSet1) and any( v is None for v in requiredSet2)):
      raise ValueError,"Error: not enough arguments were provided"

    #*******************************************************************************
    #*******************************************************************************
    #********************* Determine file paths ************************************
    #*******************************************************************************
    #*******************************************************************************
    #If we have all of set 2, those take precedence
    if(not any(v is None for v in requiredSet2)):
      usingSet2 = True
    else:
      usingSet2 = False


    if(usingSet2):
      if(gmtDateTime is None):
        #Attempt to reconstruct the date from the CFS files
        try:
          parseDummy = cfsHFile.split(".")
          dateCode = parseDummy[-2]
          timeList = [dateCode[0:4], dateCode[4:6], dateCode[6:8], dateCode[8:10]]
        except:
          #(Fall back on issuing a warning and using 0000-01-01:00)
          if(beVerbose):
            print "Warning: date code could not be determined from CFS h file name.  Defaulting to 0000-01-01 00Z"
          timeList = ["0000", "01", "01", "00"]

        #Construct a datetime item for the time on the file name
        dateOneFileName = dt.datetime(*tuple([int(mystr) for mystr in timeList]))

        #Add 6 hours to the time on the file name to get the actual time of the
        #forecast
        dateTime = dateOnFileName + dt.timedelta(hours=6)
      else:
        #Parse the date
        try:
          dateTime = dt.datetime(*tuple([int(mystr) for mystr in gmtDateTime.split(":")]))
        except:
          raise ValueError,"Could not parse --gmtdatetime.  Expecting date of form YYYY:MM:DD:HH, but got {}".format(gmtDateTime)

    else:
      #Parse the date
      try:
        dateTime = dt.datetime(*tuple([int(mystr) for mystr in gmtDateTime.split(":")]))
      except:
        raise ValueError,"Could not parse --gmtdatetime.  Expecting date of form YYYY:MM:DD:HH, but got {}".format(gmtDateTime)

      #Subtract 6 hours, since the date listed in the file is 6 hours before the actual time in the file
      dateOnFileName = dateTime - dt.timedelta(hours=6)

      #Combine the times into a string with properly 0 paddings
      dateString = "{:04}{:02}{:02}{:02}".format(dateOnFileName.year,dateOnFileName.month,dateOnFileName.day,dateOnFileName.hour)

      #Construct the PGB file name
      if(isCFSv2Forecast):
        cfsHFile = "{}/pgbh06/pgbh06.cdas1.{}.grib2".format(cfsDirectory,dateString)
        cfsFFile = "{}/flxf06/flxf06.cdas1.{}.grib2".format(cfsDirectory,dateString)
      else:
        cfsHFile = "{}/pgbh06/pgbh06.gdas.{}.grb2".format(cfsDirectory,dateString)
        cfsFFile = "{}/flxf06/flxf06.gdas.{}.grb2".format(cfsDirectory,dateString)


    #Combine the times into a readable date string
    niceDateString = "{:04}-{:02}-{:02}-{:02}Z".format(dateTime.year,dateTime.month,dateTime.day,dateTime.hour)

    #Set the CAM grid name
    if(camGridName is None):
      #Attempt to parse the grid name from the mapping directory
      try:
        [_,_,camGridName] = mappingDirectory.split("-")
        #Remove any trailing slashes
        while(camGridName[-1] == "/"):
          camGridName = camGridName[:-1]
      except:
        raise ValueError,"Could not determine cam grid name.  Please set manually with -g"

    if(beVerbose):
      print ""
      print "Using {} as the CAM grid name".format(camGridName)
      print "Verifying file presence"

    #Creat a list of the CFS files for simplifying loops
    cfsFiles = [cfsHFile,cfsFFile]

    #Print whether they exist
    if(beVerbose):
      for fname in cfsFiles:
        print "\t({}) {}".format(os.path.exists(fname),fname.split("/")[-1])

    #Check whether the CFS files exist; we can't proceed if not
    if(any( os.path.exists(fname) is False for fname in cfsFiles)):
      raise ValueError,"Error: both a CFS pgb file and a CFS flx file are required."

    #Construct the weight file names
    if(unStaggeredGrid):
      #If the grid is unstructured, then use the same weight file for the unstaggered and staggered grid variables, 
      #since they are all on the same grid for CAM-SE
      wgtFile = { \
                "CFSf_CLM" : "{}/wgts_CFSR0.3deg-to-CLM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSh_CAM" : "{}/wgts_CFSR0.5deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSh_CAMSLat" : "{}/wgts_CFSR0.5deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "CFSh_CAMSLon" : "{}/wgts_CFSR0.5deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CAM" : "{}/wgts_CFSR0.3deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSf_CAMSLat" : "{}/wgts_CFSR0.3deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CAMSLon" : "{}/wgts_CFSR0.3deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "OISST_CESMSST" : "{}/wgts_OISST1deg-to-CESMSST-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CESMSST" : "{}/wgts_CFSR0.3deg-to-CESMSST-{}-latxlon.nc".format(mappingDirectory,camGridName)}
    else:
      wgtFile = { \
                "CFSf_CLM" : "{}/wgts_CFSR0.3deg-to-CLM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSh_CAM" : "{}/wgts_CFSR0.5deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSh_CAMSLat" : "{}/wgts_CFSR0.5deg-to-CAM-{}-slatxlon.nc".format(mappingDirectory,camGridName), \
                "CFSh_CAMSLon" : "{}/wgts_CFSR0.5deg-to-CAM-{}-latxslon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CAM" : "{}/wgts_CFSR0.3deg-to-CAM-{}-latxlon.nc".format(mappingDirectory,camGridName),  \
                "CFSf_CAMSLat" : "{}/wgts_CFSR0.3deg-to-CAM-{}-slatxlon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CAMSLon" : "{}/wgts_CFSR0.3deg-to-CAM-{}-latxslon.nc".format(mappingDirectory,camGridName), \
                "OISST_CESMSST" : "{}/wgts_OISST1deg-to-CESMSST-{}-latxlon.nc".format(mappingDirectory,camGridName), \
                "CFSf_CESMSST" : "{}/wgts_CFSR0.3deg-to-CESMSST-{}-latxlon.nc".format(mappingDirectory,camGridName)}

    if(beVerbose):
      for grid,fname in wgtFile.iteritems():
        print "\t({}) {}".format(os.path.exists(fname),fname.split("/")[-1])

    supportFile = {}
    #Set the USGS file name
    supportFile["topo"] = "{}/topo-0000-01-01_{}.nc".format(mappingDirectory,camGridName)

    #Set the CAM initial condition file name
    supportFile["cami"] = "{}/cami-0000-01-01_{}.nc".format(mappingDirectory,camGridName)

    #Set the CLM initial condition file name
    supportFile["clmi"] = "{}/clmi-0000-01-01_{}.nc".format(mappingDirectory,camGridName)

    #Set the SST file name
    supportFile["sst"] = "{}/sst-0000-01-01_{}.nc".format(mappingDirectory,camGridName)

    #Set the OISST file name
    supportFile["oisst"] = oisstFile

    #Set the OISST file name
    supportFile["oiice"] = oiiceFile

    if(beVerbose):
      for grid,fname in supportFile.iteritems():
        if(fname is not None):
          #TODO:Check that all support files exist
          print "\t({}) {}".format(os.path.exists(fname),fname.split("/")[-1])


    #*******************************************************************************
    #*******************************************************************************
    #************************* Prestage files **************************************
    #*******************************************************************************
    #*******************************************************************************
    if(beVerbose):
      print "Prestaging files"

    noPrestage = ["topo","oisst","oiice"]
    sFileIsNew = {}
    for key,fname in supportFile.iteritems():
      if(not key in noPrestage):
        #Create a new file name with the date code in the string
        newFileName = "./{}-{}_{}.nc".format(key,niceDateString,camGridName)
        if(not os.path.exists(newFileName) or doClobber):
          if(beVerbose):
            print "\tcreating {}".format(newFileName)
          #Copy the support file locally
          shutil.copy(fname,newFileName)
          #Flag that the support file is new, so that we know
          #to overwrite the file later
          sFileIsNew[key] = True
        else:
          print "\t{} already exists.".format(newFileName)
          #Flag that the support file is not new, so that we can
          #avoid clobbering later
          sFileIsNew[key] = False

        #Change the location of the support file
        supportFile[key] = newFileName

    #*******************************************************************************
    #*******************************************************************************
    #************************ Run regridding code **********************************
    #*******************************************************************************
    #*******************************************************************************

    #Check whether we need to regrid into the cami file
    if((sFileIsNew["cami"] and sFileIsNew["sst"]) or doClobber):
      if(beVerbose):
        print "Regridding CFS data into:"
        print "\t{}".format(supportFile["cami"])
        print "\t{}".format(supportFile["sst"])
        doRegridCFStoCAM( \
                  cfsPgbFile = cfsHFile, \
                  cfsFlxFile = cfsFFile , \
                  wgtFileDict = wgtFile, \
                  camTopoFile =  supportFile["topo"],  \
                  camSSTFile = supportFile["sst"], \
                  outFileName =  supportFile["cami"],  \
                  unStaggeredGrid = unStaggeredGrid, \
                  beVerbose = beVerbose)

    #Check whether we need to regrid OISST data into the sst file            
    usedOISST = False
    if(supportFile["oisst"] is not None and wgtFile["OISST_CESMSST"] is not None):
      if(sFileIsNew["sst"] or doClobber):
        if(beVerbose):
          print "Regridding OISST data into:"
          print "\t{}".format(supportFile["sst"])
          doRegridOISSTtoCAM( \
                    oisst_sstFile = supportFile["oisst"], \
                    oisst_iceFile = supportFile["oiice"], \
                    weightFile = wgtFile["OISST_CESMSST"], \
                    outFile = supportFile["sst"], \
                    beVerbose = beVerbose, \
                    forecastTimeUnits = "days since {:04}-{:02}-{:02} {:02}:00:00".format(dateTime.year,dateTime.month,dateTime.day,dateTime.hour) \
                    )
        usedOISST = True

    if(sFileIsNew["clmi"] or doClobber):
      if(beVerbose):
        print "Regridding CFS data into:"
        print "\t{}".format(supportFile["clmi"])

      from iliad.interpolation import doRegridCFSv2toCLM
      doRegridCFSv2toCLM.regrid(weightFile = wgtFile["CFSf_CLM"], \
                                cfsrFile = cfsFFile, \
                                clmFile = supportFile["clmi"])

    #*******************************************************************************
    #*******************************************************************************
    #********************* Add metadata to the output files ************************
    #*******************************************************************************
    #*******************************************************************************


    #Get the revision ID
    revision = codeVersionID(file=__file__)

    fio = {}
    for ftype in ["cami","sst","clmi"]:
      if(sFileIsNew[ftype] or doClobber):
        fileName = supportFile[ftype]
        
        if(beVerbose):
          print "Adding metadata to {}".format(fileName)

        fio[ftype] = Nio.open_file(fileName,"r+")
        fio[ftype].ILIAD_author = "Travis A. O'Brien"
        fio[ftype].ILIAD_contact = "TAOBrien@lbl.gov"
        fio[ftype].ILIAD_script = "generateInitialConditions.py"
        fio[ftype].ILIAD_script_revision = str(revision.ID)
        fio[ftype].ILIAD_revision_branch = str(revision.branch)
        fio[ftype].ILIAD_canonical_repository = "ssh://hg@bitbucket.org/taobrienlbl/iliad"
      else:
        fio[ftype] = None

    if(fio["cami"] is not None):
      fio["cami"].ILIAD_original_files = ", ".join(cfsFiles + [supportFile["topo"]])
      fio["cami"].ILIAD_weight_files = ", ".join([   wgtFile["CFSh_CAM"], \
                                                    wgtFile["CFSh_CAMSLat"], \
                                                    wgtFile["CFSh_CAMSLon"], \
                                                    wgtFile["CFSf_CAM"], \
                                                    wgtFile["CFSf_CAMSLat"], \
                                                    wgtFile["CFSf_CAMSLon"] ])

    if(fio["sst"] is not None):
      if(usedOISST):
        fio["sst"].ILIAD_original_files = ", ".join([supportFile["oisst"], supportFile["oiice"]])
        fio["sst"].ILIAD_weight_files = ", ".join([ wgtFile["OISST_CESMSST"] ])
      else:
        fio["sst"].ILIAD_original_files = ", ".join([cfsFFile])
        fio["sst"].ILIAD_weight_files = ", ".join([wgtFile["CFSf_CAM"]])

    if(fio["clmi"] is not None):
      fio["clmi"].ILIAD_original_files = ", ".join([cfsFFile])
      fio["clmi"].ILIAD_weight_files = ", ".join([wgtFile["CFSf_CLM"]])

    #Close files explicitly
    for ftype in ["cami","sst","clmi"]:
      if(fio[ftype] is not None):
        fio[ftype].close()

    #Save the support file names
    self.icFiles = supportFile

if(__name__ == "__main__"):
  #*******************************************************************************
  #*******************************************************************************
  #********************** Parse command line arguments ***************************
  #*******************************************************************************
  #*******************************************************************************
  parser = argparse.ArgumentParser()

  parser.add_argument('--oisst','-o', \
                      help="An OISST file (optional)",default=None)
  parser.add_argument('--oiice','-i', \
                      help="An OISST icec file (optional)",default=None)
  parser.add_argument('--cfsdirectory','-d', \
                      help="Directory containing CFSR files",default=None)
  parser.add_argument('--cfsh','-p', \
                      help="An existing CFSv2 3D file (on h grid; optional)",default=None)
  parser.add_argument('--cfsf','-f', \
                      help="An existing CFSv2 2D file (on f grid; optional)",default=None)
  parser.add_argument('--mappingdirectory','-m', \
                      help="Directory containing the weight files",default="./")
  parser.add_argument('--gmtdatetime','-t', \
                      help="A GMT date/time for the file in the form YYYY:MM:DD:HH",default=None)
  parser.add_argument('--camgrid','-g', \
                      help="The name of the CAM grid",default=None)
  parser.add_argument('--clobber','-c', \
                      help="Do not clobber any existing files",default=False,action='store_true')
  parser.add_argument('--forecast','-w', \
                      help="Flag that we are using a CFSv2 forecast",default=False,action='store_true')
  parser.add_argument('--staggered','-s', \
                        help="CAM grid uses staggered winds US and VS",default=False,action='store_true')
  parser.add_argument('--unstructured','-u', \
                        help="(deprecated; use --staggered instead)",default=False,action='store_true')
  parser.add_argument('--quiet','-q', \
                      help="Don't print status messages",default=True,action='store_false')

  parsedArgs = vars(parser.parse_args())
  oisstFile = parsedArgs['oisst']
  oiiceFile = parsedArgs['oiice']
  cfsDirectory = parsedArgs['cfsdirectory']
  cfsHFile = parsedArgs['cfsh']
  cfsFFile = parsedArgs['cfsf']
  camGridName = parsedArgs['camgrid']
  beVerbose = parsedArgs['quiet']
  doClobber = parsedArgs['clobber']
  gmtDateTime = parsedArgs['gmtdatetime']
  mappingDirectory = parsedArgs['mappingdirectory']
  isCFSv2Forecast = parsedArgs['forecast']
  unStaggeredGrid = not parsedArgs['unstructured']
  unStaggeredGrid = not parsedArgs['staggered']

  requiredSet1 = [cfsDirectory,mappingDirectory,gmtDateTime]
  requiredSet2 = [cfsHFile,cfsFFile,mappingDirectory]

  #Check if any of the arguments are missing
  #There are two sets of valid arguments that could work together; check if either set has
  # all the required arguments
  if(any( v is None for v in requiredSet1) and any( v is None for v in requiredSet2)):
    print "Error: not enough arguments were provided"
    parser.print_help()
    quit()

  #Run the generator class initializer
  generateInitialConditions(  \
                  oisstFile = oisstFile,    \
                  oiiceFile = oiiceFile,    \
                  cfsDirectory = cfsDirectory,    \
                  cfsHFile = cfsHFile,    \
                  cfsFFile = cfsFFile,    \
                  camGridName = camGridName,    \
                  beVerbose = beVerbose,    \
                  doClobber = doClobber,    \
                  gmtDateTime = gmtDateTime,    \
                  mappingDirectory = mappingDirectory,    \
                  unStaggeredGrid = unStaggeredGrid, \
                  isCFSv2Forecast = isCFSv2Forecast)


