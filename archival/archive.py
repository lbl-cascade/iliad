"""
This module provides functionality for archiving and restoring data using the
globus transfer API.

"""

import os
import getpass
import warnings
from functools import wraps
from globusonline.transfer import api_client
from globusonline.transfer.api_client import ExternalError


###########################################################
#  Decorators used for Archive class                      #
###########################################################
def requires_goapi(func):
        """
        Decorator used to ensure that the self.goapi is initialized.

        The decorator assumes that the wrapped function has self as first
        argument and that self has the self.__create_globus_client function.
        This decorator is usually used in combination with the Archive class.
        The decorator is defined outside of the class as specification of
        decorators within classes is problematic in Python.
        """
        @wraps(func)
        def wrapped_func(self, *args, **kwargs):
            """
            Wrap function to ensure we have a goapi client available to do transfers.
            """
            if self.goapi is None:
                self.__create_globus_client__(username=self.username,
                                              auth_method=self.auth_method,
                                              certificate_file=self.certificate_file,
                                              key_file=self.key_file,
                                              goauth_token=self.goauth_token,
                                              **self.auth_kwargs)
            return func(self, *args, **kwargs)
        return wrapped_func


###########################################################
#  Transfer List                                          #
###########################################################
class TransferList(list):
    """
    Simple helper class used to define a list of data transfers.
    This is essentially a reduced version of the globusonline.transfer.api_client.Transfer
    class in that we here only specify a list of sources and destinations without requiring
    the submitter to know submission_id and endpoint information as those are often determined
    through other means.
    """

    def __init__(self):
        super(TransferList, self).__init__()

    def add_item(self,
                 source_path,
                 destination_path,
                 recursive=False,
                 verify_size=None):
        """
        Add an item to be transferred to the transfer list.

        :param source_path: String of the source path
        :param destination_path: String of the destination path
        :param recursive: Boolean indicating whether the transfer is recursive
        :param verify_size: ?
        """
        self.append({'source_path': source_path,
                     'destination_path': destination_path,
                     'recursive': recursive,
                     'verify_size': verify_size})


###########################################################
#  GlobusDataTransfer                                     #
###########################################################
class GlobusDataTransfer(object):
    """
    Base class used to help with the transfer and manipulation of
    data using the globus transfer API.
    
    Instance Variables:
    -------------------
    
    :ivar authenticate: Boolean indicating whether we should authenticate with globus and create the
                      self.goapi when we create the instance. The default value is True. When set to
                      False, then no globus transfer client will be established but rather
                      the self.goapi object will be created when required, e.g., when
                      calling the archive(..) or retrieve(..) function. Setting authenticate to False can
                      be useful when we only need to perform local operations, e.g., we want to
                      only locate files but we do not need to do anything else.

    :ivar username: The name of the user for which transfers should be performed. For convenience
               this is set by default to the users current username (which may not be the
               correct name for globus but for many users it is so this is pure convenience)

    :ivar auth_method: Select one of the authentication methods specified in Archive.auth_methods.
                  By default, interactive authentication via the prompt is used.

    :ivar certificate_file: The file with the certificate. This parameter is only used if auth_method='certificate'

    :ivar key_file: The file with the auth key. This parameter is only used if auth_method='certificate'

    :ivar goauth_token: The goauth access token retrieved from nexus. This parameter is only used if
                         auth_method='goauth'

    :ivar auth_kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                   globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.

    Class Variables:
    ----------------

    :var auth_methods: List of authentication methods support for connecting to globusonline. Valid values are:

                    * 'prompt' : Authenticate using globusonline credential via an interactive prompt
                    * 'goauth' : Authenticate using a given globusonline authentication token
                    * 'certificate' : Authenticate using a given certificate and key file

    :var common_endpoints: Dictionary of common data transfer endpoints. E.g.:

                    * 'HPSS' : NERSC HPSS archival storage data transfer enpoint, i.e, ('nersc#hpss')
                    * 'DTN' : NERSC Data Transfer Node endpoint for transfer to global storage targets ('nersc#dtn')
                    * 'NERSC' : Same as 'DTN'
                    * 'NERSC_ARCHIVE': Same as 'HPSS'
                    * 'EDISON' : NERSC endpoint for transfer to edison
                    * 'HOPPER' : NERSC enpoint for transfer to hopper
                    * 'CARVER' : NERSC enpoint for tranfer to carver

    """

    auth_methods = ["prompt", "goauth", "certificate"]
    """
    List of available  authentication methods
    """

    common_enpoints = {'HPSS': "nersc#hpss",
                       'DTN': "nersc#dtn",
                       'NERSC': "nersc#dtn",
                       'NERSC_ARCHIVE': "nersc_hpss",
                       'CARVER': 'nersc#carver',
                       'HOPPER': 'nersc#hopper',
                       'EDISON': 'nersc#edison'
                       }
    """
    Dictionary of common data transfer endpoints
    """

    def __init__(self,
                 authenticate=True,
                 username=getpass.getuser(),
                 auth_method="prompt",
                 certificate_file=None,
                 key_file=None,
                 goauth_token=None,
                 **kwargs):
        """
        Create a new GlobusDataTransfer object.

        :param authenticate: Boolean indicating whether we should authenticate with globus and create the
                      self.goapi when we create the instance. The default value is True. When set to
                      False, then no globus transfer client will be established but rather
                      the self.goapi object will be created when required, e.g., when
                      calling the archive(..) or retrieve(..) function. Setting authenticate to False can
                      be useful when we only need to perform local operations, e.g., we want to
                      only locate files but we do not need to do anything else.

        :param username: The name of the user for which transfers should be performed. For convenience
                   this is set by default to the users current username (which may not be the
                   correct name for globus but for many users it is so this is pure convenience)

        :param auth_method: Select one of the authentication methods specified in Archive.auth_methods.
                      By default, interactive authentication via the prompt is used.

        :param certificate_file: The file with the certificate. This parameter is only used if auth_method='certificate'

        :param key_file: The file with the auth key. This parameter is only used if auth_method='certificate'

        :param goauth_token: The goauth access token retrieved from nexus. This parameter is only used if
                             auth_method='goauth'

        :param kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                       globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.

        """
        # Connect to globus if needed
        self.goapi = None
        self.username = username
        self.auth_method = auth_method
        self.certificate_file = certificate_file
        self.key_file = key_file
        self.goauth_token = goauth_token
        self.auth_kwargs = kwargs

        if authenticate:
            self.__create_globus_client__(username=self.username,
                                          auth_method=self.auth_method,
                                          certificate_file=self.certificate_file,
                                          key_file=self.key_file,
                                          goauth_token=self.goauth_token,
                                          **kwargs)

    def __create_globus_client__(self,
                                 username=getpass.getuser(),
                                 auth_method="prompt",
                                 certificate_file=None,
                                 key_file=None,
                                 goauth_token=None,
                                 **kwargs):
        """
        Initialize the self.goapi globusonline api connection using the given authentication approach.

        :param username: The name of the user for which transfers should be performed. For convenience
                   this is set by default to the users current username (which may not be the
                   correct name for globus but for many users it is so this is pure convenience)

        :param auth_method: Select one of the authentication methods specified in Archive.auth_methods.
                      By default, interactive authentication via the prompt is used.

        :param certificate_file: The file with the certificate. This parameter is only used if auth_method='certificate'

        :param key_file: The file with the auth key. This parameter is only used if auth_method='certificate'

        :param goauth_token: The goauth access token retrieved from nexus. This parameter is only used if
                             auth_method='goauth'

        :param kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                       globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.

        """
        # Check if we have a username
        if username is None:
            raise ValueError("username required for authentication with globusonline.")

        # Authenticate via the prompt
        if auth_method == 'prompt':
            self.__prompt_authenticate__(username=username)
        # Authenticate using a goauth token
        elif auth_method == 'goauth':
            if goauth_token is None:
                raise ValueError("goauth_token required when using the goauth authentication method.")
            self.__goauth_authenticate__(username=username,
                                         goauth_token=goauth_token,
                                         **kwargs)
        # Authenticate using a certificate
        elif auth_method == 'certificate':
            if key_file is None or certificate_file is None:
                raise ValueError("key_file and certificate_file required when authenticating using a certificate.")
            self.__certificate_authenticate__(username=username,
                                              certificate_file=certificate_file,
                                              key_file=key_file)
        elif auth_method is None:
            raise ValueError("auth_method not set.")
        else:
            raise ValueError("Unknown auth_method defined.")

    def __prompt_authenticate__(self, username):
        """
        Initialize the the goapi by asking the user to login to globusonline to generate a goauth token.

        :param username: The name of the user for which transfers should be performed
        """
        self.goapi, _ = api_client.create_client_from_args(args=[username, "-p"])

    def __certificate_authenticate__(self, username, certificate_file, key_file, **kwargs):
        """
        Initialize the goapi using the given certificate.

        :param username: The name of the user for which transfers should be performed

        :param certificate_file: The file with the certificate. This parameter is only used if auth_method='certificate'

        :param key_file: The file with the auth key. This parameter is only used if auth_method='certificate'

        :param kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                       globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.
        """
        self.goapi = api_client.TransferAPIClient(username=username,
                                                     cert_file=certificate_file,
                                                     key_file=key_file,
                                                     **kwargs)

    def __goauth_authenticate__(self, username, goauth_token, **kwargs):
        """
        Initialize the goapi using the given goauth token.

        :param username: The name of the user for which transfers should be performed

        :param goauth_token: The goauth access token retrieved from nexus.

        :param kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                       globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.
        """
        self.goapi = api_client.TransferAPIClient(username=username,
                                                     goauth=goauth_token,
                                                     **kwargs)

    @requires_goapi
    def is_dir(self, endpoint="nersc#dtn", path=""):
        """
        Determine whether the given path at the given endpoint is a directory.

        :param endpoint: The name of the globus endpoint to use. Default is "nersc#dtn"

        :param path: The path on the system that we want to look at.

        :return: Boolean indicating whether the path is a directory
        """
        try:
            _, _, _ = self.goapi.endpoint_ls(endpoint, path)
            return True
        except ExternalError:
            return False
        return False

    @requires_goapi
    def is_file(self,
                endpoint,
                path=""):
        """
        Determine whether the given path at the given endpoint is a file.

        :param endpoint: The name of the globus endpoint to use. Default is "nersc#dtn"

        :param path: The path on the system that we want to look at.

        :return: Boolean indicating whether the path is a directory
        """
        try:
            _, _, _ = self.goapi.endpoint_ls(endpoint, path)
            return False
        except ExternalError as e:
            if e.message.endswith("is a file"):
                return True
        return False

    @requires_goapi
    def path_ls(self,
                endpoint="nersc#hpss",
                path="",
                **kwargs):
        """
        This is a helper function that provides convenient access to the self.goapi.endpoint_ls function

        :param endpoint: The name of the globus endpoint to use. Default is "nersc#hpss"

        :param path: The path on the system that we want to look at.

        :raises: RuntimeError in case that the ls on the endpoint with the given path fails

        :return: Dict of paths where the values are a dict describing the
                 'type', 'permissions', 'size' , 'user', 'group', 'last_modified' and
                 'link_target' characteristics.
        """
        code, reason, data = self.goapi.endpoint_ls(endpoint, path)
        if code not in [200, 201, 202]:
            raise RuntimeError("ls on system failed. " + str(code) + " | " + str(reason) + " | " + str(data))
        print data
        outdict = {}
        for fs_item in data['DATA']:
            outdict[fs_item['name']] = fs_item
        return outdict

    @requires_goapi
    def create_submission_id(self):
        """
        Helper function used to generate a new globus submission id for data transfer or deletion.

        :raises: RuntimeError in case the creation of the submission id fails

        :return: globus submission id
        """
        # Create the submission id
        status, message, data = self.goapi.transfer_submission_id()
        # Check the status
        if status not in [200, 201, 202]:
            raise RuntimeError("Retrieval of submission id failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))
        # Return the submission id
        return data['value']

    @requires_goapi
    def initiate_delete(self,
                        path_list,
                        endpoint='nersc#dtn',
                        recursive=False,
                        deadline=None,
                        ignore_missing=True,
                        label=None,
                        interpret_globs=False):
        """
        Initiate the deletion of a set of files or folders

        :param path_list: List of items ot be deleted

        :param endpoint: The globus endpoint where the data lives. Default value is nersc#dtn for the NERSC data
                         transfer node.

        :param recursive: Should deletion be done recursively for paths (True/False).

        :param deadline: Deadline for the deletion. Default value is None.

        :param ignore_missing: Boolean indicating whether missing objects should be ignored

        :param label: ??? Default value is None

        :param interpret_globs: Boolean. Default False. ???

        :return: String indicating the task id for the data delete and a python dict with the complete response
                 from globus regarding the delete. While the task id is also contained in the dict we return it
                 separately here, since the caller typically just needs the task id and not the full response and
                 in this way we can make it easier for the user to get to it without having to know the keys in the
                 full globus response dictionary.

        """
        # 1) Activate the endpoints
        status, message, data = self.goapi.endpoint_autoactivate(endpoint)
        if status not in [200, 201, 202]:
            raise RuntimeError("Activation of the endpoint failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))
        # 2) Get a submission id
        submission_id = self.create_submission_id()

        # 3) Create a new Delete object
        godelete = api_client.Delete(submission_id=submission_id,
                                     endpoint=endpoint,
                                     deadline=deadline,
                                     recursive=recursive,
                                     ignore_missing=ignore_missing,
                                     label=label,
                                     interpret_globs=interpret_globs)

        # 4) Add all objects to be transferred to the data transfer object
        for delete_path in path_list:
            godelete.add_item(delete_path)

        # 5) Submit the delete
        status, message, data = self.goapi.delete(godelete)
        if status not in [200, 201, 202]:
            raise RuntimeError('Submission of the object deletion failed: status=' +
                               unicode(status) +
                               " Message=" + unicode(message))

        # 6) Return the task id and globus json response
        return data['task_id'], data

    @requires_goapi
    def initiate_transfer(self,
                          transfers,
                          source_endpoint="nersc#dtn",
                          destination_endpoint="nersc#hpss",
                          deadline=None,
                          sync_level=None,
                          encrypt_data=True,
                          verify_checksum=True,
                          **kwargs):
        """
        Start a new data transfer using the globus API

        :param transfers: List of dicts describing the different items to be shipped as part of this transfer.
                          Use the helper class archive.TransferList to define this list. The basic structure of the
                          list is that each transfer is defined by a dict with the following keys:

                            * `source_path` : String with the path to the source
                            * `destination_path` : String with the path where the data should be copied to
                            * `recursive` : This key is optional. Boolean indicating whether the transfer is \
                                            recursive (default is False)
                            * `verify_size`: ???
        :type transfers: TransferList

        :param source_endpoint: String indicating the name of the source endpoint. Default value is 'nersc#dtn', i.e.,
                                the endpoint of the NERSC data transfer node.
        :param destination_endpoint: String indicating the name of the destination enpoint. Default value is
                                     'nersc#hpss', i.e., the endpoint of the NERSC HPSS archive system. It is
                                     expected that most transfers will be to archive so default values are set
                                     accordingly here.

        :param deadline: Transfer deadline
        :param sync_level: ???
        :param encrypt_data: Boolean indicating whether the data should be encrypted for the transfer (default True)
        :param verify_checksum: Boolean indicating whether a checksum verification should be performed (default True)
        :param kwargs: support additional top level data transfer options of globus online without having to
 |                     add them to the the archive API directly.

        :raises RuntimeError: if an endpoint cannot be activated, a submission id cannot be retrieved, or the
                              transfer cannot be started.

        :return: String indicating the task id for the data transfer and a python dict with the complete response
                 from globus regarding the transfer. While the task id is also contained in the dict we return it
                 separately here, since the caller typically just needs the task id and not the full response and
                 in this way we can make it easier for the user to get to it without having to know the keys in the
                 full globus response dictionary.

        """
        # 1) Activate the endpoints
        status, message, data = self.goapi.endpoint_autoactivate(source_endpoint)
        if status not in [200, 201, 202]:
            raise RuntimeError("Activation of source endpoint failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))
        status, message, data = self.goapi.endpoint_autoactivate(destination_endpoint)
        if status not in [200, 201, 202]:
            raise RuntimeError("Activation of destination endpoint failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))

        # 2) Get a submission id
        submission_id = self.create_submission_id()

        # 3) Create a transfer object to define the data transfer
        gotransfer = api_client.Transfer(submission_id=submission_id,
                                         source_endpoint=source_endpoint,
                                         destination_endpoint=destination_endpoint,
                                         deadline=deadline,
                                         sync_level=sync_level,
                                         encrypt_data=encrypt_data,
                                         verify_checksum=verify_checksum,
                                         **kwargs)

        # 4) Add all objects to be transferred to the data transfer object
        for trans in transfers:
            # Add the transfer. Alternatively we could also write:
            #  gotransfer.add_item(**trans). However, the below is a bit more explicit
            gotransfer.add_item(source_path=trans['source_path'],
                                destination_path=trans['destination_path'],
                                recursive=trans['recursive'],
                                verify_size=trans['verify_size'])

        # 5) Submit the transfer
        status, message, data = self.goapi.transfer(gotransfer)
        if status not in [200, 201, 202]:
            raise RuntimeError('Submission of the transfer failed: status=' +
                               unicode(status) +
                               " Message=" + unicode(message))

        # 6) Return the task id and globus json response
        return data['task_id'], data


###########################################################
#  Archive                                                #
###########################################################
class Archive(GlobusDataTransfer):
    """
    Class used to implement archiving and retrieval of data files.

    Instance Variables:
    -------------------

    :ivar default_notify_on_success: Define the default list of people to send success notifications to.
                                Default value is None, indicating that the default list defined by
                                this function should be used.

    :ivar default_notify_on_failure: Define the default list of people to send failure notifications to.
                                Default value is None, indicating that the default list defined by
                                this function should be used.

    :ivar auth_method: Authentication method to be used. One of "prompt",

    TODO Implement notification on success/failure

    """
    
    def __init__(self,
                 data_db=None,
                 authenticate=True,
                 username=getpass.getuser(),
                 auth_method="prompt",
                 certificate_file=None,
                 key_file=None,
                 goauth_token=None,
                 default_notify_on_success=None,
                 default_notify_on_failure=None,
                 **kwargs):
        """
        Create a new Archive object to archive or retrieve data.

        :param data_db: The DataDB instance with the database used for managing data files and data
                        transfers.
        :type data_db: DataDB

        :param authenticate: Boolean indicating whether we should authenticate with globus and create the
                      self.goapi when we create the instance. The default value is True. When set to
                      False, then no globus transfer client will be established but rather
                      the self.goapi object will be created when required, e.g., when
                      calling the archive(..) or retrieve(..) function. Setting authenticate to False can
                      be useful when we only need to perform local operations, e.g., we want to
                      only locate files but we do not need to do anything else.

        :param username: The name of the user for which transfers should be performed. For convenience
                   this is set by default to the users current username (which may not be the
                   correct name for globus but for many users it is so this is pure convenience)

        :param auth_method: Select one of the authentication methods specified in Archive.auth_methods.
                      By default, interactive authentication via the prompt is used.

        :param certificate_file: The file with the certificate. This parameter is only used if auth_method='certificate'

        :param key_file: The file with the auth key. This parameter is only used if auth_method='certificate'

        :param goauth_token: The goauth access token retrieved from nexus. This parameter is only used if
                             auth_method='goauth'

        :param default_notify_on_success: Define the default list of people to send success notifications to.
                                    Default value is None, indicating that the default list defined by
                                    this function should be used.

        :param default_notify_on_failure: Define the default list of people to send failure notifications to.
                                    Default value is None, indicating that the default list defined by
                                    this function should be used.

        :param kwargs: Additional keyword arguments for the globusonline authentication routines. See also the
                       globusonline.transfer.api_client.TransferAPIClient class (__init__) for further details.
        """
        # Call the super constructor
        super(Archive, self).__init__(authenticate=authenticate,
                                      username=username,
                                      auth_method=auth_method,
                                      certificate_file=certificate_file,
                                      key_file=key_file,
                                      goauth_token=goauth_token,
                                      **kwargs)
        # Save the additional parameters not covered by the base class
        self.data_db = data_db
        if default_notify_on_success is not None:
            self.default_notify_on_success = default_notify_on_success
        else:
            self.default_notify_on_success = []
        if default_notify_on_failure is not None:
            self.default_notify_on_failure = default_notify_on_failure
        else:
            self.default_notify_on_failure = []

    @requires_goapi
    def prearchive(self,
                   source_endpoint,
                   source_path,
                   archive_endpoint,
                   archive_base_path,
                   recursive=None,
                   **kwargs):
        """
        Prepare data for archiving.

        This function is called by the archive(...) function before the archiving
        of the data is performed. Here one can, e.g., prepare data for archiving.
        The function also generates the list of transfers to be performed for
        archiving.

        To define a custom prearchive(..) function simply defined a derived class
        and overwrite the function.

        :param source_path: The full path the directory where the data is stored.

        :param source_endpoint: String indicating the name of the source endpoint.

        :param destination_path: The full path where the data should be stored.

        :param archive_endpoint: String indicating the name of the destination endpoint.

        :param recursive: Should archiving be done recursively for the path (True/False). The default value is None
                          indicating that the function should determine whether to use the recursive flag based on
                          the type of data to be transferred (i.e., if the path is dir then recursive will be set to
                          True and if it is a file then it will be set to False).

        :returns: This function returns a list of TransferList objects describing a set of
                  data transfers to be performed and the items to be transferred as part of
                  each transfer. Return in case the prearchive fails.

        """
        trans_list = TransferList()
        # If the user did not indicate whether the transfer should be recursive or not, then determine
        # the recursive flag automatically based on the type (file or dir) we need to transfer
        if recursive is None:
            recursive_flag = self.is_dir(source_endpoint, source_path)
        else:
            recursive_flag = recursive
        trans_list.add_item(source_path=source_path,
                            destination_path=archive_base_path,
                            recursive=recursive_flag,
                            verify_size=None)
        return [trans_list]

    @requires_goapi
    def archive(self,
                source_endpoint="nersc#dtn",
                source_path="",
                archive_endpoint="nersc#hpss",
                archive_base_path="",
                recursive=None,
                notify_on_success=None,
                notify_on_failure=None, **kwargs):
        """
        Archives a simulation directory to HPSS.


        :param recursive:
        :param source_path: The full path to the directory containing the iliad hindcasts.
                       This function expects the directory to have the standard ILIAD structure with
                           caseTemplates
                            initialConditions
                           namelistTemplates
                           iliad-{RESOLUTION}-{YYYY_MM_DD_HH1}-{YYYY_MM_DD_HH2}*
                           hindcastOutputFiles

        :param source_endpoint: String indicating the name of the source_path endpoint. Default value is 'nersc#dtn',
                                i.e., the endpoint of the NERSC data transfer node.

        :param archive_base_path: The full path of the base directory in which to store the archives. The
                         actual directory where the files are stored will be "{archive_base_path}/{res}/{year}",
                         where "res" is the resolution code for the simulation, and "year" is the simulation
                         year (both should be inferrable from the iliad-* directory names
                         in `hindcastDirectory'

        :param archive_endpoint: String indicating the name of the destination endpoint. Default value is
                                     'nersc#dtn', i.e., the endpoint of the NERSC data transfer node.

        :param recursive: Should archiving be done recursively for the path (True/False). The default value is None
                          indicating that the function should determine whether to use the recursive flag based on
                          the type of data to be transferred (i.e., if the path is dir then recursive will be set to
                          True and if it is a file then it will be set to False).

        :param notify_on_success: A list of addresses to which to send success notifications. Default is None
                                  indicating that the default notification lists should be used.

        :param notify_on_failure: A list of addresses to which to send failure notifications. Default is None
                                  indicating that the default notification lists should be used.

        :raises: ???

        :returns: This function returns a tuple with the following three elements:

            * `transfer_task_ids` : List with all globus task ids for the transfers. None is used in case
                                    a particular transfer was not issued. May be empty in case that no archive
                                    operations were performed in response to the call.
            * `transfer_task_data` : List of the full response from the globus transfer API when the
                                    transfer was scheduled. In case that the scheduling of the transfer
                                    failed a dict with all the data pertaining to the transfer is
                                    returned instead, which also includes the error that occurred.
                                    May be empty in case that no archive operations were performed in response
                                    to the call.
            * `num_scheduling_errors: Number of errors scheduling data transfers. Transfers that have not
                                    been scheduled will have transfer_task_id of None and transfer_task_data[i]['error']
                                    will describe the error and the remaining keys transfer_task_data[i] will
                                    be the inputs used for scheduling the transfer.

        """
        # Define the default settings for the notification
        if notify_on_success is None:
            notify_on_success = self.default_notify_on_success
        if notify_on_failure is None:
            notify_on_failure = self.default_notify_on_failure

        # Make sure that the source and archive iendpoints are active
        status, message, data = self.goapi.endpoint_autoactivate(source_endpoint)
        if status not in [200, 201, 202]:
            raise RuntimeError("Activation of source endpoint failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))
        status, message, data = self.goapi.endpoint_autoactivate(archive_endpoint)
        if status not in [200, 201, 202]:
            raise RuntimeError("Activation of archive endpoint failed. Status=" +
                               unicode(status) +
                               " Message=" + unicode(message))

        # Perform any steps required before archiving the data
        transfer_lists = self.prearchive(source_endpoint=source_endpoint,
                                         source_path=source_path,
                                         archive_endpoint=archive_endpoint,
                                         archive_base_path=archive_base_path,
                                         recursive=recursive)
        if transfer_lists is None:
            return [], [], 0  # Nothing to do, i.e, no transfers, no transfer data, and no errors

        # Make sure that we have a list of TransferList objects and not just a single TransferList object
        # This avoids special cases when generating the data transfers.
        if isinstance(transfer_lists, TransferList):
            transfer_lists = [transfer_lists]

        # Initiate the transfers and register them with the database
        transfer_task_ids = [None] * len(transfer_lists)
        transfer_task_data = [None] * len(transfer_lists)
        num_scheduling_errors = 0
        for index, transfer_job in enumerate(transfer_lists):
            # initiate the transfer
            try:
                transfer_task_ids[index], transfer_task_data[index] = \
                    self.initiate_transfer(transfers=transfer_job,
                                           source_endpoint=source_endpoint,
                                           destination_endpoint=archive_endpoint,
                                           deadline=None,
                                           sync_level=None,
                                           encrypt_data=True,
                                           verify_checksum=True,
                                           **kwargs)
            # Record if an error occurred while trying to schedule the transfer and move on
            except RuntimeError as e:
                transfer_task_ids[index] = None
                transfer_task_data[index] = {'error': e,
                                             'transfers': transfer_job,
                                             'source_endpoint': source_endpoint,
                                             'archive_endpoint': archive_endpoint,
                                             'deadline': None,
                                             'sync_level': None,
                                             'encrypt_data': True,
                                             'verify_checksum': True}
                transfer_task_data[index].update(kwargs)
                num_scheduling_errors += 1
            # register the transfer with the database if possible
            if self.data_db is not None:
                self.data_db.add_transfer(transfers=transfer_job,
                                          globus_task_id=transfer_task_ids[index],
                                          source_endpoint=source_endpoint,
                                          destination_endpoint=archive_endpoint)

        # TODO use the Archive.send_email_notification(...) function to send an email status message.
        #      In the simplest case we can just check if num_scheduling_errors==0 to check for success/failure

        # Return all the generated transfer tasks
        return transfer_task_ids, transfer_task_data, num_scheduling_errors

    def locate(self,
               year=None,
               resolution=None,
               months=None,
               **kwargs):
        """
        Locate relevant files on HPSS

        :param year: The year of model output to extract

        :param resolution: The resolution to extract

        :param months: A list of integers (ranging 1 to 12) corresponding to months of model output to extract

        :returns: List of tuples (location, components) indicating for each file the location and relevant components
                  of the file. The components portion is used for archive files, e.g., tar files, as in some cases
                  only parts of the files in the archive may be relevant to the search.

        """
        # TODO Implement the locate function. See also self.data_db and DataDB.locat(...)
        pass

    @requires_goapi
    def retrieve(self,
                 filelist=None,
                 archive_endpoint="nersc#hpss",
                 destination_endpoint="nersc#dtn",
                 destination_path=os.getcwd(),
                 notify_on_success=None,
                 notify_on_failure=None):
        """
        Retrieve data from the archive.

        :param filelist: List of tuples (location, components), indicating for each file the location and relevant
                         components. This can be a user-defined list or the output of the locate function.

        :param archive_endpoint: The globus endpoint for the archive store. Default is nersc#hpss.

        :param destination_endpoint: The destination endpoint. Default is nersc#dtn.

        :param destination_path: The folder  where the files should be placed after retrieval.

        :param notify_on_success: A list of addresses to which to send success notifications. Default is None
                                     indicating that the default notification lists should be used.

        :param notify_on_failure: A list of addresses to which to send failure notifications. Default is None
                                  indicating that the default notification lists should be used.

        """
        if notify_on_success is None:
            notify_on_success = self.default_notify_on_success
        if notify_on_failure is None:
            notify_on_failure = self.default_notify_on_failure
        pass
        # TODO implement the data retrieval. We here need to deal with untaring of files and keeping only
        #      the required data portions. We also need to deal with generating the folder structure at
        #      the target location. This may not be easily possible as this requires a coupling of data
        #      transfer with follow-up tasks. At the very least we need a task that stays alive during
        #      the entire data retrieval process to monitor progress and start processing once data is ready.

    @requires_goapi
    def update_transfer_status(self):
        """
        Update the status of all transfers. This means we need to:

        1) Query the database defined in self.data_db for all transfers of the user that are not complete
           by calling self.data_db.get_incomplete_transfers(...)
        2) Check for all the relevant transfers what their current status is
        3) Call self.data_db.update_transfer_status(...) to update the status of the transfer(s) in the database

        :return:
        """
        # TODO implement the update_transfer_status function. See the doc-string for details
        return NotImplementedError('This function has not been implemented yet.')

    @staticmethod
    def send_email_notification(subject,
                                body,
                                sender='convert@openmsi.nersc.gov',
                                email_type='success',
                                email_success_recipients=None,
                                email_error_recipients=None):
        """
        Send email notification to users.

        :param subject: Subject line of the email
        :param body: Body text of the email.
        :param sender: The originating email address
        :param email_type: One of 'success, 'error', 'warning'. Error messages are sent
                     to ConvertSettings.email_error_recipients, success messages to
                     ConvertSettings.email_success_recipients and warning messages are sent to both lists.
        :param email_success_recipients: List of user that should receive an email if the status is success
                     or warning.
        :param email_error_recipients: List of users that should receive an email if the status is error
                     or warning.

        :returns: Boolean indicating the success (True) or (Failure) of sending the email.

        """
        if email_error_recipients is None:
            email_error_recipients = []
        if email_success_recipients is None:
            email_success_recipients = []
        # Define the list of recipients
        if email_type == 'success':
            recipients = email_success_recipients
        elif email_type == 'error':
            recipients = email_error_recipients
        else:
            recipients = email_error_recipients + email_success_recipients
        # Remove duplicates from the list of recipients
        recipients = list(set(recipients))
        # Check if we have any recipients
        if len(recipients) == 0:
            return

        from smtplib import SMTP
        from email.MIMEText import MIMEText
        from email.Header import Header
        from email.Utils import parseaddr, formataddr

        header_charset = 'ISO-8859-1'
        body_charset = 'US-ASCII'
        for bc in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
            try:
                body_charset = bc
                body.encode(body_charset)
            except UnicodeError:
                pass
            else:
                break

        # Define the sender and recipients
        sender_name, sender_addr = parseaddr(sender)
        sender_name = str(Header(unicode(sender_name), header_charset))
        sender_addr = sender_addr.encode('ascii')

        tostr = ""
        for ri in range(len(recipients)):
            rec = recipients[ri]
            recname, recaddr = parseaddr(rec)
            recname = str(Header(unicode(recname), header_charset))
            recaddr = recaddr.encode('ascii')
            tostr += formataddr((recname, recaddr))
            if ri < (len(recipients)-1):
                tostr += ", "

        # Construct the message
        msg = MIMEText(body.encode(body_charset), 'plain', body_charset)
        msg['From'] = formataddr((sender_name, sender_addr))
        msg['To'] = tostr
        msg['Subject'] = Header(unicode(subject), header_charset)

        # Send the message using sendmail
        try:
            smtp = SMTP("localhost")
            smtp.sendmail(sender, recipients, msg.as_string())
            smtp.quit()
        except:
            warnings.warn('Email could not be sent' + str(sys.exc_info()))
            return False
        return True


###########################################################
#  DataDB                                                 #
###########################################################
class DataDB(object):
    """
    Class for interacting with the database where we manage our data.
    """

    def add_transfer(self,
                     transfers,
                     globus_task_id,
                     source_endpoint,
                     destination_endpoint):
        """
        Add a new data transfer to the database

        :param transfers: TransferList object with a list of the different data transfers included in this transfer
        :param globus_task_id: The task id associated with the transfer. Set task-id to None to indicate that the
                               scheduling of the transfer has either failed or simply has not been done yet.
        :param source_endpoint: The source endpoint used
        :param destination_endpoint: The destination endpoint used

        :return:
        """
        # TODO Implement this function to record a data transfer. This function is called by Archive.archive(...)
        pass

    def update_transfer_status(self,
                               globus_task_id,
                               status):
        """
        Updated the status of a file transfer.

        :param globus_task_id: The globus task id under which the transfer is stored in the database
        :param status: The new status of the transfer
        """
        # TODO Update the status of the transfer in the database. See also Archive.update_transfer_status(...)
        pass

    def get_incomplete_transfers(self,
                                 username=getpass.getuser()):
        """
        Get all incomplete, i.e, scheduled, running, and error transfers.

        :param username: The user for which transfers should be retrieved. Set to None
                         if all transfers should be considered. Default value is the
                         current username given by getpass.getuser()

        :return: The function returns two lists:

                    * List of the globus_task_ids of all relevant transfers
                    * List of the current status of the transfers
        """
        # TODO Implement the retrieval of all incomplete data tasks. See also Archive.update_transfer_status(...)
        pass

    def locate(self,
               year,
               resolution,
               months=range(1, 13)):
        """
        Locate relevant files based on the specified query value.

        :param year:
        :param resolution:
        :param months:
        :return:
        """
        # TODO Implement the locate function. See also Archive.locate(...) and Archive.retrieve(...)
        pass


