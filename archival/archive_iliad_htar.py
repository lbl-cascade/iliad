#!/usr/bin/env python
"""
Implement data archiving for ILIAD using htar.
"""
import os
import errno
import tempfile
import subprocess
import iliadCaseCollection as ifc

class ArchiveILIAD:
    """
    Implement data archiving for ILIAD
    """

    def __init__(self,  \
                 be_verbose = True, \
                 **kwargs):
        """
        Initialize ArchiveILIAD object


        :param be_verbose: flag whether to be verbose
        """

        # Save the verbosity flag
        self.be_verbose = be_verbose

    def pprint(self,msg):
        """Prints if self.be_verbose is True"""
        if self.be_verbose:
            print(msg)

    def archive(self,
                source_path, \
                archive_base_path = '/home/t/taobrien/cascade/iliad', \
                ):
        """
        
        Archive the ILIAD simulations.

        :param source_path: The full path the directory where the data is stored.

        :param archive_base_path: The full path where the data should be stored.

        :returns: 

        """
        # Construct a dict of ILIAD files to archive
        self.pprint("Initializing archival of {}".format(source_path))
        caseCollection,parsedObject = ifc.iliadCaseCollection(source_path)

        #Get the resolution
        resolution = parsedObject.resolutionCode

        #Set the archive path
        archive_path = archive_base_path + '/' + resolution

        # Check file conformance
        for tar_path in sorted(caseCollection):
            self.pprint("Checking htar conformance for files in {}".format(tar_path))
            # Check that the file lengths conform to htar
            failed_prefix_inds = [ i for i,ffile in enumerate(caseCollection[tar_path]) \
                                    if len(os.path.dirname(ffile)[1:]) > 154 ]
            failed_name_inds = [ i for i,ffile in enumerate(caseCollection[tar_path]) \
                                   if len(os.path.basename(ffile)) > 99 ]
            if len(failed_prefix_inds) > 0 or len(failed_name_inds) > 0:
                # Create a list of unique non-compliant file name indices
                ifail = list(set(failed_prefix_inds + failed_name_inds))
                # Print non-compliant file names
                print "Non-compliant file names:"
                for i in ifail:
                    print '\t',caseCollection[tar_path][i]
                raise RuntimeError,"Files in {} contain non-compliant names".format(tar_path)

        failed_htar_calls = []
        # Add each tarfile to the list, run htar
        for tar_path in sorted(caseCollection):
            #Set the destination tar file name
            destination_path=archive_path + '/' + tar_path 

            # Create a temporary file that lists all the files to archive
            file_contents = "\n".join(caseCollection[tar_path]) + '\n'
            with tempfile.NamedTemporaryFile(delete=False) as fout:
                fout.write(file_contents)
                tmp_file_name = fout.name

            # Check if the archive exists and is already complete
            self.pprint('\tchecking status of {}...'.format(destination_path))
            archive_already_exists, reason = self.check_contents(destination_path, caseCollection[tar_path])

            if not archive_already_exists:
                # If it doesn't or is incomplete, run htar
                self.pprint('\t\tproceeding with htar because {}.'.format(reason))

                # If the tar file contains the original iliad files or hi-res regridded files,
                # at an h0, h1, or h3 level, remove the local files on completion to save space.
                if (tar_path[:5] == 'iliad' or tar_path[:5] == 'hires') and  \
                        ( 'h0' in tar_path or 'h1' in tar_path or 'h3' in tar_path):
                    extra_opts = '-Hrmlocal'
                else:
                    extra_opts = ''

                # set the htar line
                htar_line = "htar {ext} -V -L {tmp} -cf {tar_path}".format( \
                                tmp = tmp_file_name,  \
                                tar_path = destination_path, \
                                ext = extra_opts)
                self.pprint('\t'+htar_line)
                # run htar
                try:
                    subprocess.check_call(htar_line.split())
                except:
                    failed_htar_calls.append(htar_line)
                os.unlink(tmp_file_name)
            else:
                self.pprint('\t\t{} already exists and contents are verified; skipping.'.format(tar_path))

        return failed_htar_calls


    def check_contents(self, tar_file, file_list):
        """Checks if tar_file has successfully been created on HPSS.

            :param tar_file: the full path to the tar file on HPSS

            :param file_list: an ordered list of files that should be in tar_file

            :rtype: tuple (logical,string).  For the logical returns True if the file exists and contents are
                    consistent, and False otherwise.  For the string, returns the reason for returning False
                    or '' for True.

            This does two checks: (1) checks that the list of files in both
            sets are identical.  If file_list is a subset of the files in
            tar_file, then an exception is raised, since this indicates that
            files on disk may have been lost since tar_file was created.  (2)
            checks that the file sizes are all identical.

            For speed purposes, this does not do a byte-by-byte comparison.
        
        """

        # Attempt to get a listing of files in tar_file
        htar_line = 'htar -tf {}'.format(tar_file)
        self.pprint('\t\tchecking if {} exists'.format(tar_file))
        try:
            # run htar and output the stderr stuff to /dev/null
            with open(os.devnull,'w') as DEVNULL:
                htar_output = subprocess.check_output(htar_line.split(),stderr=DEVNULL)
        except:
            # If this fails, it indicates that the file doesn't exist
            return False,'no valid corresponding tar file exists on HPSS'

        self.pprint('\t\t{} exists; checking validity...'.format(tar_file))
        #**************************
        # Parse the output of htar
        #**************************
        # get the lines that have file names
        content_lines = htar_output.split('\n')[:-3]

        # check the number of files
        num_lines = len(content_lines)
        if num_lines != len(file_list):
            if num_lines > len(file_list):
                print "\n##### Tar file:\n"
                for i in range(num_lines):
                    print content_lines[i].split()[-1]
                print "\n##### On disk:\n"
                for i in range(len(file_list)):
                    print file_list[i]
                #This indicates that the tar file was not created successfully
                #raise RuntimeError,'{} has more files than specified ({} in the tar file, {} on disk); perhaps files on disk have been deleted?'.format(tar_file,num_lines,len(file_list))
                return True,'{} has more files than specified ({} in the tar file, {} on disk); perhaps files on disk have been deleted?'.format(tar_file,num_lines,len(file_list))
            else:
                #This indicates that the tar file was not created successfully
                return False,'HPSS file has {} files, local list has {}'.format(len(content_lines),len(file_list))

        # get the list of file names and file sizes
        name_size_list = [ (line.split()[-1], int(line.split()[-4])) for line in content_lines ]

        #*******************************
        # Check that the contents match
        #*******************************

        # check file name matching
        file_names_match = [ name_size_list[i][0] == file_list[i] for i in range(num_lines) ]
        if not all(file_names_match):
            for i in range(num_lines):
                if name_size_list[i][0] != file_list[i]:
                    print i,name_size_list[i][0],file_list[i]

            # assume that inequality in any of the names indicates a bad tar file
            raise RuntimeError,'file names mismatch between the HPSS file and the local list'
            #return False,'file names mismatch between the HPSS file and the local list'

        # check file size matching
        file_sizes_match = [ name_size_list[i][1] == os.stat(file_list[i]).st_size for i in range(num_lines) ]
        if not all(file_sizes_match):
            # assume that inequality in any of the file sizes indicates a bad tar file
            return False,'{}/{} file sizes mismatch between the HPSS file and the local list'.format(sum(file_sizes_match),num_files)

        #*********
        # Success 
        #*********
        # if we got this far, the file appears to match; indicate that the file exists and is valid
        return True,''
       


#*******************************************************************************
#*******************************************************************************
#******************* Command line code *****************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":

    import sys

    archiveDir = sys.argv[1]

    testArchive = ArchiveILIAD()

    testArchive.archive(source_path = archiveDir)
