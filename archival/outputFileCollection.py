#!/usr/bin/env python
"""
Utilities for taking an ILIAD run directory and dividing the files up into
logical collections for archival purposes.

"""

import os
import glob
from iliad.utilities import parseILIADFileName as parser

def outputFileCollection(outputDirectory, \
                         extension = None, \
                         searchTerm = None):
  """
  Given an ILIAD run directory with CESM output files, outputFileCollection()
  stores the information necessary to create logically separate tar files for
  archival.

  :param outputDirectory: An output directory containing ILIAD output files

  :param extension: the extension given for archive file names

  :param searchTerm: the search term that uniquely yields ILIAD netCDF files
                     (if None, defaults to 'iliad*.nc')

  :return: A dict where each key is an archive file name, and each item is a list of files for that archive file

  outputFileCollection finds all netCDF files in the given output directory and
  sorts them into groups as follows:
    
    1. separates files into model output components (e.g., lnd vs atm, etc.)
    2. within the model output components, separates into output type (e.g., h0 vs h1, etc.)
    3. within each output type, separates files by year
    4. within each year, separates files by month

        
  """

  #Set the default search term if needed
  if searchTerm is None:
      searchTerm = 'iliad*.nc'

  #Get a list of netCDF files in the given directory
  ncFileList = glob.glob(outputDirectory + '/{}'.format(searchTerm))
  #Set the number of files
  numFiles = len(ncFileList)

  #Check if these are high-res regridding files
  if searchTerm is not None and 'hires-regridding' in outputDirectory:
      baseName = 'hires-regridded'
  elif searchTerm is not None and 'regridding' in outputDirectory:
      baseName = 'regridded'
  else:
      baseName = None

  #Initalize the output file dict
  outputFileDict = {}

  #Define a routine for safely adding files to the dict
  def __addFileToDict__(parsedObject,baseName = None):
    ofname = __outputFileName__(parsedObject,extension,baseName)
    if not ofname in outputFileDict:
      outputFileDict[ofname] = []
    #Add the parsedObject's file to the appropriate list
    outputFileDict[ofname].append("{}/{}".format(outputDirectory,parsedObject.fileName))

  #Parse and sort all the files
  [ __addFileToDict__(parser.parseILIADFileName(os.path.abspath(fname)),baseName) for fname in ncFileList ]

  #Check that the number of sorted files matches the original number
  assert numFiles == sum([ len(arch) for key,arch in outputFileDict.iteritems() ])

  return outputFileDict,parser.parseILIADFileName(os.path.abspath(ncFileList[0]))

    

def __outputFileName__( parsedObject, \
                        extension=None, \
                        baseName = None):
  """Constructs the archive filename for the archive into which the given parsedObject's file should go.

    :param parsedObject: a parsed object (of type parseILIADFileName) from the current heirarchy level

    :param extension: the archive file extension (tar is default if None is provided)

    :return: an output filename consistent with the ILIAD file names
  """

  #Set the default value for extension if necessary
  if extension == None:
    extension = 'tar'

  if 'regridded' in parsedObject.fileName.split('.')[0]:
      outputFileNameTemplate = "{bn}.{base}.{modelComponent}.{olev}.{year:04}.{month:02}.{ext}"

      outputFileName = outputFileNameTemplate.format( \
                        bn = baseName, \
                        base = parsedObject.fileName.split('.')[1], \
                        modelComponent = parsedObject.modelComponent, \
                        olev = parsedObject.outputLevel, \
                        year = parsedObject.runStartDate.year, \
                        month = parsedObject.runStartDate.month, \
                        res = parsedObject.resolutionCode, \
                        ext = extension)

  else:
      outputFileNameTemplate = "{base}.{modelComponent}.{olev}.{year:04}.{month:02}.{ext}"

      outputFileName = outputFileNameTemplate.format( \
                        base = parsedObject.fileName.split('.')[0], \
                        modelComponent = parsedObject.modelComponent, \
                        olev = parsedObject.outputLevel, \
                        year = parsedObject.runStartDate.year, \
                        month = parsedObject.runStartDate.month, \
                        res = parsedObject.resolutionCode, \
                        ext = extension)

  return outputFileName


#*******************************************************************************
#*******************************************************************************
#********************* Unit testing code ***************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":

  #Test outputFileCollection() on ./testing/run (which contains a suite of
  #empty files whose names were taken from an ILIAD run)
  testCollection = outputFileCollection('./testing/regridding',searchTerm='regridded*.nc')

  #Flag whether to print all the files going into each archive
  printAllFiles = False

  #Print a table header
  print "\n{}\t:\t{}".format("Archive file name                                         ","# of files contained")
  print "---------------------------------"

  #Loop over all the archive files
  for arch in testCollection:
    #Print each archive file name and the number of files to go in it
    print "{}\t:\t{}".format(arch,len(testCollection[arch]))

    #Print each file for the current archive
    if printAllFiles:
      for f in testCollection[arch]:
        print("\t" + f)
    
  print ""

