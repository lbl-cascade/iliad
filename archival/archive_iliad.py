#!/usr/bin/env python
"""
Implement data archiving for ILIAD using globus online.
"""
import os
import errno
import tarfile
import iliadCaseCollection as ifc
from archive import Archive, TransferList, requires_goapi

import multiprocessing as mp

def mkdir_p(path):
    """Emulates mkdir -p"""
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def createArchive(args):
    """Create a tar of all the items in ifcDict[archiveName].

    :param args: a tuple of arguments (tar_path, ifcDict, archiveName)

    This function is suitable for use with Pool.map()
    """
    #Extract the arguments
    tar_path, ifcDict, archiveName = args

    #Add the tar scratch path to the archive name
    fullArchivePath = tar_path + '/' + archiveName
    #open the archive
    with tarfile.open(fullArchivePath, 'w') as fout:
        #Write all files to the archive
        #This uses a list comprehension to reduce loop overhead
        [fout.add(ffile) for ffile in ifcDict[archiveName] ]


#try:
#    from cascade.components.archive import Archive, TransferList, requires_goapi
#except ImportError:

class ArchiveILIAD(Archive):
    """
    Implement data archiving for ILIAD
    """

    def __init__(self,  \
                        tar_procs = 9, \
                        tar_scratch = '/global/scratch2/sd/taobrien/cascade/scratch', \
                        be_verbose = True, \
                        **kwargs):
        """
        Initialize ArchiveILIAD object

        :param tar_procs: the number of processes to use for creating tar files

        :param tar_scratch: the location to create scratch files

        :param kwargs: All the arguments for the archive.Archive base class constructor.
        """
        super(ArchiveILIAD, self).__init__(**kwargs)
        # Change the default email notification behavior to send email to Travis in all cases
        if len(self.default_notify_on_success) == 0:
            self.default_notify_on_success = ['TAOBrien@lbl.gov', ]
        if len(self.default_notify_on_failure) ==0:
            self.default_notify_on_failure = ['TAOBrien@lbl.gov', ]

        #Save the tar options
        self.tar_procs = tar_procs
        self.tar_scratch = tar_scratch

        #Save the verbosity flag
        self.be_verbose = be_verbose

    def pprint(self,msg):
        """Prints if self.be_verbose is True"""
        if self.be_verbose:
            print(msg)

    @requires_goapi
    def prearchive(self,
                   source_endpoint,
                   source_path,
                   archive_endpoint,
                   archive_base_path,
                   recursive=None,
                   **kwargs):
        """
        Prepare data for archiving.

        This function is called by the archive(...) function before the archiving
        of the data is performed. Here one can, e.g., prepare data for archiving.
        The function also generates the list of transfers to be performed for
        archiving.

        To define a custom prearchive(..) function simply defined a derived class
        and overwrite the function.


        :param source_path: The full path the directory where the data is stored.

        :param source_endpoint: String indicating the name of the source endpoint.

        :param destination_path: The full path where the data should be stored.

        :param archive_endpoint: String indicating the name of the destination endpoint.

        :param recursive: Should archiving be done recursively for the path (True/False). The default value is None
                          indicating that the function should determine whether to use the recursive flag based on
                          the type of data to be transferred (i.e., if the path is dir then recursive will be set to
                          True and if it is a file then it will be set to False).

        :returns: This function returns a list of TransferList objects describing a set of
                  data transfers to be performed and the items to be transferred as part of
                  each transfer. Return in case the prearchive fails.

        """
        # Construct a dict of ILIAD files to archive
        caseCollection,parsedObject = ifc.iliadCaseCollection(source_path)

        #Get the resolution
        resolution = parsedObject.resolutionCode

        # Make the scratch directory
        mkdir_p(self.tar_scratch)

        # Initialize the multiprocessing pool
        tarPool = mp.Pool(self.tar_procs)

        # Create the arguments for tarPool.map() to send to createArchive()
        archiveArgs = [ (self.tar_scratch,caseCollection,archive) for archive in sorted(caseCollection) ]

        self.pprint("Started pre-caching tar files")
        # Run the tar job in parallel
        tarPool.map(createArchive,archiveArgs)
        self.pprint("Finished pre-caching tar files")

        # Create the list of globus transfer tasks to be scheduled
        output = []

        # Create a TransferList containing all the subtasks (i.e. files, dirs) 
        # to be tranferred as part of a globus task
        trans_list = TransferList()

        #Set the archive path
        archive_path = archive_base_path + '/' + resolution

        # Add each tarfile to the list of files to transfer
        for tar_path in caseCollection:
            trans_list.add_item(source_path=self.tar_scratch + '/' + tar_path,
                                destination_path=archive_path + '/' + tar_path, 
                                recursive=False,
                                verify_size=None)

        # Add the transfer to the list of globus transfers to be performed
        output.append(trans_list)

        # Return the list of al transfers to be performed
        return output

    @requires_goapi
    def archive(self,
                source_endpoint="nersc#dtn",
                source_path="",
                archive_endpoint="nersc#hpss",
                archive_base_path='/home/t/taobrien/cascade/iliad',
                recursive=None,
                notify_on_success=None,
                notify_on_failure=None,
                **kwargs):
        """
        Overwrite the Archive to archive method because we want to change the default values
        of some of the input parameters. Other then that we should only need to customize the
        pre-archive method.

        See Archive.archive(..) for details on the usage of the function.
        :param recursive:
        """
        super(ArchiveILIAD, self).archive(source_endpoint=source_endpoint, source_path=source_path,
                                          archive_endpoint=archive_endpoint, archive_base_path=archive_base_path,
                                          notify_on_success=notify_on_success, notify_on_failure=notify_on_failure)

    # TODO We may also need to overwrite the locate(...) and retrieve(...) functions inherited from Archive(...)


#*******************************************************************************
#*******************************************************************************
#******************* Command line code *****************************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":

    import sys

    archiveDir = sys.argv[1]

    testArchive = ArchiveILIAD(username='taobrienlbl')

    testArchive.archive(source_path = archiveDir)
