#!/usr/bin/env python
"""
Utilities for generating a list of tar files and tar files contents for
archiving an ILIAD simulation directory.
"""
import outputFileCollection as ofc
import os

def iliadCaseCollection(iliadDirectory,extension = None):
  """Given an ILIAD run directory, logically sorts all files for archival in separate tar files.

    :param iliadDirectory: the path to an ILIAD simulation

    :param extension: the extension for archive files (defaults to tar if None is given)

    :returns: a dict where each key in the dict corresponds to an archive file
              name and where each item is a list of files for the given archive file.

    The files are sorted first into output netCDF files and everything else
    (everything else goes into an archive called `{dirname}.casedirectory.tar`,
    where `dirname` is the base name of the ILIAD run directory).  The output
    netCDF files are sorted according to the heuristic in
    iliad.archival.outputFileCollection, which nominally sorts files by model
    componenent, output level, year, and month.

    Example usage for simple archiving:
    
    ```python

    import tarfile
    from iliad.archival import iliadCaseCollection

    #Generate the list of archives
    myCollection = iliadCaseCollection(myILIADDirectory)

    #Loop over archive files
    for arch in myCollection:

      #Write all the files in the archive list to the archive
      with tarfile.open(arch,'w') as fout:
        [fout.add(ffile) for ffile in myCollection[arch] ]

    ```
  """

  #Set the default value for extension if needed
  if extension is None:
    extension = 'tar'

  #Get the list of output archive files and their contents
  archiveFileDict,parsedObject = ofc.outputFileCollection(iliadDirectory + '/run')

  #Add the list of regridded files
  try:
      archiveFileDict.update(ofc.outputFileCollection(iliadDirectory + '/regridding',searchTerm='regridded.iliad*.nc')[0])
  except IndexError:
      print "Warning: no files found in {}/regridding".format(iliadDirectory)

  #Add the list of regridded files
  try:
      archiveFileDict.update(ofc.outputFileCollection(iliadDirectory + '/hires-regridding',searchTerm='regridded.iliad*.nc')[0])
  except IndexError:
      print "Warning: no files found in {}/hires-regridding".format(iliadDirectory)

  #Get the base name of the ILIAD run directory
  iliadBaseDirectory = os.path.abspath(iliadDirectory).split('/')[-1]

  #Check that we have archive files
  if len(archiveFileDict) == 0:
    raise RuntimeError,"No ILIAD netCDF files were found in {}/run; is {} an ILIAD run directory?".format(iliadDirectory,iliadBaseDirectory)

  #Walk through the output file directory and filter out any files matching {iliadBaseDirectory}/run/iliad*.nc
  dirWalk = os.walk(iliadDirectory)
  nonRunFiles = ["{}/{}".format(wtuple[0],ffile) for wtuple in dirWalk for ffile in wtuple[2] if \
                    (not (wtuple[0].split('/')[-1] == 'run' \
                     and ffile[:5] == 'iliad' and ffile[-3:] == '.nc') ) and \
                    (not (wtuple[0].split('/')[-1] == 'regridding' \
                     and ffile[:9] == 'regridded' and ffile[-3:] == '.nc') ) and \
                    (not (wtuple[0].split('/')[-1] == 'hires-regridding' \
                     and ffile[:9] == 'regridded' and ffile[-3:] == '.nc') ) \
                    ]

  #Set the name of the tarfile containing the case directory files
  caseTarFile = "{}.casedirectory.{}".format(iliadBaseDirectory,extension)
  archiveFileDict[caseTarFile] = nonRunFiles

  #Return the file dict
  return archiveFileDict,parsedObject

#*******************************************************************************
#*******************************************************************************
#******************** Unit testing code ****************************************
#*******************************************************************************
#*******************************************************************************

if __name__ == "__main__":
  import tarfile

  testDirectory = '/scratch3/scratchdirs/taobrien/cascade/iliad/ne120np4/iliad-ne120np4-2005_01_01_00-2005_05_01_00/'
  #testDirectory = './testing'

  testCollection,_ = iliadCaseCollection(testDirectory)

  #Flag whether to print all the files going into each archive
  printAllFiles = False

  #Print a table header
  print "\n{}\t:\t{}\t{}".format("Archive file name                                         ","# of files contained","Size (GB)")
  print "---------------------------------"

  #Loop over all the archive files
  for arch in sorted(testCollection):

    #Get the size of the archive; convert it from Bytes to GB
    archSize = sum([ os.path.getsize(ffile) for ffile in testCollection[arch]])/(1024.**3)

    #Print each archive file name and the number of files to go in it
    print "{}\t:\t{}\t\t\t{:0.0f}".format(arch,len(testCollection[arch]),archSize)

    #Print each file for the current archive
    if printAllFiles:
      for f in testCollection[arch]:
        print("\t" + f)


    
  print ""

