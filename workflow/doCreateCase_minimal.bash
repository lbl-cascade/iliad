#!/bin/bash

#ILIAD NUM_INST=$1
#ILIAD NUM_PES_PER_INST=240
#ILIAD PES_PER_NODE=240
#ILIAD NUM_THRDS=4
#ILIAD COMPILER="intel"
#ILIAD
#ILIAD MACHINE="edison"
#ILIAD ILIAD_SOURCE="/global/project/projectdirs/m1949/local/${MACHINE}/iliad"
#ILIAD ILIAD_DOMAIN="ne120np4"
#ILIAD ILIAD_IC_DIR="${SCRATCH}/cascade/iliadHindcasts/iliad-${ILIAD_DOMAIN}/initialConditions"
#ILIAD ILIAD_USER_NAMELIST_TEMPLATE_DIRECTORY="${SCRATCH}/cascade/iliadHindcasts/iliad-${ILIAD_DOMAIN}/namelistTemplates"
#ILIAD INPUTDATADIR=/global/project/projectdirs/m1949/model_input/cesm/inputdata
#ILIAD CESM_BASE="/project/projectdirs/m1949/cesm_src/cesm1_2_2/"
#ILIAD ENV_MACH_SPECIFIC="/project/projectdirs/m1949/cesm_src/env_mach_specific.${MACHINE}"
#ILIAD PYTHONPATH_ADDENDUM="/global/project/projectdirs/m1949/local/${MACHINE}"
#ILIAD MODULEPATH_PREPENDUM="/project/projectdirs/m1949/local/${MACHINE}/modulefiles"

#******************************************************************************
#*********** NOTE: the above variables need to be written *********************
#***************** by the templating script. **********************************
#******************************************************************************

#******************************
# ILIAD SYSTEM CONTROL PARAMS
#******************************
#TAO: note that the values of the following variables
#don't matter for this script; they are overridden by the clone script
PBSQUEUE="regular"
WALLTIMESTR="00:20:00"
GMT_START="2000:01:01:00"
GMT_END="2001:01:10:00"

FORECASTHOURLIST="0"
CASENAME="${ILIAD_DOMAIN}-${NUM_INST}inst-${NUM_PES_PER_INST}proc${NUM_THRDS}thrds"
NUM_PES=`expr ${NUM_INST} \* ${NUM_PES_PER_INST} \* ${NUM_THRDS}`
NUMPERTWOINST=`expr 2 \* ${NUM_PES_PER_INST}`
APPROX_NUM_NODES=`expr ${NUM_PES} \/ ${PES_PER_NODE}`
NUM_TASK=`expr ${NUM_PES} \/ ${NUM_THRDS}`

if [ -d "${CASENAME}" ]; then
  rm -rf ${CASENAME}
fi

${CESM_BASE}/scripts/create_newcase \
  -case $CASENAME \
  -compset FC5 \
  -res ${ILIAD_DOMAIN}_${ILIAD_DOMAIN} \
  -mach ${MACHINE}

if [ "$?" -ne "0" ]; then
  echo "create_newcase failed with error $?"
  exit $?
fi

#Copy this build script to the case directory so we have a record of how the
#case was made
cp $0 $CASENAME/
cd $CASENAME

#Copy ILIAD namelist template files to the case directory
cp ${ILIAD_USER_NAMELIST_TEMPLATE_DIRECTORY}/*_0001 .

#Run/build location
./xmlchange -file env_build.xml -id EXEROOT -val $PWD/bld
./xmlchange -file env_run.xml -id RUNDIR -val $PWD/run

#Build options
if [ "${COMPILER}" == "gnu" ]; then
  ./xmlchange -file env_build.xml -id COMPILER -val "gnu"
fi
#Turn on debug mode
#./xmlchange -file env_build.xml -id DEBUG -val "TRUE"

#Turn off chemistry
./xmlchange -file env_build.xml -id CAM_CONFIG_OPTS -val "-phys cam5 -chem none"
./xmlchange -file env_build.xml -id RTM_MODE -val "NULL"

#Run length optiosn
./xmlchange -file env_run.xml -id STOP_OPTION -val nstep
./xmlchange -file env_run.xml -id STOP_N -val 10
./xmlchange -file env_run.xml -id RESUBMIT -val 1

#PIO options
./xmlchange -file env_run.xml -id ATM_PIO_STRIDE -val 24
./xmlchange -file env_run.xml -id ATM_PIO_ROOT -val 0
./xmlchange -file env_run.xml -id ATM_PIO_NUMTASKS -val ${APPROX_NUM_NODES}
./xmlchange -file env_run.xml -id ATM_PIO_TYPENAME -val 'pnetcdf'

./xmlchange -file env_run.xml -id PIO_STRIDE -val 24
./xmlchange -file env_run.xml -id PIO_ROOT -val 1
./xmlchange -file env_run.xml -id PIO_NUMTASKS -val ${APPROX_NUM_NODES}

#Timer options
#./xmlchange -file env_run.xml -id CHECK_TIMING -val TRUE
#./xmlchange -file env_run.xml -id SAVE_TIMING -val TRUE
#./xmlchange -file env_run.xml -id TIMER_LEVEL -val 4
#./xmlchange -file env_run.xml -id COMP_RUN_BARRIERS -val TRUE

#Number of processors
./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_ATM -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_ATM -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_LND -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_LND -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_ICE -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_ICE -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_OCN -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_OCN -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_GLC -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_GLC -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_ROF -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_ROF -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_WAV -val $NUM_THRDS
./xmlchange -file env_mach_pes.xml -id NINST_WAV -val $NUM_INST

./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val $NUM_TASK
./xmlchange -file env_mach_pes.xml -id NTHRDS_CPL -val $NUM_THRDS

#Inputdata directory
./xmlchange -file env_run.xml -id DIN_LOC_ROOT -val ${INPUTDATADIR}
./xmlchange -file env_run.xml -id DIN_LOC_ROOT_CLMFORC -val ${INPUTDATADIR}/atm/datm7

#Archive directory
./xmlchange -file env_run.xml -id DOUT_S_ROOT -val ${PWD}/archive
./xmlchange -file env_run.xml -id DOUT_S -val FALSE
./xmlchange -file env_archive.xml -id DOUT_S -val FALSE


#If a machine-specific env file exists, copy it here
if [ -e "${ENV_MACH_SPECIFIC}" ];
then
  cp ${ENV_MACH_SPECIFIC} ./env_mach_specific
fi
#Modify env_mach_specific for the GNU compiler to work
#sed -i "s:gcc/.*:gcc/4.8.0:" env_mach_specific


#********************************************************************************
#********************************************************************************
#***************** End of XML configuration *************************************
#********************************************************************************
#********************************************************************************

#Run cesm_setup
./cesm_setup

#Add a flag to FFLAGS in the Macros file
#(this must happen after cesm_setup)
if [ "${COMPILER}" == "gnu" ]; then
  sed -i "s/\(FFLAGS:=.*\)/\1\ -fno-range-check\ -fcray-pointer\ -mcmodel=large\ -dynamic\nLDFLAGS:=-mcmodel=large\ -dynamic\n/" Macros
else
  sed -i "s/\(FFLAGS:=.*\)/\1\ -mcmodel=medium\ -dynamic\nLDFLAGS:=-mcmodel=medium\ -dynamic\n/" Macros
fi

#Change the PBS queue and add a line to send e-mails about job exit
PBSFILE=${CASENAME}.run
sed -i "s/^#PBS\ -q\ regular/#PBS\ -q\ ${PBSQUEUE}\n#PBS\ -m\ a/" ${PBSFILE}
#Set the wall time
sed -i "s/^#PBS\ -l\ walltime=.*/#PBS\ -l\ walltime=${WALLTIMESTR}/" ${PBSFILE}

BUILDDIR=$PWD

cat > ${CASENAME}.build.pbs << EOF
#!/bin/csh -f

#PBS -N bld.${CASENAME}
#PBS -q ${PBSQUEUE}
#PBS -m a
#PBS -l mppwidth=24
#PBS -l walltime=00:29:00
#PBS -j oe
#PBS -S /bin/csh -V


cd $BUILDDIR

./${CASENAME}.build

EOF
