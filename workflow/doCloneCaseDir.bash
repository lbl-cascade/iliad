#!/bin/bash

#ILIAD GMT_START="2000:05:01:00"
#ILIAD GMT_END="2000:09:01:00"
#ILIAD 
#ILIAD NUM_PES_PER_INST=240
#ILIAD PES_PER_NODE=240
#ILIAD PBSQUEUE="regular"
#ILIAD WALLTIMESTR="03:00:00"
#ILIAD NUM_THRDS=4
#ILIAD DO_SUBMIT=1
#ILIAD 
#ILIAD MACHINE="edison"
#ILIAD ILIAD_DOMAIN="ne120np4"
#ILIAD ILIAD_SOURCE="/global/project/projectdirs/m1949/local/${MACHINE}/iliad"
#ILIAD ILIAD_IC_DIR="${SCRATCH}/cascade/iliadHindcasts/iliad-${ILIAD_DOMAIN}/initialConditions"
#ILIAD ILIAD_USER_NAMELIST_TEMPLATE_DIRECTORY="${SCRATCH}/cascade/iliadHindcasts/iliad-${ILIAD_DOMAIN}/namelistTemplates"
#ILIAD ILIAD_CASE_TEMPLATE_DIRECTORY="${SCRATCH}/cascade/iliadHindcasts/iliad-${ILIAD_DOMAIN}/caseTemplates"
#ILIAD INPUTDATADIR=/global/project/projectdirs/m1949/model_input/cesm/inputdata
#ILIAD CESM_BASE="/project/projectdirs/m1949/cesm_src/cesm1_2_2/"
#ILIAD ENV_MACH_SPECIFIC="/project/projectdirs/m1949/cesm_src/env_mach_specific.${MACHINE}"
#ILIAD PYTHONPATH_ADDENDUM="/global/project/projectdirs/m1949/local/${MACHINE}"
#ILIAD MODULEPATH_PREPENDUM="/project/projectdirs/m1949/local/${MACHINE}/modulefiles"

#******************************************************************************
#*********** NOTE: the above variables need to be written *********************
#***************** by the templating script. **********************************
#******************************************************************************

#******************************
# ILIAD SYSTEM CONTROL PARAMS
#******************************
FORECASTHOURLIST="0"

#*************************
#Calculate case params
#*************************
#Get the number of instances
NUM_INST=`python ${ILIAD_SOURCE}/utilities/dateensemble.py --gmtstartdate=${GMT_START} --gmtenddate=${GMT_END} --forecasthours ${FORECASTHOURLIST} -c`
#Set the case name
CASENAME="iliad-${ILIAD_DOMAIN}-${GMT_START//:/_}-${GMT_END//:/_}"
#Set the number of PES required
NUM_PES=`expr ${NUM_INST} \* ${NUM_PES_PER_INST} \* ${NUM_THRDS}`
#Set a helper variable that sets the number of PES used for two instances (this helps ensure proper decomposition)
NUMPERTWOINST=`expr 2 \* ${NUM_PES_PER_INST}`

#Set the case template directory
#CASE_TEMPLATE="${ILIAD_CASE_TEMPLATE_DIRECTORY}/iliad-${ILIAD_DOMAIN}-${NUM_INST}inst"
CASE_TEMPLATE="${ILIAD_CASE_TEMPLATE_DIRECTORY}/${ILIAD_DOMAIN}-${NUM_INST}inst-${NUM_PES_PER_INST}proc${NUM_THRDS}thrds"

if [ -d "${CASENAME}" ]; then
  rm -rf ${CASENAME}
fi

echo "Creating ${CASENAME} from ${CASE_TEMPLATE}"
${CESM_BASE}/scripts/create_clone \
  -case $CASENAME \
  -clone ${CASE_TEMPLATE} 

if [ "$?" -ne "0" ]; then
  echo "create_clone failed with error $?"
  exit $?
fi

#Copy this clone script to the case directory so we have a record of how the
#case was made
cp $0 $CASENAME/

#Move into the case directory
cd $CASENAME

#Copy ILIAD namelist template files to the case directory
cp ${ILIAD_USER_NAMELIST_TEMPLATE_DIRECTORY}/*_0001 .

echo "Creating the instance date list"

#Run location
./xmlchange -file env_build.xml -id EXEROOT -val $PWD/bld
./xmlchange -file env_run.xml -id RUNDIR -val $PWD/run
#Archive directory
./xmlchange -file env_run.xml -id DOUT_S_ROOT -val ${PWD}/archive
mkdir ${PWD}/archive


#Run length options
./xmlchange -file env_run.xml -id STOP_OPTION -val ndays
./xmlchange -file env_run.xml -id STOP_N -val 5
#./xmlchange -file env_run.xml -id RESUBMIT -val 0

#PIO options
#./xmlchange -file env_run.xml -id ATM_PIO_NUMTASKS -val="2420"  />    

#Archive options
#Turn off model restarts
./xmlchange -file env_run.xml -id REST_OPTION -val never
./xmlchange -file env_run.xml -id REST_N -val 1

#********************************************************************************
#********************************************************************************
#***************** Hijack the preview_namelists command *************************
#********************************************************************************
#********************************************************************************

HIJACKED_PREVIEW_NAMELISTS="preview_namelists_double"

cat > preview_namelists << EOF
#! /bin/csh -f

#Run the hijacted preview namelists script to create a false double-instance set of CESM namelist files
# that are based on the user_*_0001 files with template flags
./${HIJACKED_PREVIEW_NAMELISTS}

source ./Tools/ccsm_getenv || exit -1

#Make a namelist template directory and a namelist file directory
mkdir -p namelistTemplates
mkdir -p namelistFiles

#Copy the just-created namelist files for instance 0001 into the namelistTemplates folder
cp CaseDocs/*_0001 namelistTemplates

#Allow these files to be written (non-writing permissions are inherited from CaseDocs)
chmod +w namelistTemplates/*

#Change any instance of 0001 in the namelist files to a mako template string
sed -i 's/0001/\$\{ensemblenumber}/g' namelistTemplates/*

#Run the mako template script
setenv PYTHONPATH ${PYTHONPATH}:${PYTHONPATH_ADDENDUM}
module load mako
python ${ILIAD_SOURCE}/core/createCESMNamelistEnsemble.py \
  --icdirectory=${ILIAD_IC_DIR} \
  --domain=${ILIAD_DOMAIN}  \
  --gmtstartdate=${GMT_START} \
  --gmtenddate=${GMT_END} \
  --templatedir=./namelistTemplates \
  --outputdir=./namelistFiles \
  --forecasthours=${FORECASTHOURLIST} \
  --cesminputdir=${INPUTDATADIR} \
  --oceandomainfile=domain.ocn.1x1.111007.nc \
  --oceandomaindir=${INPUTDATADIR}/ocn/docn7

#Copy the templated namelist files to CaseDocs
cp -f namelistFiles/* CaseDocs

#Copy the templated namelist files to run
cp -f namelistFiles/* run

#Print feedback to the screen
cat << EOF2

This is a hijacked version of preview_namelists that first calls
preview_namelists_double to generate a two-instance set of namelists, and then
uses ${ILIAD_SOURCE}/core/createCESMNamelistEnsemble.py 
to create the actual set of *_NNNN input files.  This is done because
preview_namelists is unacceptably slow to generate large numbers of namelist
files.

EOF2
EOF

#Run the setup script again (apparently this is necessary even after a clone)
echo "Running cesm_setup"
./cesm_setup

#Modify the PBS script
PBSFILE=${CASENAME}.run
sed -i "s/^#PBS\ -q\ regular/#PBS\ -q\ ${PBSQUEUE}\n#PBS\ -m\ a/" ${PBSFILE}
#Set the wall time
sed -i "s/^#PBS\ -l\ walltime=.*/#PBS\ -l\ walltime=${WALLTIMESTR}/" ${PBSFILE}

#Change the way the cpl and cesm log files are named in the CESM run script; this must be
#done because the log file names are altered by the hijacked preview_namelists script
sed -i "s:setenv\ LID.*:setenv\ LID\ \`grep logfile run/atm_modelio.nml_0001 | sed -e \"s/.*\\\.//\" |sed -e \"s/'//\"\`:" ${PBSFILE}
sed -i "s:set\ CESMLogFile.*:set\ CESMLogFile\ =\ \"cesm.log.\${LID}\":" ${PBSFILE}
sed -i "s:set\ CPLLogFile.*:set\ CPLLogFile\ =\ \"cpl.log.\${LID}\":" ${PBSFILE}

#Fix a bug in CESM 1.2.2 associated with timing
sed -i 's:CASETOOLS/st_archive.sh:CASETOOLS/st_archive.sh;\ cd $CASEROOT:' ${PBSFILE}

#*********************************************************
# Split the PBS script into the prestage and run portions
#*********************************************************

#*******************
# The prestage file
#*******************
PRESTAGE=${CASENAME}.prestage

cat > ${PRESTAGE} << EOFP
#!/bin/csh -f
###PBS -A 
#PBS -N preview_nml
#PBS -q premium
#PBS -m a
#PBS -l mppwidth=1
#PBS -l walltime=00:29:00
#PBS -j oe
#PBS -S /bin/csh -V

# ---------------------------------------- 
EOFP

awk '/PE\ LAYOUT/,/CESM\ PRESTAGE\ SCRIPT\ HAS\ FINISHED\ SUCCESSFULLY/' $PBSFILE >> ${PRESTAGE}

cat >> ${PRESTAGE} << EOFP2
echo "-------------------------------------------------------------------------"

#Ensure that the qsub command has no job dependencies
./xmlchange -file env_run.xml -id BATCHSUBMIT -val qsub
#set the environment variable too
setenv BATCHSUBMIT qsub

cat > tempres <<EOF
\$BATCHSUBMIT -V ./\$CASE.run
EOF
source tempres
if (\$status != 0) then
 echo "ccsm_postrun error: problem sourcing tempres " 
endif
rm tempres
EOFP2

#*******************
# The run file
#*******************
TMPRUNFILE=${CASENAME}.tmp

#Extract the PBS header (extract to the first blank line)
awk '1; /^$/ {exit}' ${PBSFILE} | sed 's/csh\ -f/csh\ -V/' > $TMPRUNFILE
#Add the directory change line
echo 'cd $RUNDIR' >> $TMPRUNFILE
#Add the execution part of the PBS file (and replace the resubmission line with a line that runs
#the prestage script
awk '/CSM\ EXECUTION\ BEGINS\ HERE/,0' ${PBSFILE} | sed 's/CASE\.run/CASE\.prestage/' >> $TMPRUNFILE 
#Overwrite the original PBS file
mv ${TMPRUNFILE} ${PBSFILE}

#Replace the run line in the submit script with the prestage script
SUBMITFILE=${CASENAME}.submit
sed -i 's/{CASE}\.run/{CASE}\.prestage/' ${SUBMITFILE}

#Remove the line that runs preview_namelists upon build 
sed -i "s:^./preview_namelists:#./preview_namelists\necho\ 'ILIAD skipping namelist creation':" ./${CASENAME}.build

#Run the build script again (apparently this is necessary even after a clone)
echo "Running build"
./${CASENAME}.build

#Submit the case if requested
echo "${CASENAME} is ready to submit"
if [ "${DO_SUBMIT}" -eq "1" ]; then
  ./${CASENAME}.submit
fi
