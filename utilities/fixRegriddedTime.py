#!/usr/bin/evn python
import netCDF4 as nc
import os
import datetime as dt
import numpy as np

def fixTimeVar(regridded_file, \
                time_units = 'days since 1979-01-01 00:00:00', \
                run_base_time = None,
                be_verbose = False):
    """Given a regridded and archived ILIAD file, fix the time varible to reflect
       the actual time represented by the data."""


    #Get the base date of the file
    if run_base_time is None:
        init_date_stamp = os.path.basename(regridded_file).split('.')[-2].split('_')[-1]
        file_date_stamp = os.path.basename(regridded_file).split('.')[-3]

    #parse the year, month, day, and hour
    iyear = int(init_date_stamp[:4])
    imonth = int(init_date_stamp[5:7])
    iday = int(init_date_stamp[8:10])
    ihour = int(init_date_stamp[11:13])

    #Create a units variable
    old_units = "days since {:04}-{:02}-{:02} {:02}:00:00".format(iyear,imonth,iday,ihour)

    #Create a datetime object from the file stamp
    fyear = int(file_date_stamp[:4])
    fmonth = int(file_date_stamp[5:7])
    fday = int(file_date_stamp[8:10])
    fhour = int(file_date_stamp[11:13])
    file_stamp_obj = dt.datetime(fyear,fmonth,fday,fhour)

    #if be_verbose:
    #    print "Checking {}".format(regridded_file),'...'
    #First check if we need to do any fixing
    doReturn = False
    #with nc.Dataset(regridded_file,'r') as fin:
    fin = nc.Dataset(regridded_file,'r')
    if True:
        tunits = fin.variables['time'].units
        t0 = fin.variables['time'][0]
        t0_file = nc.date2num(file_stamp_obj,tunits)
        #If the time units are already set, then we need do nothing further
        if tunits == time_units and t0_file == t0:
            doReturn = True
    fin.close()
    del(fin)

    if doReturn : 
        #if be_verbose:
        #    print "nothing to do on {}; skipping.".format(regridded_file)
        return

    if be_verbose:
        print "fixing time var in {}...".format(regridded_file)
    #Otherwise open the file for writing
    #with nc.Dataset(regridded_file,'r+') as fio:
    fio = nc.Dataset(regridded_file,'r+')
    if True:
        time = fio.variables['time'][:]
        time_bnds_lo = fio.variables['time_bnds'][:,0]
        time_bnds_hi = fio.variables['time_bnds'][:,1]

        #Convert the time variable into our new units
        dtobjs = nc.num2date(time,old_units)
        new_times = nc.date2num(dtobjs,time_units)

        #Convert the time bounds into our new units
        dtobjs = nc.num2date(time_bnds_lo,old_units)
        new_time_bnds_lo = nc.date2num(dtobjs,time_units)
        dtobjs = nc.num2date(time_bnds_hi,old_units)
        new_time_bnds_hi = nc.date2num(dtobjs,time_units)

        #Set the units
        fio.variables['time'].units = time_units
        #Set the calendar properly
        fio.variables['time'].calendar = 'standard'
        #Write the new time values
        fio.variables['time'][:] = new_times
        #Write the new time bounds values
        fio.variables['time_bnds'][:,0] = new_time_bnds_lo
        fio.variables['time_bnds'][:,1] = new_time_bnds_hi
    fio.close()
    del(fio)




if __name__ == "__main__" : 
    import sys
    from multiprocessing import Pool


    #Get the list of files from the command line
    files = sys.argv[1:]

    multiprocess = True
    if multiprocess:
        #Use multiple procs to fix the files
        fixPool = Pool(8)
        fixPool.map(fixTimeVar,files)
    else:
        for file in files:
            fixTimeVar(file,be_verbose=True)

