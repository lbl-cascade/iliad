#!/usr/bin/env python
import glob
import os
import collections
import depcache
import errno

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

#res = 'ne16np4'
res = os.path.getcwd()[-1]
regridScriptArgBase = '-c -w /project/projectdirs/m1949/iliadAuxilliaryFiles/multiResRegriddingFiles/tempestremap/tempestremap_{res}_persiann-2.0-degree_weights.nc'.format(res=res)

cwd = os.getcwd()
baseDir = '/scratch3/scratchdirs/taobrien/cascade/iliad/' + res

runFiles = collections.OrderedDict()
regridDirs = collections.OrderedDict()
#Get a list of run directories and all the relevant files to regrid within
runDirs = [ dir + '/run' for dir in \
            sorted(glob.glob(baseDir + '/iliad-*'))]

runFiles[res] = collections.OrderedDict()
regridDirs[res] = collections.OrderedDict()
for runDir in runDirs:
    #Get the run code
    run = runDir.split('/')[-2]

    #Set the search string for the iliad nc files
    runFiles[res][run] = runDir + '/iliad*.cam*.nc'

    #Construct the regrid directory for this run
    regridDirs[res][run] = '/'.join(runDir.split('/')[:-1]) + '/regridding' 
    mkdir_p(regridDirs[res][run])

#Set common options for depcache_run
pythonScript = '/global/project/projectdirs/m1949/local/edison//esmfregrid/regridFile'
numProcs = 156
limitCoresPerNode = 4
additionalBatchScriptLines = ["setenv OMP_NUM_THREADS 6"]
additionalAprunArgs = ["-S 2 -d 6"]
pbsQueue = 'premium'
pbsWalltime = '00:30:00'
doSubmit = True
beVerbose = False
doBuildBundle = True

#Go through each run and submit a regridding job
for res in runFiles:
  for run in runFiles[res]:
    pythonScriptArgs = regridScriptArgBase + ' ' + runFiles[res][run]

    #Change to the regridding directory
    os.chdir(regridDirs[res][run])

    #Run depcache_run
    jobID = depcache.depcache_run( \
                 pythonScript, \
                 pythonScriptArgs, \
                 numProcs = numProcs, \
                 pbsQueue = pbsQueue, \
                 pbsWalltime = pbsWalltime, \
                 doSubmit = doSubmit, \
                 depcacheDir = '/dev/shm', 
                 beVerbose = beVerbose, \
                 doBuildBundle = doBuildBundle, \
                 limitCoresPerNode = limitCoresPerNode, \
                 additionalBatchScriptLines = additionalBatchScriptLines, \
                 additionalAprunArgs = additionalAprunArgs, \
                 )

    #Change back to the original directory
    os.chdir(cwd)

    #Print the res, run and jobID
    print res,run,jobID
