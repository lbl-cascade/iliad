#!/usr/bin/env python
"""
Executes an NCL script (nclScript) and passes any variables provided in
**kwargs to the NCL code.  This can be used to wrap NCL code.

Example:
    import ncl

    nclScript = "begin\n print(a)\n print(b)\nend\n"

    try:
      #Execute the NCL script with the arguments a and b
      nclOutput = ncl.execute(nclScript,a="Hello World!",b=1.0)
    except BaseException as e:
      print "Execution of {} failed: {}".format(tempFile,e)

"""
import os
import argparse
import subprocess
import sys
import numpy as np

def execute(nclScript,_doRemoveScript_=True,**kwargs):
  """Executes an NCL script (nclScript) and passes any variables provided in **kwargs to the NCL code.  This can be used to wrap NCL code.  nclScript can either be code (detected by the presence of \\n) or a script name."""

  tempFile = ""
  #If there is a carraige return in the ncl script name, then assume that it is raw NCL code rather than
  # a script name; write the code to a temporary file if so
  if( "\n" in nclScript ):
    nclCode = nclScript
    #Write the ncl script to a temporary file
    tempFile = "test.{}.ncl".format(os.getpid())
    with open(tempFile,'w') as fout:
        fout.write(nclCode)

    #Re-set the script name
    nclScript = tempFile

  else:
      #Set that we shouldn't remove the script
      _doRemoveScript_ = False
      #Check whether the given ncl script exists
      hasFoundScript = False
      if(not os.path.exists(nclScript)):
        #Check whether the script exists in the python path
        searchpaths = list(sys.path)
        #Finally, check whether the script exists in the same directory as this file
        searchpaths.append(os.path.dirname(os.path.realpath(__file__)))
        for pathdir in searchpaths:
          potentialName = "{}/{}".format(pathdir,nclScript)
          if(os.path.exists(potentialName)):
            hasFoundScript = True
            nclScript = potentialName
            break

      else:
        hasFoundScript = True

      if(not hasFoundScript):
        raise ValueError, "{} does not exist".format(nclScript)

  #Initialize the NCL command
  #TODO: check that this is in the path and is executable
  nclExecCommand = ["ncl"]

  #Iterate over the provided variables and add to the execution command
  #list in the form var=value (which sets var=value in the NCL script)
  #Treat string differently from other variables
  for var,value in kwargs.iteritems():
    #TODO: check that the provided value is something than
    #can map sensibly to an NCL type

    if(type(value) is str):
      #Check if the provided value is a string; protect
      #the string argument by wrapping quotation marks around it
      additionalArg = "{}=\"{}\"".format(var,value)
    elif(type(value) is list or type(value) == np.ndarray or type(value) is tuple):
      #Check if we are passing in a list; check if the
      #the types of the values are numeric or string
      isNumeric = False
      isString = False
      try:
          1 + value[0]
          isNumeric = True
      except:
          try:
              value[0].join(["1","2","3"])
              isString = True
          except BaseException as e:
              print e
              pass
      #Check that the type is something we can handle
      if not isNumeric and not isString:
          raise ValueError,"Argument {} is neither numeric nor stringlike".format(var)

      try:
          # Check that the type of all items is the same
          vtype = type(value[0])
          if not all([ type(val) == vtype for val in value ]):
              raise ValueError
      except:
          raise ValueError,"Variable {} appears to be a list, but not all items have the same type".format(var)
        
      if isNumeric:
          innard = ",".join([str(val) for val in value])
          #additionalArg = var + r' = (~'.replace('~',chr(47)) + innard + "/)"
          additionalArg = var + r' = (/' + innard + "/)"
      
      if isString:
          innard = '","'.join(value)
          additionalArg = r'{}=(/"{}"/)'.format(var,innard)

    else:
      #Otherwise, simply provide the argument
      additionalArg = "{}={}".format(var,value)

    #Append the given argument to the execution list
    nclExecCommand.append(additionalArg)

  #Finally, append the script name to the execution list
  nclExecCommand.append(nclScript)

  #Initialize the returnOutput to none, since this variable may not
  #otherwise be created if subprocess.check_output() fails
  returnOutput = None
  try:
    #Execute the script by providing check_output with the execution list
    #Gather all the NCL output (STDOUT/STDERR) into returnOutput
    returnOutput = subprocess.check_output(nclExecCommand,stderr = subprocess.STDOUT)

    #Check whether the "fatal:" keyword is present in the output, which indicates
    #that the NCL script failed (this is necessary because NCL doesn't use return codes)
    #TODO: is there a more robust method for determining failure?
    # (changing the "end" statement to "status_exit(0) /n end /n status_exit(1)" in custom NCL scripts will apparently trigger NCL to raise exit code 1 for most abnormal termination circumstances.  Combined with the 'fatal' search, this may be a good solution.
    if("fatal:" in returnOutput):
      #Raise an error if the script failed
      raise RuntimeError,"NCL code had a fatal error"
  #Check if NCL execution failed in any way and report an informative error message
  except BaseException as e:
    print \
        """
Executing the following command failed:
  {}

Execption is: {}
        """.format(" ".join(nclExecCommand),e)

    if(returnOutput is not None):
      print "NCL output was: "
      print ""
      print returnOutput

    raise e

  if(_doRemoveScript_):
      try:
        os.remove(tempFile)
      except:
        pass

  #Return the output of the NCL script
  return returnOutput


#*******************************************************************************
#*******************************************************************************
#*********************** Unit testing code *************************************
#*******************************************************************************
#*******************************************************************************
if(__name__ == "__main__"):

  #Create a dummy NCL script that prints the values "a" and "b"
  # this will fail unless "a" and "b" are specified in the command list
  nclScript = \
"""
begin

  print(a)
  print(b)
  print(c)
  print(d)
  print(e)

status_exit(0)
end
status_exit(1)
"""

  try:
    #Execute the NCL script with the arguments a and b
    nclOutput =  execute(nclScript,_doRemoveScript_=False,a="Hello World!",b=1.0,c=np.array([1,2,3,4]),d=["a.nc","b.nc","d.nc"],e = ("a.nc","b.nc","d.nc"))

    #Print the output of the NCL code
    print nclOutput
  except BaseException as e:
    raise# "Execution failed: {}".format(e)


  
