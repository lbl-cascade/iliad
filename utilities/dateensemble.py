#!/usr/bin/env python
import datetime as dt

def dateStringToDatetime(dateString):
  import datetime as dt
  """Transforms a date string of the format "YYYY:MM:DD:HH" into a datetime object"""
  #Transform the dates into datetime objects
  try:
    return dt.datetime(*tuple([int(mystr) for mystr in dateString.split(":")]))
  except:
    raise ValueError,"Could not parse dateString.  Expecting date of form YYYY:MM:DD:HH, but got {}".format(dateString)

def isLeap(year):
  """Returns True if `year` is a leapyear; False otherwise"""
  if(year % 4 != 0):
    return False
  elif(year % 100 != 0):
    return True
  elif(year % 400 == 0):
    return True
  else:
    return False

def daysPerMonth(year):
  return [31,28+int(isLeap(year)),31,30,31,30,31,31,30,31,30,31]

class ensemble:

  def __init__(self,startdate,enddate,forecastHours=[0,6,12,18]):
    """

    Creates a list of dates within a given time span (input arguments
    are datetime-like objects).

    The returned object is a list of datetime objects.

    """

    #***************************************************************************
    #***************************************************************************
    #*********** Create the list of dates within the given range ***************
    #***************************************************************************
    #***************************************************************************

    #First check that the hour portion of startDate and endDate are in the
    #forecastHours list
    if not startdate.hour in forecastHours:
      #If not, recreate startdate using the lowest value in the forecastHours list
      #(this assumes forecastHours is ordered)
      startdate = dt.datetime(startdate.year,startdate.month,startdate.day,forecastHours[0])

    if not enddate.hour in forecastHours:
      #If not, recreate startdate using the highest value in the forecastHours list
      #(this assumes forecastHours is ordered)
      enddate = dt.datetime(enddate.year,enddate.month,enddate.day,forecastHours[-1])



    #Create a list of potential dates starting from the beginning of the
    #requested year to the end of the requested year
    possibleDates = []
    for year in range(startdate.year,enddate.year+1):
      for month in range(12):
        for day in range(daysPerMonth(year)[month]):
          for hour in forecastHours:
            try:
              possibleDates.append(dt.datetime(year,month+1,day+1,hour))
            except BaseException as e:
              print "Datetime failed for year={}, month = {}, day = {}, hour = {}".format(year,month+1,day+1,hour)
              raise BaseException, e

    istart = possibleDates.index(startdate)
    iend = possibleDates.index(enddate)

    self.dates = possibleDates[istart:iend]


if(__name__ == "__main__"):
    import argparse
    #*******************************************************************************
    #*******************************************************************************
    #********************** Parse command line arguments ***************************
    #*******************************************************************************
    #*******************************************************************************
    parser = argparse.ArgumentParser()

    parser.add_argument('--gmtstartdate','-t', \
                      help="A GMT date/time for the start date in the form YYYY:MM:DD:HH",default=None)
    parser.add_argument('--gmtenddate','-e', \
                      help="A GMT date/time for the end date in the form YYYY:MM:DD:HH",default=None)
    parser.add_argument('--forecasthours','-f', \
                      help="The hours over which to run the hindcasts (should be a comma separated list)",default="0,6,12,18")
    parser.add_argument('--count','-c', \
                      help="Flag whether to print the count of dates in the given ensemble",default=False,action="store_true")

    parsedArgs = vars(parser.parse_args())
    doPrintCount = parsedArgs['count']
    gmtStartDate = parsedArgs['gmtstartdate']
    gmtEndDate = parsedArgs['gmtenddate']
    try:
        forecastHours = [int(hour) for hour in parsedArgs['forecasthours'].split(",")]
    except:
        parser.print_help()
        raise ValueError,"Error: could not parse arguments for --forecasthours: {}".format(parsedArgs['forecasthours'])

    requiredSet = [gmtStartDate,gmtEndDate]

    #Check if any of the arguments are missing
    if(any( v is None for v in requiredSet)):
        parser.print_help()
        raise ValueError,"not enough arguments were provided"

    dateEnsemble = ensemble(    \
                                startdate = dateStringToDatetime(gmtStartDate), \
                                enddate = dateStringToDatetime(gmtEndDate), \
                                forecastHours = forecastHours)

    numDates = len(dateEnsemble.dates)
    if doPrintCount:
        #If flagged, simply print the number of dates
        print numDates
    else:
        #Otherwise, print all of the dates in the range provided
        for i,date in zip(range(numDates),dateEnsemble.dates):
          print "{i:04}\t{date}".format(i=i,date=str(date))
                            

