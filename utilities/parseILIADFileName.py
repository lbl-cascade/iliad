import os
import datetime as dt

class parseILIADFileName:
    """Parses an ILIAD file name and extracts available information."""

    def __init__(self,inFileName):

        fileName = os.path.basename(inFileName)

        #Get the model type
        modelComponent = fileName.split('.')[-4].split('_')[0]

        #Get the output level
        outputLevel = fileName.split('.')[-3]

        #Get the resolution code
        runResCode = fileName.split('-')[1]

        #Get the date sting (the start of the run
        runDateStr = fileName.split('-')[2]
        #Parse the run start into a datetime object
        year,mon,day,hour = tuple([ int(item) for item in runDateStr.split('_') ])
        runStart = dt.datetime(year,mon,day,hour)

        #Get the run number
        runNum = int(fileName.split('.')[-4].split('_')[1]) - 1    
        #Assume that we ran 1 simulation each day to determine
        #the start date of the simulation
        initDate = runStart + dt.timedelta(days=runNum)

        #Parse the simulation day and add it to determine the start date of the file
        day = int(fileName.split(".")[-2].split('-')[2]) - 1
        thisFileStartDate = initDate + dt.timedelta(days=day)

        #Save the parsed information in the class
        self.fileName = fileName
        self.fileStartDate = thisFileStartDate
        self.runStartDate = initDate
        self.resolutionCode = runResCode
        self.modelComponent = modelComponent
        self.outputLevel = outputLevel

    def dateFromTimeIndex(self,itime,hourSpacing=3):
        """Given a time index, return the corresponding datetime object"""
        return self.fileStartDate + dt.timedelta(hours=itime*hourSpacing)
    
if __name__ == "__main__":

    #*************
    # Simple test
    #*************
    #The fileStartDate here should be Aug 28 2005 00Z (2005-08-28 00:00:00)
    fileName = "iliad-ne120np4-2005_05_01_00-2005_09_01_00.cam_0119.h1.0001-01-02-00000.nc"
    print parseILIADFileName(fileName).fileStartDate
    #This should print the same date + 21 hours
    print parseILIADFileName(fileName).dateFromTimeIndex(7)

    #***************************
    # Adding prefix to filename
    #***************************
    fileName = "regridded.iliad-ne120np4-2005_05_01_00-2005_09_01_00.cam_0119.h1.0001-01-02-00000.nc"
    print parseILIADFileName(fileName).fileStartDate

    #************************
    # Adding dir to filename
    #************************
    fileName = "/arbitrary/path-to-file/with.complex-dirnames/regridded.iliad-ne120np4-2005_05_01_00-2005_09_01_00.cam_0119.h1.0001-01-02-00000.nc"
    print parseILIADFileName(fileName).fileStartDate

    

