import gc
import os
import glob
import errno
import stat
import shutil
from multiprocessing import Pool
import netCDF4 as nc
from iliad.utilities import parseILIADFileName as parser
from iliad.utilities import fixRegriddedTime as fix

def mkdir_p(path,mode = None,owner=-1,group=-1):
    """A command that makes a new directory (like mkdir -p) and optionally
       sets the directory mode, owner, and group for parent parts of the directory.

       (see documentation of os.chown)

       :param path: the path to create
       :param mode: the directory mode (see documentation of os.chmod)
       :param owner: the directory owner (see documentation of os.chown)
       :param group: the directory group (see documentation of os.chown)

    """
    #Attempt to make the directory
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

    #Change the mode of the directory all the way up the tree
    if mode is not None:
        subdir = '/'
        #Loop over all parent pieces of the path
        for part in path.split('/'):
            #Increment the current subdirectory with the current parent part
            subdir += '/' + part
            #Change the mode/owner/group
            try:
                os.chmod(subdir,mode)
                os.chown(subdir,owner,group)
            except:
                pass

def mapIliadFileToPath(iliadFile, \
                            regridResolution='2.0', \
                            baseDirectory='/project/projectdirs/m1517/cascade/iliad'):
    """Given an ILIAD regridded filename, return a path where the file should be archived
    
       :rtype: tuple (archivePath,fileName,parser)
                     where parser is an instance of a parseILIADFileName object describing
                     the file information.
    """
    fileInfo = parser.parseILIADFileName(iliadFile)

    #Set the template for directory names
    directoryTemplate = "{base}/regridded-{rres}-deg/{ores}/{olev}/forecast-day-{fnum}/{year}"

    #Set the template for file names
    fileTemplate = "iliad.{ores}_on_{rres}-deg-grid.{olev}.{fyear:04}-{fmon:02}-{fday:02}-{fhour:02}Z.init_{iyear:04}-{imon:02}-{iday:02}-{ihour:02}Z.nc"

    #Get the forecast day
    forecastDay = (fileInfo.fileStartDate - fileInfo.runStartDate).days
    #Get the forecast year
    forecastYear = fileInfo.fileStartDate.year
    #Get the file output level (e.g., h1)
    outputLevel = fileInfo.outputLevel
    #Get the resolution
    originalResolution = fileInfo.resolutionCode

    #Set the archive directory
    archiveDirectory = directoryTemplate.format( \
                                    base = baseDirectory, \
                                    rres = regridResolution, \
                                    ores = originalResolution, \
                                    fnum = forecastDay, \
                                    olev = outputLevel, \
                                    year = forecastYear)

    #Set the new filename
    filePath = fileTemplate.format( \
                                    ores = originalResolution, \
                                    rres = regridResolution, \
                                    olev = outputLevel, \
                                    fnum = forecastDay, \
                                    fyear  = fileInfo.fileStartDate.year, \
                                    fmon   = fileInfo.fileStartDate.month, \
                                    fday   = fileInfo.fileStartDate.day, \
                                    fhour   = fileInfo.fileStartDate.hour, \
                                    iyear  = fileInfo.runStartDate.year, \
                                    imon   = fileInfo.runStartDate.month, \
                                    iday   = fileInfo.runStartDate.day, \
                                    ihour   = fileInfo.runStartDate.hour, \
                                    )

    return archiveDirectory,filePath,parser


    
def duplicateFile(fileName):
    """Given a fileName, duplicates the file to the archiving directory"""

    # run garbage collection; try to make sure
    # netcdf files are closed
    gc.collect()

    #Make a mode that is user/group read/writeable and global readable for directories
    groupReadDirMode =  stat.S_IRUSR | \
                        stat.S_IWUSR | \
                        stat.S_IXUSR | \
                        stat.S_IRGRP | \
                        stat.S_IWGRP | \
                        stat.S_IXGRP | \
                        stat.S_IROTH | \
                        stat.S_IXOTH
    groupNumber = 55533 #m1517 (found by using `id`)

    #Make a mode that is user/group read/writeable and global readable for files
    groupReadFileMode =     stat.S_IRUSR | \
                            stat.S_IWUSR | \
                            stat.S_IRGRP | \
                            stat.S_IWGRP | \
                            stat.S_IROTH


    #Get the path for the file
    archiveDirectory,filePath,parser = mapIliadFileToPath(fileName)
    archivePath = archiveDirectory + '/' + filePath

    no_create_archive_file = False

    if os.path.exists(archivePath):
        #If the file exists, check that it can be opened with netCDF
        try:
            #with nc.Dataset(archivePath,'r') as fin:
            #    pass
            fin = nc.Dataset(archivePath,'r')
            fin.close()
            del(fin)
            no_create_archive_file = True
        except:
            #Check whether the originating file is valid
            try:
                #with nc.Dataset(fileName,'r') as fin:
                #    pass
                fin = nc.Dataset(archivePath,'r')
                fin.close()
                del(fin)
            except:
                #If we can't open it, remove it
                #os.unlink(archivePath)
                #add the original to the list of failures
                return fileName

    # if the above check passed, we don't need to do an archive; simply return
    if no_create_archive_file:
        return None

    #Check that the originating file is a valid netCDF file
    try:
        #with nc.Dataset(fileName,'r') as fin:
        #    pass

        fin = nc.Dataset(archivePath,'r')
        fin.close()
        del(fin)
    except:
        #add the original to the list of failures
        return fileName

    #Make sure the directory exists
    if not os.path.exists(archiveDirectory):
        mkdir_p(archiveDirectory,mode=groupReadDirMode,group=groupNumber)

    #Copy the file to the destination
    shutil.copy(fileName,archivePath)

    #Fix the time variable
    fix.fixTimeVar(archivePath)

    #Set permissions/ownership for the new file
    os.chmod(archivePath,groupReadFileMode)
    os.chown(archivePath,-1,groupNumber)

    return None




#*******************************************************************************
#*******************************************************************************
#*********************** Main code *********************************************
#*******************************************************************************
#*******************************************************************************
#Set the resolution
resolution = 'ne120np4'

#Get a list of iliad directories
iliadBaseDir = os.getcwd() + '/' + resolution
iliadDirs = sorted(glob.glob(iliadBaseDir + '/iliad-{}*'.format(resolution)))

#Create a list of files to copy
fileList = []
for idir in iliadDirs:
    fileList += sorted(glob.glob(idir + '/regridding/regridded*.nc'))

print 'Duplicating',len(fileList),'files'
multiProc = False
if multiProc:
    #Initialize the multiprocessing
    copyPool = Pool(8)
    failedRegrids = copyPool.map(duplicateFile,fileList)
else:
    failedRegrids = [duplicateFile(ff) for ff in fileList]

#Remove any None values
failedRegrids = [ ffile for ffile in failedRegrids if ffile is not None]
if len(failedRegrids) > 0 :
    print "{} Failed regrids:".format(len(failedRegrids))
    for ffile in failedRegrids:
        print '\t',ffile

print 'Done.'

