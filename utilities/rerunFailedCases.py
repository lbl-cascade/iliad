#!/usr/bin/env python
"""Checks on which ILIAD simulations failed/succeeded.  Resubmits the failed jobs"""
import os
import glob
import subprocess

# Set the dependent job (icJobID = '3532428.edique02')
# (if set to '', no dependency is presumed) 
icJobID = None

# Set the current directory
_cwd = os.getcwd()

# Find all iliad case directories in the current directory
iliadDirs = sorted(glob.glob('iliad*'))

# Initialize the success/failed run list
successfulRuns = []
failedRuns = []

# Loop through the case directories and check for success
for idir in iliadDirs:

    # Open the CaseStatus file
    with open('{}/CaseStatus'.format(idir),'r') as fin:
        flines = fin.readlines()

    # Check whether 'SUCCESSFUL' appears in the last line; if
    # so, the run is deemed successful
    if 'SUCCESSFUL' in flines[-1]:
        successfulRuns.append(idir)
    else:
        failedRuns.append(idir)

# Report on the succesful runs
print "Successful Runs:"
for idir in successfulRuns:
    print '\t',idir

# Report on the failed runs
print "Failed Runs:"
for idir in failedRuns:
    print '\t',idir

#Move into the case directory
for caseDirectory in failedRuns:
    os.chdir(caseDirectory)

    if icJobID is None:
        newBatchLine = '<entry id="BATCHSUBMIT"   value="qsub"/>\n'
    else:
        #Modify the qsub line so that the job depends on the IC job completing successfully
        #NOTE: the doCloneCaseDir.bash script changes BATCHSUBMIT back to qsub as part of the *.prestage script
        newBatchLine = '<entry id="BATCHSUBMIT"   value="qsub -W depend=afterok:{}"  />\n'.format(icJobID)

    with open('env_run.xml','r') as fin:
        envRunLines = fin.readlines()
    #Substitute the new batch submit line
    envRunLines = [ l if not "BATCHSUBMIT" in l else newBatchLine for l in envRunLines  ]
    #Write the new xml file
    with open('env_run.xml','w') as fout:
        fout.write(''.join(envRunLines))

    #Submit the new job
    submitLine = './{}.submit'.format(os.path.basename(caseDirectory))
    print  "Running " + submitLine
    subprocess.check_call(submitLine.split(),stderr=subprocess.STDOUT)

    #Go back to the original directory
    os.chdir(_cwd)
