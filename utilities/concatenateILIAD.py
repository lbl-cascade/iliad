#!/usr/bin/env python
"""
Concatenates a collection of ILIAD files and organizes date information.
"""

import os
import sys
from iliad.utilities import parseILIADFileName as iparser
import netCDF4 as nc
import numpy as np
import subprocess
import datetime as dt
import multiprocessing as mp

def convertFile(fname):
  """Converts the time dimension of a file to a record dimension; for use with mp.Pool.map()"""
  commandList = ['ncks','-O','--mk_rec_dmn', 'time',fname,fname]
  subprocess.check_call(commandList)

def concatenateILIAD(fileCollection, \
                     doClobber = False, \
                     beVerbose = True, \
                     convertTime = False, \
                     outFileName = None):
    """Given a collection of ILIAD files, create an archive of the files
    
    
    :param fileCollection: a list of ILIAD files
                        (assumed to use the normal ILIAD naming scheme)

    :param doClobber: flags whether to clobber an existing output file
    
    :param beVerbose: flags whether to print diagnostic output
    
    :param convertTime: flags whether to use `ncks` to convert the time dimension to unlimited

    :param outFileName: the name of the output file
                           
    :returns: the name of the new archive
    """
    
    def vprint(msg):
        """Print only if beVerbose is True"""
        if beVerbose:
            print(msg)
    
    #Parse the date information from all the files
    parseInfoList = [ (fname,iparser.parseILIADFileName(fname)) for fname in fileCollection ]
    
    #Get a list of filenames sorted by the first time index in the file
    dateSortedList = sorted([ (pp.dateFromTimeIndex(0),fname,pp) for fname,pp in parseInfoList ])
    
    #********************************
    # Construct the output file name
    #********************************
    
    #Get the first and last (date,file,parseILIADFileName) tuples from the sorted list
    firstFile = dateSortedList[0]
    lastFile = dateSortedList[-1]
    
    #Get the file prefix
    prefix = os.path.basename(firstFile[1]).split('-')[0]
    
    #Get the resolution code
    rescode = firstFile[2].resolutionCode
    
    #Get the output level
    olev = firstFile[2].outputLevel
    
    #Get the model component
    comp = firstFile[2].modelComponent
    
    #Get the first date in the first file
    idate = firstFile[0] 
    
    #Get the last date in the last file
    with nc.Dataset(lastFile[1],'r') as fin:
        #Get the number of time indices in the last file
        ntime = len(fin.variables['time'][:])
        iend = ntime - 1
    edate = lastFile[2].dateFromTimeIndex(iend) 
    
    #Set the start/end date strings
    dateTemplate = "{y:04}_{m:02}_{d:02}_{h:02}"
    fdate = dateTemplate.format(y=idate.year,m=idate.month,d=idate.day,h=idate.hour)
    ldate = dateTemplate.format(y=edate.year,m=edate.month,d=edate.day,h=edate.hour)
    
    #Set the output file name
    if outFileName is None:
        outFileNameTemplate = "{prefix}-{rescode}-{fdate}-{ldate}.{comp}.{olev}.archive.nc"
        outFileName = outFileNameTemplate.format( \
                                prefix = prefix, \
                                rescode = rescode, \
                                fdate = fdate, \
                                ldate = ldate, \
                                olev = olev, \
                                comp = comp)
        
    #************************************
    # Create the output file with ncrcat
    #************************************
    #Check if the output file already exists
    if os.path.exists(outFileName):
        if not doClobber:
            raise RuntimeError,"{} exists, but doClobber is set to False".format(outFileName)
    
    #Set the list of files to pass to ncrcat
    sortedFileNames = " ".join([dd[1] for dd in dateSortedList])
    
    #Convert the time dimension to a record dimension in the input files if needed
    if convertTime:
        vprint("Fixing record dimensions")
        #Use 8 procs to fix the time dimensions
        fixPool = mp.Pool(8)
        fixPool.map(convertFile,sortedFileNames.split())

    
    #Run ncrcat
    command = "ncrcat -O {files} {ofile}".format(files=sortedFileNames,ofile=outFileName)
    vprint("Creating {}".format(outFileName))
    subprocess.check_call(command.split())
    
    #Overwrite the time dimension in the new file
    newUnits = 'hours since 1979-01-01 00:00:00'
    outTimes = np.array([nc.date2num(dd[2].dateFromTimeIndex(i),units=newUnits) for i in range(ntime) for dd in dateSortedList])
    
    #Set the time bounds
    dtime = dt.timedelta(hours=1.5)
    tbound0 = np.array([nc.date2num(dd[2].dateFromTimeIndex(i) - dtime,units=newUnits) for i in range(ntime) for dd in dateSortedList])
    tbound1 = np.array([nc.date2num(dd[2].dateFromTimeIndex(i) + dtime,units=newUnits) for i in range(ntime) for dd in dateSortedList])
            
    #Open the new output file
    with nc.Dataset(outFileName,'r+') as fio:
        #Set the units and change the calendar attribute
        fio.variables['time'].units = newUnits
        fio.variables['time'].calendar='standard'
        #Write the times to the file
        fio.variables['time'][:] = outTimes
        #Write the time bounds to the file
        fio.variables['time_bnds'][:,0] = tbound0
        fio.variables['time_bnds'][:,1] = tbound1
        
    return outFileName

#***************************
# Command line utility code
#***************************
if __name__ == "__main__":

    import argparse

    #*******************************************************************************
    #*******************************************************************************
    #********************** Parse command line arguments ***************************
    #*******************************************************************************
    #*******************************************************************************
    parser = argparse.ArgumentParser( \
                                    description = "Concatenate ILIAD files", \
                                    formatter_class = argparse.RawDescriptionHelpFormatter, \
                                    epilog = __doc__)

    parser.add_argument('--outputfile','-o', \
                      help="The file to which to write",default=None)
    parser.add_argument('--clobber','-c', \
                      help="Clobber existing output file",default=False,action='store_true')
    parser.add_argument('--quiet','-q', \
                      help="Suppress diagnostic printing output",default=False,action='store_true')

    parser.add_argument('--fixtimedim', \
                      help="Flags whether to convert the time dimension to a record in the input files",default=False,action='store_true')
    parser.add_argument('inputfile',nargs='*',help="Input file names")

    parsedArgs = vars(parser.parse_args())
    inFileNames = parsedArgs['inputfile'][:]
    outFileName= parsedArgs['outputfile']
    doClobber = parsedArgs['clobber']
    convertTime = parsedArgs['fixtimedim']
    beVerbose = not parsedArgs['quiet']

    if len(inFileNames) == 0:
        print "Error: at least 1 input file name is required"
        print ""
        parser.print_help()
        quit()

    #Concatenate the files
    concatenateILIAD(fileCollection = inFileNames, \
                     doClobber = doClobber, \
                     beVerbose = beVerbose, \
                     convertTime = convertTime, \
                     outFileName = outFileName)
