from numpy import *

def conformDimensions(inputArray,outputShape):
    """Copies inputArray to match outputShape (assuming the dimensions of
inputArray are a contiguous subset of outputShape).

    """

    try:
        inputArray = 1*array(inputArray)
    except:
        raise ValueError,"inputArray is not arraylike."
    
    #Check for subsetting
    inputArrayShape = shape(inputArray)
    inputArrayRank = len(inputArrayShape)
    outputRank = len(outputShape)

    #If the input array is a scalar, simply an array with the same shape as outputShape, but filled with the scalar value)
    if(inputArrayShape == ()):
        outputArray = inputArray*ones(outputShape)
        return outputArray

    #Check that the input array dimensions are a subset of the output array shape
    if not all([inputArrayShape[r] in outputShape for r in range(inputArrayRank)]):
        raise ValueError,"The dimensions of inputArray are not a subset of the dimensions of outputShape: shape(inputArray) = {}, ouputShape = {}".format(inputArrayShape,outputShape)

    #Generate a transpose operation that puts the matching dimensions at the end of the array
    leftmostList = []
    rightmostList = []
    for r in range(outputRank):
        if(outputShape[r] in inputArrayShape):
            rightmostList.append(r)
        else:
            leftmostList.append(r)

    transposeList = leftmostList + rightmostList

    #Generate the transposed shape
    transposeShape = [ outputShape[t] for t in transposeList ] 

    #Generate the reverse transpose operation
    #reverseTranspose = [ transposeList[t] for t in transposeList ]
    reverseTranspose = [ transposeList.index(r) for r in range(outputRank) ]

    #Use broadcasting to create a conformant, but transposed array (the matching dims are required to be
    #in the rightmost dimensions for broadcasting to work)
    outputArray = inputArray*ones(transposeShape)

    #Now reverse the transpose the array to get an array of the proper shape
    outputArray = transpose(outputArray,reverseTranspose)

    return outputArray


if __name__ == "__main__":


    inputArray,_ = meshgrid(array(range(10)),array(range(11)) + 25)
    outputShape = [3,4,11,10,5]

    outputArray =  conformDimensions(inputArray,outputShape)
    print shape(inputArray)
    print shape(outputArray)
    print inputArray
    print outputArray[1,3,:,:,2]
        

