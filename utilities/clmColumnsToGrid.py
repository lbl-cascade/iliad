#!/usr/bin/env python
from numpy import *
import netCDF4 as nc
from iliad.interpolation import geoBin
import os

def copyVariableMetadata(varName,fin,fout):
    #Copy all attributes
    for att in fin.variables[varName].ncattrs():
        fout.variables[varName].setncattr(att,fin.variables[varName].getncattr(att))

def attemptVariableMetadataCopy(varName,fin,fout):
    #Check if the variable is in fin
    if varName in fin.variables:
        #TODO: deal with missing values
        #If it is, create it in the output file
        fout.createVariable(varName,fin.variables[varName].datatype,fin.variables[varName].dimensions)
        copyVariableMetadata(varName,fin,fout)



def gridCLMFile(    infilename, \
                    outfilename,    \
                    gridfilename=None, \
                    doClobber=False,    \
                    fillValue = +1e36, \
                    outVarList = None):
    """Takes a CLM file and puts the column-oriented data into a gridded file that is suitable for viewing"""
    
    #Open the input CLM file
    try:
        fin = nc.Dataset(infilename,"r")
    except BaseException as e:
        print "Failed to open {}".format(infilename)
        raise e

    #Read the lat/lon column variables
    columnLats = fin.variables["cols1d_lat"][:]
    columnLons = fin.variables["cols1d_lon"][:]

    #Open the output file
    try:
        fout = nc.Dataset(outfilename,"w",clobber=doClobber)
    except BaseException as e:
        print "Failed to open {} for writing with mode 'w'".format(outfilename)
        raise e

    if(gridfilename is not None):
        #Open the grid file
        try:
            fgrid = nc.Dataset(gridfilename,"r")
        except BaseException as e:
            print "Failed to open {}".format(gridfilename)
            raise e

        #Read the lat/lon grid variables
        gridLat = fgrid.variables["lat"][:]
        gridLon = fgrid.variables["lon"][:]

        #********************************************************
        # Generate latitude bins
        #********************************************************
        #Set the edges of the latitude grid as the midpoints of the lat values
        latTmp = (gridLat[:-1] + gridLat[1:])/2.
        latEdges = zeros([len(latTmp)+2])
        latEdges[1:-1] = latTmp
        #Set the endpoints of the lat grid edges as +/- 90 degrees
        latEdges[0] = -90.0
        latEdges[-1] = 90.0
        #Create a geoBin instance with the right number of bins (the edge/center values will be overwritten)
        latBins = geoBin.geoBin(latEdges[0],latEdges[-1],numBins = len(latEdges)-1,bCosBins=False)
        #Set the left edges of the latitude bins
        latBins.leftEdge = latEdges[:-1]
        #Set the right edges of the latitude bins
        latBins.rightEdge = latEdges[1:]
        #Set the latitude bin centers
        latBins.center = gridLat
        #Estimate the bin width from the width of the centermost bin
        centerBin = len(gridLat)/2 + 1
        latBins.deltaBin = latEdges[centerBin] - latEdges[centerBin-1]

        #********************************************************
        # Generate longitude bins
        #********************************************************
        #Set the edges of the longitude grid as the midpoints of the lon values
        lonTmp = (gridLon[:-1] + gridLon[1:])/2.
        lonEdges = zeros([len(lonTmp)+2])
        lonEdges[1:-1] = lonTmp
        #Set the edges of the longitude grid as the midpoint of the wraparound of the opposite edge
        lonEdges[0] = ((gridLon[-1]-360.) + gridLon[0])/2.
        lonEdges[-1] = ((gridLon[0] + 360.) + gridLon[-1])/2.
        #Create a geoBin instance with the right number of bins (the edge/center values will be overwritten)
        lonBins = geoBin.geoBin(lonEdges[0],lonEdges[-1],numBins = len(lonEdges)-1,bCosBins = False)
        #Set the edges of the longitude bins
        lonBins.leftEdge = lonEdges[:-1]
        lonBins.rightEdge= lonEdges[1:]
        #Set the centers of the longitude bins
        lonBins.center = gridLon
        centerBin = len(gridLon)/2 + 1
        #Estimate the bin width from the width of the centermost bin
        lonBins.deltaBin = lonEdges[centerBin] - lonEdges[centerBin-1]

        #Get the grid indices of the lat/lons
        iGrid = array([latBins.getIndex(lat) for lat in columnLats ])
        jGrid = array([lonBins.getIndex(lon) for lon in columnLons ])

        haveLatLonVars = True

    else:
        iGrid = fin.variables["cols1d_jxy"][:] - 1
        jGrid = fin.variables["cols1d_ixy"][:] - 1

        #Attempt to determine the underlying grid spacing (assuming that lons vary most quickly)
        dLatThresh = 1e-4 #Assume that latitude spacing  is larger than 0.004 to deal with numerical issues
        deltaLon = columnLons[1] - columnLons[0]
        deltaLat = amin([abs(columnLats[i+1] - columnLats[i]) for i in nonzero(diff(columnLats) > dLatThresh)[0]])

        #Generate the latitude grid
        latCenters = arange(-90,90+deltaLat,deltaLat)

        #Generate the longitude grid
        lonCenters = arange(0,360,deltaLon)

        #Flag that we don't have a netcdf version of the lat/lon variables
        haveLatLonVars = False

    #Get the list of output variables if it hasn't been specified (i.e., variables with a column dimension)
    if(outVarList is None):
        outVarList = []
        for var in fin.variables:
            if u'column' in fin.variables[var].dimensions:
                outVarList.append(var)

    #Go through the dimensions and copy them to the output file
    dimList = []
    for var in outVarList:
        for dim in list(fin.variables[var].dimensions):
            dimList.append(dim)

    dimList = list(sorted(set(dimList)))

    #Create the dimensions needed by the requested variables
    for dim in dimList:
        if dim != u'column':
            fout.createDimension(dim,len(fin.dimensions[dim]))
            attemptVariableMetadataCopy(dim,fin,fout)

    #Create the lat/lon dimensions
    if(haveLatLonVars):
        for dim in ['lat','lon']:
            fout.createDimension(dim,len(fgrid.dimensions[dim]))
            attemptVariableMetadataCopy(dim,fgrid,fout)
    else:
        fout.createDimension('lat',len(latCenters))
        fout.createDimension('lon',len(lonCenters))
        fout.createVariable('lat','f8',('lat',))
        fout.variables['lat'].long_name = "latitude"
        fout.variables['lat'].units = "degrees_north"

        fout.createVariable('lon','f8',('lon',))
        fout.variables['lon'].long_name = "longitude"
        fout.variables['lon'].units = "degrees_east"

    #Go through the variable list and create variables
    for var in outVarList:
        varDims = list(fin.variables[var].dimensions)
        varDims.pop(varDims.index(u'column'))
        varDims.append('lat')
        varDims.append('lon')
        if('int' in str(fin.variables[var].datatype)):
            dumFill = -9999
        else:
            dumFill = fillValue
        fout.createVariable(var,fin.variables[var].datatype,varDims,fill_value = dumFill)
        copyVariableMetadata(var,fin,fout)

    #Copy the dimension variables
    for dim in fout.dimensions:
        if dim in fin.variables:
            fout.variables[dim][:] = fin.variables[dim][:]
        if haveLatLonVars:
            if dim in fgrid.variables:
                fout.variables[dim][:] = fgrid.variables[dim][:]

    if(not haveLatLonVars):
        fout.variables['lat'][:] = latCenters
        fout.variables['lon'][:] = lonCenters

    #Go through the output variables and write their data
    #to the output file
    for var in outVarList:
        if(var in fout.variables):
            varShape = shape(fout.variables[var])
            varRank = len(varShape)

            #Use an integer-appropriate fill value if the variable is of an integer type
            if('int' in str(fout.variables[var].dtype)):
                dumFill = -9999
            #Otherwise, use the provided fill value
            else:
                dumFill = fillValue
            #Create a dummy variable that is filled with fill values
            outVar = dumFill*ones(varShape)

            #If the variable is 2D, treat the array one way
            if(varRank == 2):
                outVar[iGrid,jGrid] = fin.variables[var][:]

            #If it is 3D, treat it another
            if(varRank == 3):
                for k in range(varShape[0]):
                    outVar[k,iGrid,jGrid] = fin.variables[var][:,k]

            #Write the variable to disk
            fout.variables[var][:] = outVar


if __name__ == "__main__":
    import argparse

    #*******************************************************************************
    #*******************************************************************************
    #********************** Parse command line arguments ***************************
    #*******************************************************************************
    #*******************************************************************************
    parser = argparse.ArgumentParser()

    parser.add_argument('--gridfile','-g', \
                      help="A file with the lat/lon grid (optional)",default=None)
    parser.add_argument('--outputfile','-o', \
                      help="The file to which to write (optional); the prefix 'gridded.' is added to the input file name by default",default=None)
    parser.add_argument('--variables','-v', \
                      help="A list of variables to columnize (optional)",default=None)
    parser.add_argument('--clobber','-c', \
                      help="Clobber existing output file",default=False,action='store_true')
    parser.add_argument('inputfile',nargs=1)

    parsedArgs = vars(parser.parse_args())
    variables = parsedArgs['variables']
    infilename = parsedArgs['inputfile'][0]
    outfilename= parsedArgs['outputfile']
    gridfilename= parsedArgs['gridfile']
    doClobber = parsedArgs['clobber']

    #Construct the default output file name
    if(outfilename is None):
      outfilename = 'gridded.{}'.format(os.path.basename(infilename))

    gridCLMFile(infilename = infilename,    \
                gridfilename = gridfilename,    \
                outfilename = outfilename, \
                doClobber = doClobber,
                outVarList = variables)



